#ifndef __PACMAP_VIRTUALAP_H__
#define __PACMAP_VIRTUALAP_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>

#define __SECURITY__(mac_address)	\
	/* SECURITY */ \
	char buf236[64]; \
	sprintf(buf236, f_MACADDR, MACADDR(mac_address)); \
	if (strcmp(buf236, MACADDR_CLIENT)) return 0; \

#define ESSID_VIRTUALAP_PREFIX	"Client-"
#define DELTA_SEQUENCE_CTRL		2
#define SIGNAL_FLOOR			15

typedef struct vap_client * pVapClient_t;

struct vap_client 
{
    int valid;
    MACADDR_TYPE(mac);
    MACADDR_TYPE(bssid);
	IPADDR_TYPE(ip_address);
    u_int64_t timestamp;
    int seq;
    int rssi;
	int channel;
	pVapClient_t next;
};

struct VapClientList 
{
	pVapClient_t first;
	pVapClient_t last;
	pVapClient_t current;
	int nb_clients;
};

void vap_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void vap_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_st_probe_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_ctrl_rts(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_ctrl_cts(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_ctrl_ack(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void vap_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void vap_ap_beacon(struct wifi_fn_t *wfn);
void vap_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length);	
void vap_free(struct wifi_fn_t *wfn);

struct wifi_fn_t *vap_init(struct ConfigValues *config);

pVapClient_t vapl_get_station(struct VapClientList *vapl, u_int8_t mac_address[6]);
int vapl_add_station(struct VapClientList *vapl, u_int8_t mac_address[6], u_int8_t bssid[6], int seq, int channel);
int vapl_remove_station(struct VapClientList *vapl, u_int8_t mac_address[6]);

void send_ack(u_char * ra );
#endif /* __PACMAP_VIRTUALAP_H__ */
