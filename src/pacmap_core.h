#ifndef __PACMAP_PACMAP_CORE_H__
#define __PACMAP_PACMAP_CORE_H__

#include <pacmap/pacmap.h>

/* ATH send/recv functions */
int packet_send(void* buf, size_t count, int rate);
void packet_recv(int fd, short event, void *arg);

/* TAP send/recv functions */
int packet_ti_send(void* buf, size_t count);
void packet_ti_recv(int fd, short event, void *arg);

/* AP MSGS send/recv functions */
void ap_msgs_init(int *listen_fd);
void ap_msgs_msg_accept(int fd, short ev, void *arg);
void ap_msgs_msg_recv(int fd, short event, void *arg);
int ap_msgs_msg_send(void *buf, size_t count, IPADDR_TYPE(ip_address));

/* Periodic functions */
void periodic_fn(int fd, short event, void *arg);
void periodic_change_channel(int fd, short event, void *arg);
void periodic_scanning(int fd, short event, void *arg);

void pacmap_add_timer(int timer_usec, void *arg, int sz_arg);

#endif
