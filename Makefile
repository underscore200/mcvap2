CC	= gcc
LD	= $(CC)

TARGET		= libpacmap.so

PYTHON_VERSION	= 2.6
CFLAGS		= -g -Wall -I/usr/include/python$(PYTHON_VERSION) -I../include/ -I.
CFLAGS_DEBUG	= 
# -Wextra -Wunused 
LDFLAGS		= -levent -lssl -lpython$(PYTHON_VERSION) -fPIC -shared -Wl,-soname,$(TARGET)

PREFIX		= /usr/local
LIBDIR		= $(PREFIX)/lib
INCLUDEDIR	= $(PREFIX)/include
PYMODDIR	= $(LIBDIR)/python$(PYTHON_VERSION)/dist-packages

SRCS	= 	osdep/common.c osdep/linux.c osdep/linux_tap.c osdep/network.c osdep/osdep.c \
			osdep/radiotap/radiotap-parser.c include/crypto.c \
			python_pacmap.c pacmap_core.c init_pacmap.c \
			wifi/accesspoint.c wifi/station.c wifi/protocol_80211.c \
			wifi/ap_80211.c wifi/sta_80211.c wifi/virtualap.c

OBJS 	= 	${SRCS:.c=.o}

.SUFFIXES:
.SUFFIXES: .o .c
.c.o :
	$(CC) $(CFLAGS) $(CFLAGS_DEBUG) -c -o $@ $<

all: links $(OBJS) 
	@echo "**** Linking C library ****"
	$(LD) $(LDFLAGS) $(CFLAGS) -o $(TARGET) $(OBJS)

clean:
	rm -f $(TARGET) $(OBJS)

install: 
	install $(TARGET) $(LIBDIR)/
	ln -f -s $(LIBDIR)/$(TARGET) $(PYMODDIR)/pacmapmodule.so
	cp -RLv ../include/pacmap/ $(INCLUDEDIR)
	
uninstall: 
	rm -fr $(LIBDIR)/$(TARGET)
	rm -fr $(PYMODDIR)/$(TARGET)
	rm -fr $(INCLUDEDIR)/pacmap/	

dclean: clean
	rm -fr ../include/pacmap/*
	
EXHEADER = 	"pacmap.h" "verbose.h" "wifi/ieee802_11.h" "wifi/protocol_80211.h" \
			"wifi/ap_80211.h" "wifi/sta_80211.h" "wifi/virtualap.h" \
			"wifi/accesspoint.h" "wifi/station.h"
EXHEADER_DIRS = "wifi"

links:
	@here="`pwd`"; \
	for dir in $(EXHEADER_DIRS); do \
		mkdir -p "../include/pacmap/$$dir"; \
	done; \
	for file in $(EXHEADER); do \
		dest="../include/pacmap/$$file"; \
		ln -fs "$$here"/"$$file" "$$dest"; \
	done;
