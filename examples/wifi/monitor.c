/*
 * Example of a monitor card library.
 * This device just listens to packets, but does not process them.
 *
 * This file can be used as a sample library for PacMap, using IEEE 802.11.
 *
 * 2010	 Maria Eugenia Berezin <berezin @ imag.fr>
 *
 */

#include <pacmap/wifi/protocol_80211.h>
#include "monitor.h"

void monitor_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_assoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_probe_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_ctrl_rts(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_ctrl_cts(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_ctrl_ack(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
}

void monitor_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length)
{
}


void monitor_periodic(struct wifi_fn_t *wfn, u_char *packet, int length)
{
}

void monitor_timer_fn(struct wifi_fn_t *wfn, void *arg)
{
}

void monitor_ap_beacon(struct wifi_fn_t *wfn)
{
}

void monitor_monitor_channels(struct wifi_fn_t *wfn)
{
}

void monitor_scanning(struct wifi_fn_t *wfn, void *arg)
{
}

void monitor_free(struct wifi_fn_t *wfn)
{
}

struct wifi_fn_t *monitor_init(struct ConfigValues *config)
{
	struct wifi_fn_t *wfn;

	DPRINT(DMAX, "%s: monitor_init", __FILE__);
	wfn = wfn_alloc(0);

	if (!wfn)
		return NULL;

	wfn->wifi_st_assoc_request	= monitor_st_assoc_request;
	wfn->wifi_st_assoc_response	= monitor_st_assoc_response;
	wfn->wifi_st_disassoc		= monitor_st_disassoc;
	wfn->wifi_st_reassoc_request	= monitor_st_reassoc_request;
	wfn->wifi_st_reassoc_response	= monitor_st_reassoc_response;
	wfn->wifi_st_probe_request	= monitor_st_probe_request;
	wfn->wifi_st_probe_response	= monitor_st_probe_response;
	wfn->wifi_st_beacon	= monitor_st_beacon;
	wfn->wifi_st_auth	= monitor_st_auth;
	wfn->wifi_st_deauth	= monitor_st_deauth;
	wfn->wifi_ctrl_rts	= monitor_ctrl_rts;
	wfn->wifi_ctrl_cts	= monitor_ctrl_cts;
	wfn->wifi_ctrl_ack	= monitor_ctrl_ack;
	wfn->wifi_data_data	= monitor_data_data;
	wfn->wifi_data_nodata	= monitor_data_nodata;
	wfn->wifi_ti_recv = monitor_ti_recv;

	wfn->wifi_periodic = monitor_periodic;
	wfn->wifi_timer_fn = monitor_timer_fn;
	wfn->wifi_ap_beacon	= monitor_ap_beacon;
	wfn->wifi_monitor_channels = monitor_monitor_channels;
	wfn->wifi_scanning = monitor_scanning;

	wfn->wifi_free = monitor_free;

	wfn->time_beacon = BEACON_INTERVAL_TIMER;
	wfn->time_monitor_channels = BEACON_INTERVAL_TIMER;
	wfn->time_scanning = BEACON_INTERVAL_TIMER;

	return wfn;
}
