#ifndef __PACMAP_STATION_H__
#define __PACMAP_STATION_H__

#include <sys/types.h>
#include <string.h>
#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>

typedef struct ST_info *pClient_t;
typedef struct AP_info *pAP_info;

#define MAX_CHANNEL_TIME 	150000
#define MIN_CHANNEL_TIME 	20000
#define TIME_SCANNING 		10000000
#define AUTH_ASSOC_TIMEOUT	3000000
#define AP_THRESHOLD		165
#define AP_TIMEOUT_SEC		1
#define DELTA_TIMESTAMP 	10

enum {
	STA_IDLE				= 0,
	STA_SCANNING			= 1,
	STA_AUTH_REQUEST		= 2,
	STA_ASSOC_REQUEST		= 3,
};
#define MAX_AP_INFO		10

struct AP_info
{
	u_int8_t bssid[6];
	u_int8_t mac_address[6];
	unsigned char essid[33];
	int channel;
	u_int64_t timestamp;
	u_char rates_supp[5];
	u_char rates_supp_ext[9];
  	int rate;
	int encryption;
	char key_wep[256];
	unsigned short interval;
	u_int16_t capability;
	int rssi;
	int is_valid;
	struct timeval tv;
	pAP_info next;		/* the next ap in list 	 */
};

struct APInfoList 
{
	pAP_info first;
	pAP_info last;
	pAP_info current;
	int nb_aps;
};

// WPA structure
struct WPA_hdsk
{
	u_char stmac[6];			/* supplicant MAC			 */
	u_char snonce[32];			/* supplicant nonce			 */
	u_char anonce[32];			/* authenticator nonce		 */
	u_char keymic[16];			/* eapol frame MIC			 */
	u_char eapol[256];			/* eapol frame contents		 */
	int eapol_size;				/* eapol frame size			 */
	int keyver;					/* key version (TKIP / AES)	 */
	int state;					/* handshake completion		 */
};

/* Per client info structure */
struct ST_info
{
	u_int8_t mac_address[6]; 	/* the client's MAC address  */
	u_char rates_supp[9];
	u_char rates_supp_ext[5];
	int channel;
	pAP_info ap;
	int state;			 
	int got_probe_requests;
	struct APInfoList ap_info_list; 	
	int capability;
	int interval;
};

void sta_read_options(pClient_t sta);
int sta_get_next_channel(pClient_t sta);

pAP_info sta_new_ap_info(u_int8_t bssid[6], u_int8_t mac_address[6], unsigned char essid[33], int channel, int rssi, u_int64_t timestamp, u_int16_t capability);
int is_same_ap(pAP_info ap1, pAP_info ap2);
int sta_get_ap_info(pClient_t sta, pAP_info myap);
void sta_ap_free(pClient_t sta, pAP_info ap, pAP_info prev_ap);
void sta_save_ap_info(pClient_t sta, pAP_info new_ap);
void sta_print_ap_list(pClient_t sta);
void sta_save_ap_rssi(pClient_t sta, int rssi);
void sta_ap_list_timeout(pClient_t sta);
int sta_ap_timeout(pClient_t sta, pAP_info ap);

void sta_init(pClient_t sta, u_int8_t mac_address[6]);
void sta_free(pClient_t sta);


#endif
