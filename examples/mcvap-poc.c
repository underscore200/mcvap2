/*
 * Example for testing purposes
 * 
 * 15/01/2010    Maria Eugenia Berezin <berezin @ imag.fr>
 *
 */

#include <pacmap/pacmap.h>
#include "wifi/mcvap-poc.h"

int main(int argc, char** argv)
{
        /*  Multi-channel Virtual AP */
        pacmap_init(argc, argv, mcvap_poc_init);
        return 0;
}

