#ifndef __PACMAP_MULTICHANNELVAPPOC_H__
#define __PACMAP_MULTICHANNELVAPPOC_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>

void mcvap_poc_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_poc_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_poc_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_poc_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_poc_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_poc_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_poc_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_poc_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_poc_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_poc_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_poc_ap_beacon(struct wifi_fn_t *wfn);
void mcvap_poc_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length);	
void mcvap_poc_free(struct wifi_fn_t *wfn);

struct wifi_fn_t *mcvap_poc_init(struct ConfigValues *config);

#endif /* __PACMAP_MULTICHANNELVAPPOC_H__ */
