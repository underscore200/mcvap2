#ifndef __PACMAP_VAPBEACON_H__
#define __PACMAP_VAPBEACON_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>

void vap_beacon_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_beacon_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void vap_beacon_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_beacon_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_beacon_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_beacon_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_beacon_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_beacon_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void vap_beacon_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void vap_beacon_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void vap_beacon_ap_beacon(struct wifi_fn_t *wfn);
void vap_beacon_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length);	
void vap_beacon_free(struct wifi_fn_t *wfn);

struct wifi_fn_t *vap_beacon_init(struct ConfigValues *config);

#endif /* __PACMAP_VAPBEACON_H__ */
