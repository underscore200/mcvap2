/*
 *  802.11 station
 *
 */

#ifndef __PACMAP_STA_80211_H__
#define __PACMAP_STA_80211_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>

void sta_80211_st_assoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void sta_80211_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void sta_80211_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void sta_80211_st_probe_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void sta_80211_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void sta_80211_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void sta_80211_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void sta_80211_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void sta_80211_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void sta_80211_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length);	

void sta_80211_start_scan(struct wifi_fn_t *wfn, void *arg);
void sta_80211_probe_request(struct wifi_fn_t *wfn, void *arg);
void sta_80211_send_probe_request(struct wifi_fn_t *wfn);
void sta_80211_send_auth_request(struct wifi_fn_t *wfn);
void sta_80211_send_assoc_request(struct wifi_fn_t *wfn);
void sta_80211_auth_assoc_timeout(struct wifi_fn_t *wfn, void *arg);
void sta_80211_send_data_nodata(struct wifi_fn_t *wfn);

struct wifi_fn_t *sta_80211_init(struct ConfigValues *config);
void sta_80211_free(struct wifi_fn_t *wfn);

#endif







