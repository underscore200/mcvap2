#ifndef __PACMAP_MONITOR_H__
#define __PACMAP_MONITOR_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>

void monitor_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_st_assoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void monitor_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_st_probe_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_ctrl_rts(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_ctrl_cts(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_ctrl_ack(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void monitor_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void monitor_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void monitor_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length);	

void monitor_periodic(struct wifi_fn_t *wfn, u_char *packet, int length);		
void monitor_timer_fn(struct wifi_fn_t *wfn, void *arg);
void monitor_ap_beacon(struct wifi_fn_t *wfn);
void monitor_monitor_channels(struct wifi_fn_t *wfn);
void monitor_scanning(struct wifi_fn_t *wfn, void *arg);	

void monitor_free(struct wifi_fn_t *wfn);		

struct wifi_fn_t *monitor_init(struct ConfigValues *config);

#endif /* __PACMAP_MONITOR_H__ */
