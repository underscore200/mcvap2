/**
 *  802.11 monitor AP
 *  based on airbase-ng.c
 *
 *  2010	Maria Eugenia Berezin	- Added libevent functionality
 *  2008	Yan Grunenberger		- First version
 *  AP MSGS server from http://code.google.com/p/ishbits/
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  In addition, as a special exception, the copyright holders give
 *  permission to link the code of portions of this program with the
 *  OpenSSL library under certain conditions as described in each
 *  individual source file, and distribute linked combinations
 *  including the two.
 *  You must obey the GNU General Public License in all respects
 *  for all of the code used other than OpenSSL. *  If you modify
 *  file(s) with this exception, you may extend this exception to your
 *  version of the file(s), but you are not obligated to do so. *  If you
 *  do not wish to do so, delete this exception statement from your
 *  version. *  If you delete this exception statement from all source
 *  files in the program, then also delete it here.
 */

#include <event.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <fcntl.h>
#include <errno.h>
#include "osdep/osdep.h"
#include "pacmap_core.h"

struct EventArg
{
	struct event ev;
	void *arg;
};

int setnonblock(int fd);

/**
 * Wireless interface packet send/recv functions 
 */
int packet_send(void *buf, size_t count, int rate) 
{
	struct wif *wi = _wi_out;
	struct tx_info txinfo;
	DPRINT(DMAX2, "%s/packet_send", __FILE__);
	
	txinfo.ti_rate = rate;
	if (wi_write(wi, buf, count, &txinfo) == -1) {
		DPRINT(DERR, "%s/packet_send: wi_write()", __FILE__);
		return -1;
	}

	return 0;
}

void packet_recv(int fd, short event, void *arg)
{
	unsigned char buf[4096];
	int len;
	struct event *ev = arg;

	/* Reschedule this event */
	event_add(ev, NULL);

	struct rx_info rxinfo;
	struct wif *wi = _wi_in;

	len = wi_read(wi, buf, sizeof(buf), &rxinfo);
	if (len == -1) {
		DPRINT(DERR, "%s/packet_sent: wi_read()", __FILE__);
		return;
	} else if (len == 0) {
		return;
	}
	/* Note:
	rxinfo.ri_mactime	= time
	rxinfo.ri_power		= rssi - between 160 and 215 ? // no this sends negative values
	rxinfo.ri_noise		= (fixed) 161
	rxinfo.ri_channel	= channel
	rxinfo.ri_freq		= (fixed) 0
	rxinfo.ri_rate		= 1, 5.5, 18...
	rxinfo.ri_antenna	= 1/2
	*/
	if (_wfn->wifi_packet_recv)
	{
		_wfn->rate = rxinfo.ri_rate;
		_wfn->wifi_packet_recv(_wfn, buf, len, wi, rxinfo.ri_power);
	}
}

/**
 * TAP interface packet send/recv functions 
 */
int packet_ti_send(void* buf, size_t count)
{
	struct tif *ti = _ti_in;
	
	DPRINT(DMAX2, "%s/packet_ti_send", __FILE__);
	if (ti_write(ti, buf, count) == -1) {
		DPRINT(DERR, "%s/packet_ti_send: ti_write()", __FILE__);
		return -1;
	}
	return 0;
}

void packet_ti_recv(int fd, short event, void *arg)
{
	unsigned char buf[4096];
	int len;
	struct event *ev = arg;

	/* Reschedule this event */
	event_add(ev, NULL);

	struct wif *wi = _wi_in;
	struct tif *ti = _wfn->dv_ti;

	len = ti_read(ti, buf, sizeof(buf));
	if (len == -1) {
		perror("ti_read()");
		return;
	} else if (len == 0) {
		return;
	}
	if (_wfn->wifi_packet_ti_recv)
		_wfn->wifi_packet_ti_recv(_wfn, buf, len, wi);
}

/**
 * AP MSGS server send/recv functions 
 */
typedef struct ClientAddrInfo * pClientAddrInfo_t;
struct ClientAddrInfo
{
	struct event ev;
	struct sockaddr_in client_addr;
};

/* Set a socket to non-blocking mode. */
int setnonblock(int fd)
{
	int flags;

	flags = fcntl(fd, F_GETFL);
	if (flags < 0)	return flags;
	flags |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) < 0) return -1;

	return 0;
}

/* Open connection to server and send AP MSGS msg */
int ap_msgs_msg_send(void *buf, size_t count, IPADDR_TYPE(ip_address)) 
{
	DPRINT(DMAX2, "%s/ap_msgs_msg_send", __FILE__);
    int s, r;
    struct sockaddr_in serv_addr;
    struct hostent *phe;

	if ((s = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{
        DPRINT(DERR, "%s/ap_msgs_msg_send: Socket couldn't be created: %s", __FILE__ , strerror(errno));
        return -1;
    }

    /* Set options */
	char host_name[64];
	sprintf(host_name, ""f_IPADDR"", IPADDR(ip_address));
    /* Get IP for host name */
    phe = gethostbyname(host_name);

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(config.ap_msgs_port);
    memcpy(&serv_addr.sin_addr, phe->h_addr_list[0], phe->h_length);

    if ((r = connect(s, (struct sockaddr *)&serv_addr, sizeof(serv_addr)))) 
    {    	
        DPRINT(DERR, "%s/ap_msgs_msg_send: Connection not created: %s", __FILE__ , strerror(errno));
        close(s);
        return -1;
    }

    /* Send message to server */
    if (send(s, buf, count, 0) != count) 
    {
        DPRINT(DERR, "%s/ap_msgs_msg_send: Couldn't send message to server: %s", __FILE__ , strerror(errno));
        close(s);
        return -1;
    }
    close(s);
	return 0;
}

/* Client socket is ready for reading. */
void ap_msgs_msg_recv(int fd, short event, void *arg)
{
	DPRINT(DMAX2, "%s/ap_msgs_msg_recv", __FILE__);
	pClientAddrInfo_t client_info = (pClientAddrInfo_t) arg;
	u_char buf[8196];
	int len;

	len = read(fd, buf, sizeof(buf));
	if (len <= 0) 
	{
		if (len == 0)
		{
			/* Client disconnected, remove the read event and the free the client structure. */
			DPRINT(DMAX2, "%s/ap_msgs_msg_recv: Client disconnected.", __FILE__);
		}
		else if (len < 0) 
		{
			/* Some other error occurred, close the socket, remove the event and free the client structure. */
			DPRINT(DMAX2, "%s/ap_msgs_msg_recv: Socket failure, disconnecting client: %s", __FILE__ , strerror(errno));
		}
		close(fd);
		event_del(&client_info->ev);
		free(client_info);
		return;
	}
	if (_wfn->wifi_ap_msgs_msg_recv)
	{
		IPADDR_TYPE(ip_address);
		memcpy(ip_address, &client_info->client_addr.sin_addr, IPADDR_TYPE_SZ);
		_wfn->wifi_ap_msgs_msg_recv(_wfn, ip_address, buf, len);
	}
}

/**
 * This function will be called by libevent when there is a connection
 * ready to be accepted.
 */
void ap_msgs_msg_accept(int fd, short event, void *arg)
{
	DPRINT(DMAX2, "%s/ap_msgs_msg_accept", __FILE__);
	int client_fd;
	struct sockaddr_in client_addr;
	socklen_t client_len = sizeof(client_addr);
	pClientAddrInfo_t client_info;

	/* Accept the new connection. */
	client_fd = accept(fd, (struct sockaddr *)&client_addr, &client_len);
	if (client_fd == -1) 
	{
		DPRINT(DERR, "%s/ap_msgs_msg_accept: accept failed", __FILE__);
		return;
	}

	/* Set the client socket to non-blocking mode. */
	if (setnonblock(client_fd) < 0)
		DPRINT(DERR, "%s/ap_msgs_msg_accept: failed to set client socket non-blocking", __FILE__);

	/* We've accepted a new client, allocate a client object to
	 * maintain the state of this client. */
	client_info = calloc(1, sizeof(*client_info));
	if (client_info == NULL)
		DPRINT(DERR, "%s/ap_msgs_msg_accept: malloc failed", __FILE__);

	memcpy(&client_info->client_addr, &client_addr, sizeof(struct sockaddr_in));
	/* Setup the read event, libevent will call ap_msgs_on_read() whenever
	 * the clients socket becomes read ready.  We also make the
	 * read event persistent so we don't have to re-add after each
	 * read. */
	event_set(&client_info->ev, client_fd, EV_READ|EV_PERSIST, ap_msgs_msg_recv, client_info);

	/* Setting up the event does not activate, add the event so it
	 * becomes active. */
	event_add(&client_info->ev, NULL);

	DPRINT(DMAX2, "%s/ap_msgs_msg_accept: Accepted connection from %s", __FILE__, inet_ntoa(client_addr.sin_addr));
}

void ap_msgs_init(int *listen_fd)
{
	struct sockaddr_in listen_addr;
	int reuseaddr_on = 1;

	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0) 
	{
		DPRINT(DERR, "%s/ap_msgs_init: listen failed", __FILE__);
	}
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr_on, sizeof(reuseaddr_on)) == -1) 
	{
		DPRINT(DERR, "%s/ap_msgs_init: setsockopt failed", __FILE__);
	}

	memset(&listen_addr, 0, sizeof(listen_addr));
	listen_addr.sin_family = AF_INET;
	listen_addr.sin_addr.s_addr = INADDR_ANY;
	listen_addr.sin_port = htons(config.ap_msgs_port);

	if (bind(fd, (struct sockaddr *)&listen_addr, sizeof(listen_addr)) < 0)
	{
		DPRINT(DERR, "%s/ap_msgs_init: bind failed", __FILE__);
	}
	if (listen(fd, 5) < 0)
	{
		DPRINT(DERR, "%s/ap_msgs_init: listen failed", __FILE__);
	}
	
	/* Set the socket to non-blocking, this is essential in event
	 * based programming with libevent. */
	if (setnonblock(fd) < 0)
	{
		DPRINT(DERR, "%s/ap_msgs_init: failed to set server socket to non-blocking", __FILE__);
	}
	*listen_fd = fd;
}


/**
 * Periodic functions 
 */
void periodic_fn(int fd, short event, void *arg)
{
	struct timeval tv;
	struct EventArg *evarg = arg;
	DPRINT(DMAX2, "%s/periodic_fn: %ds %dus", __FILE__, _wfn->time_beacon / 1000000, _wfn->time_beacon % 1000000);

	if (_wfn->time_beacon)
	{
		tv.tv_usec = _wfn->time_beacon % 1000000; 
		tv.tv_sec  = _wfn->time_beacon / 1000000; 
		/* Reschedule this event */
		evtimer_add(&evarg->ev, &tv);
	}

	/* Send beacon */
	if (_wfn->wifi_ap_beacon) 
		_wfn->wifi_ap_beacon(_wfn);	
}

void periodic_change_channel(int fd, short event, void *arg)
{
	/* Monitor all channels */
	struct timeval tv;
	struct EventArg *evarg = arg;
	DPRINT(DMAX2, "%s/periodic_change_channel: %ds %dus", __FILE__, _wfn->time_monitor_channels / 1000000, _wfn->time_monitor_channels % 1000000);
	
	if (_wfn->time_monitor_channels)
	{
		tv.tv_usec = _wfn->time_monitor_channels % 1000000; 
		tv.tv_sec  = _wfn->time_monitor_channels / 1000000;
	
		/* Reschedule this event */
		evtimer_add(&evarg->ev, &tv);
	}

	if (_wfn->wifi_monitor_channels)
		_wfn->wifi_monitor_channels(_wfn);	
}

void periodic_scanning(int fd, short event, void *arg)
{
	/* Active Scanning */
	struct timeval tv;
	struct EventArg *evarg = arg;
	DPRINT(DMAX2, "%s/periodic_scanning: %ds %dus = %0.3fs / %.0fms", __FILE__, \
		_wfn->time_scanning / 1000000, _wfn->time_scanning % 1000000, \
		((float) _wfn->time_scanning) / 1000000, \
		((float) _wfn->time_scanning) / 1000);
	
	if (_wfn->time_scanning)
	{
		tv.tv_usec = _wfn->time_scanning % 1000000; 
		tv.tv_sec  = _wfn->time_scanning / 1000000; 
	
		/* Reschedule this event */
		evtimer_add(&evarg->ev, &tv);
	}

	if (_wfn->wifi_scanning)
		_wfn->wifi_scanning(_wfn, evarg->arg);	
}	

void pacmap_timer_fn(int fd, short event, void *arg)
{
	struct EventArg *evarg = (struct EventArg *) arg;

	if (_wfn->wifi_timer_fn)
	{
		_wfn->wifi_timer_fn(_wfn, evarg->arg);	
	}
	if (evarg)
	{		
		if (evarg->arg)
			free(evarg->arg);
		free(evarg);
	}
}	

void pacmap_add_timer(int timer_usec, void *arg, int sz_arg)
{
	/* Active Scanning */
	DPRINT(DMAX2, "%s/pacmap_add_timer: %ds %dus = %0.3fs / %.0fms", __FILE__, \
			timer_usec / 1000000, timer_usec % 1000000, \
			((float) timer_usec) / 1000000, ((float) timer_usec) / 1000);
	struct EventArg *evarg;
	if (timer_usec)
	{
		struct timeval tv;
		tv.tv_usec = timer_usec % 1000000; 
		tv.tv_sec = timer_usec / 1000000;
		
		/* Create structure */
		evarg = (struct EventArg *) malloc(sizeof(struct EventArg));
		memset(evarg, '\0', sizeof(struct EventArg));
		
		if (arg && sz_arg)
		{
			evarg->arg = (void *) malloc(sz_arg);
			memcpy(evarg->arg, arg, sz_arg);
		}

		/* Schedule this event */
		evtimer_set(&evarg->ev, pacmap_timer_fn, evarg);
		event_add(&evarg->ev, &tv);
	}
}

int wifi_set_channel(int channel)
{
	return wi_set_channel(_wi_out, channel);
}

int wifi_get_channel()
{
	return wi_get_channel(_wi_out);
}

