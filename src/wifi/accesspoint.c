/*
 *  Access point functionality
 *
 */

#include <stdlib.h>
#include <pacmap/wifi/protocol_80211.h>
#include <pacmap/wifi/ieee802_11.h>
#include "accesspoint.h"

pClient_t ap_client_create(MACADDR_TYPE(mac_address))
{
	pClient_t c = (pClient_t) malloc (sizeof(struct ST_info));
	bzero(c, sizeof(struct ST_info));
	memcpy(c->mac_address, mac_address, 6);
	// gettimeofday(&c->tv, NULL);
	return c;
}

void ap_client_delete(pClient_t client)
{
	if (client) 
		free(client);
	client = NULL;
}

int ap_get_seq_ctrl(pAP_t ap)
{
	int s = ap->seq_ctrl;
	ap->seq_ctrl += 16;
	ap->seq_ctrl %= 65536;
	return s;
}

void ap_update_client_rxinfo(pAP_t ap, MACADDR_TYPE(mac_address), int rssi)
{
	pClient_t c = ap_search_client(ap, mac_address);
	if (c) 
	{
		c->rssi = rssi;
		// gettimeofday(&c->tv, NULL);
	}
}

int ap_auth_client(pAP_t ap, MACADDR_TYPE(mac_address), MACADDR_TYPE(bssid))
{
	/* Check if client is already in the list */
	pClient_t c = ap_search_client(ap, mac_address);
	if (!c)
	{
		c = ap_add_client(ap, mac_address, bssid);
	}
	c->state = STA_AUTH_SUCCESS;
	memcpy(c->bssid, bssid, MACADDR_TYPE_SZ);
	return 1;
}

int ap_assoc_client(pAP_t ap, MACADDR_TYPE(mac_address), MACADDR_TYPE(bssid))
{
	/* Check if client is already authorized */
	pClient_t c = ap_search_client(ap, mac_address);
	if (!c || c->state <  STA_AUTH_SUCCESS)
	{
		return 0;
	}
	c->state = STA_ASSOC_SUCCESS;
	return 1;
}

pClient_t ap_add_client(pAP_t ap, MACADDR_TYPE(mac_address), MACADDR_TYPE(bssid))
{
	pClient_t client = ap_client_create(mac_address);
	memcpy(client->bssid, bssid, 6);

	if (ap->client_list.nb_clients == 0)
	{
		ap->client_list.first = client;
	}
	else
	{
		ap->client_list.last->next = client;		
	}
	
	ap->client_list.last = client;
	ap->client_list.current = client;
	ap->client_list.nb_clients++;
	return client;
}

pClient_t ap_get_first_client(pAP_t ap)
{
	if (ap)
	{
		ap->client_list.current = ap->client_list.first;
		return ap->client_list.first;
	}	
	return NULL;
}

pClient_t ap_get_next_client(pAP_t ap)
{
	if (ap && ap->client_list.current)
	{
		ap->client_list.current = ap->client_list.current->next;
		return ap->client_list.current;
	}	
	return NULL;
}

pClient_t ap_search_client(pAP_t ap, MACADDR_TYPE(mac_address))
{
	pClient_t c = ap->client_list.first;

	/* Search client */
	if (c) 
	{
		while (c && !is_same_mac(mac_address, c->mac_address)) 
		{
			c = c->next;
		}
	}
	return c;
}

int ap_delete_client(pAP_t ap, MACADDR_TYPE(mac_address))
{
	pClient_t c = ap->client_list.first;
	pClient_t prev = NULL;

	/* Search client */
	if (c) 
	{
		while (c && !is_same_mac(mac_address, c->mac_address)) 
		{
			prev = c;
			c = c->next;
		}
	}

	if (c && is_same_mac(mac_address, c->mac_address))
	{
		/* Arrange pointers */
		if (ap->client_list.first == c)
		{
			ap->client_list.first = c->next;
			ap->client_list.current = ap->client_list.first;
		}
		/* Client could be the only one */
		if (ap->client_list.last == c)
		{
			ap->client_list.last = prev;
			ap->client_list.current = prev;
		}
		/* Client is in the middle */
		if (prev)
		{
			prev->next = c->next;
			ap->client_list.current = prev;
		}
		ap->client_list.nb_clients--;
		ap_client_delete(c);
		return 1;
	}
	return 0;
}

int ap_queue_is_empty(pAP_t ap)
{
	return (ap->packet_queue.nb_packets == 0);
}
void ap_queue_insert_packet(pAP_t ap, void* buf, size_t count, int rate)
{
	pPacket_t packet = (pPacket_t) malloc(sizeof(struct ePacket));
	memset(packet, '\0', sizeof(struct ePacket));
	
	/* Copy data */
	memcpy(packet->buffer, buf, sizeof(packet->buffer));
	packet->length = count;
	packet->rate = rate;
	packet->next = NULL;
	
	if (ap->packet_queue.last)
	{
		ap->packet_queue.last->next = packet;
	}
	else
	{
		ap->packet_queue.first = packet;
	}
	ap->packet_queue.last = packet;
	ap->packet_queue.nb_packets++;
	DPRINT(DDEBUG, "ap_queue_insert_packet %d sizeof buf %d", ap->packet_queue.nb_packets, sizeof(packet->buffer));
}

int ap_queue_remove_packet(pAP_t ap, pPacket_t packet)
{
	if (ap->packet_queue.first)
	{	
		/* Copy the packet */
		memset(packet, '\0', sizeof(struct ePacket));
		pPacket_t p = ap->packet_queue.first;
		memcpy(packet->buffer, p->buffer, p->length);
		packet->length = p->length;
		packet->rate = p->rate;
	
		/* Remove packet from queue */
		p = ap->packet_queue.first;
		ap->packet_queue.first = ap->packet_queue.first->next;
		free(p);
		ap->packet_queue.nb_packets--;
		
		/* Empty queue ?*/
		if (ap_queue_is_empty(ap))
		{
			ap->packet_queue.last = NULL;
		}
	}
	DPRINT(DDEBUG, "ap_queue_remove_packet %d sizeof %d", ap->packet_queue.nb_packets, sizeof(struct ePacket));
	return ap->packet_queue.nb_packets;
}

int ap_client_check_timeout(pAP_t ap, pClient_t client)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	if (tv.tv_sec - client->tv.tv_sec > CLIENT_TIMEOUT_SEC)
	{
		ap_delete_client(ap, client->mac_address);
		client = NULL;
		return 1;
	}
	return 0;
}

void ap_free(pAP_t ap)
{
	pClient_t c, f;
	c = ap->client_list.first;
	while (c)
	{	
		f = c;
		c = f->next;
		ap_client_delete(f);
	}
}

/*  Accesspoint creation & destruction */
void ap_init(pAP_t ap, struct ConfigValues *config)
{
	/*  at this point, we have to fill AP_conf structure	 */
	memset(ap, 0, sizeof(struct AP_conf));

	memcpy(ap->bssid, config->mac_address, MACADDR_TYPE_SZ);
	memcpy(ap->mac_address, config->mac_address, MACADDR_TYPE_SZ);
	memcpy(ap->essid, config->essid, strlen(config->essid));
	memcpy(ap->ip_address, config->ip_address, IPADDR_TYPE_SZ);
	memcpy(ap->br_macaddr, config->br_macaddr, MACADDR_TYPE_SZ);
	// ap->rate = rate;
	ap->channel = config->channel;
	ap->rssi_threshold = config->rssi_threshold;
	
	/* Client list */
	ap->client_list.first = NULL;
	ap->client_list.last = NULL;
	ap->client_list.current = NULL;
	ap->client_list.nb_clients = 0;
	
	ap->interval = 0x064;
	ap->capability = 0x401;
	ap->seq_ctrl = 0;

	int i = 0;
	memset(ap->rates_supp, 0, sizeof(ap->rates_supp)); 
	memset(ap->rates_supp + i++, RATE_1M/500000 | 0x80, 1); 
	memset(ap->rates_supp + i++, RATE_2M/500000 | 0x80, 1);
	memset(ap->rates_supp + i++, RATE_5_5M/500000 | 0x80, 1); 
	memset(ap->rates_supp + i++, RATE_11M/500000 | 0x80, 1); 
	memset(ap->rates_supp + i++, RATE_18M/500000, 1); 
	memset(ap->rates_supp + i++, RATE_24M/500000, 1); 
	memset(ap->rates_supp + i++, RATE_36M/500000, 1); 
	memset(ap->rates_supp + i++, RATE_54M/500000, 1); 
	
	i = 0;
	memset(ap->rates_supp_ext, 0, sizeof(ap->rates_supp_ext)); 
	memset(ap->rates_supp_ext + i++, RATE_6M/500000, 1); 
	memset(ap->rates_supp_ext + i++, RATE_9M/500000, 1); 
	memset(ap->rates_supp_ext + i++, RATE_12M/500000, 1); 
	memset(ap->rates_supp_ext + i++, RATE_48M/500000, 1); 
	
}
