#ifndef __PACMAP_H__
#define __PACMAP_H__

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <pacmap/verbose.h>

// #define RTC_RESOLUTION 512
#define DEFAULT_WIRELESS_IFACE	"ath0"
#define DEFAULT_ESSID			"pacmap"
#define DEFAULT_CHANNEL			6
#define DEFAULT_IP_ADDRESS		"192.168.1.13"
#define DEFAULT_AP_MSGS_PORT	31416
#define DEFAULT_RSSI_THRESHOLD	170

#define EID_SUPPORTED_RATES_LENGHT 8
#define EID_SUPPORTED_RATES_EXT_LENGHT 4

#define MACADDR_TYPE(x) u_int8_t x[6]
#define MACADDR_TYPE_SZ (sizeof(u_int8_t)*6)
#define f_MACADDR "%02X:%02X:%02X:%02X:%02X:%02X"
#define MACADDR(m) (m)[0], (m)[1], (m)[2], (m)[3], (m)[4], (m)[5]
#define MAC_ADDRESS_NULL (u_int8_t *) "\0\0\0\0\0\0"

#define IPADDR_TYPE(x) u_int8_t x[4]
#define IPADDR_TYPE_SZ (sizeof(u_int8_t)*4)
#define f_IPADDR "%d.%d.%d.%d"
#define IPADDR(i) (i)[0], (i)[1], (i)[2], (i)[3]
#define IP_IS_NULL(i) (((i)[0] == 0) && ((i)[1] == 0) && ((i)[2] == 0) && ((i)[3] == 0))
#define IP_ADDRESS_NULL (u_int8_t *) "\0\0\0\0"
#define IPADDR_BROADCAST (u_int8_t *) "\377\377\377\377"
#define FALSE 0
#define TRUE 1

struct devices
{
	// Input/Output devices
	int fd_in, arptype_in;
	int fd_out, arptype_out;
	// int fd_rtc;
	int ti_in, ti_out;
}
dev;

struct wif *_wi_in, *_wi_out;
struct wifi_fn_t *_wfn;
struct tif *_ti_in, *_ti_out;

struct Device_t
{
	MACADDR_TYPE(mac_address);
	IPADDR_TYPE(ip_address);
};

struct ConfigValues
{
	MACADDR_TYPE(mac_address);
	IPADDR_TYPE(ip_address);
	MACADDR_TYPE(br_macaddr);
	char essid[120];
	char essid_prefix[120];
	char wifi_iface[10];
	int verbose;
	int channel;
	int ap_msgs_port;
	int ap_neighbors_nb;
	int stations_nb;
	struct Device_t *ap_neighbors;
	struct Device_t *stations;
	u_int8_t rssi_threshold;
} config;

struct wifi_fn_t {
	/* Device functions for received packets */
	void (*wifi_st_assoc_request)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_st_assoc_response)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_st_disassoc)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
	void (*wifi_st_reassoc_request)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_st_reassoc_response)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_st_probe_request)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_st_probe_response)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_st_beacon)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_st_auth)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_st_deauth)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_ctrl_rts)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_ctrl_cts)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_ctrl_ack)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
	void (*wifi_data_data)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
	void (*wifi_data_nodata)(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
	void (*wifi_ti_recv)(struct wifi_fn_t *wfn, u_char *packet, int length);		

	/* Device periodic functions */
	void (*wifi_periodic)(struct wifi_fn_t *wfn, u_char *packet, int length);		
	void (*wifi_timer_fn)(struct wifi_fn_t *wfn, void *arg);
	void (*wifi_ap_beacon)(struct wifi_fn_t *wfn);
	void (*wifi_monitor_channels)(struct wifi_fn_t *wfn);
	void (*wifi_scanning)(struct wifi_fn_t *wfn, void *arg);

	/* ap_msgs functions */
	void (*wifi_ap_msgs_scan_request)(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length);
	void (*wifi_ap_msgs_scan_response)(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length);
	void (*wifi_ap_msgs_client_move)(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length);
	
	/* Protocol functions */
	int (*wifi_packet_recv)(struct wifi_fn_t *wfn, u_char *packet, int length, struct wif *wi, int rssi);		
	int (*wifi_packet_ti_recv)(struct wifi_fn_t *wfn, u_char *packet, int length, struct wif *wi);		
	int (*wifi_ap_msgs_msg_recv)(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length);

	void (*wifi_free)(struct wifi_fn_t *wfn);		
	void (*free)(struct wifi_fn_t *wfn);		

	void *wfn_priv;
	struct tif *dv_ti;
	struct wif *dv_wi;
	void *evtimer;
	int time_beacon;
	int time_monitor_channels;
	int time_scanning;		
	int rate;
};

int pacmap_init(int argc, char** argv, struct wifi_fn_t *(*wifi_init)(struct ConfigValues *config));

int wifi_set_channel(int channel);
int wifi_get_channel();

void ato_mac_address(MACADDR_TYPE(macaddr), char *s);
void ato_ip_address(IPADDR_TYPE(macaddr), char *s);
int is_same_mac(MACADDR_TYPE(a), MACADDR_TYPE(b));
int is_same_ip(IPADDR_TYPE(a), IPADDR_TYPE(b));

#endif
