#!/usr/bin/python

"""
@maru
05/01/2010	Unable to join the network "Client-00:25:00:02:c8:30"
"""

import psyco
import pacmap
try:
	from scapy.all import *
except:
	from scapy import *
import signal
import time, calendar,datetime 
import random
import os

# Parameters for the AP
tap_ip_address="192.168.10.10"
tap_mac_address="00:11:22:33:44:55"
bssid = "00:11:22:33:44:55"
floor = 15 # seuil de detection de mobilite
expiracy = 10 # expiration des clients connectes
delta = 2 # nombre de balises recues avant disparition locale du terminal

# global variables
timestamp = 0
index=-1



def handler(signal, stackframe):
    print "[py] OUCH"

signal.signal(signal.SIGSEGV, handler)




class Client:
    
    def __init__(self, mac, bssid):
        self.bssid = bssid
        self.mac = mac
        self.timestamp = time.mktime(datetime.datetime.now().timetuple())
        self.sequence = 0
        self.lastsequence = 0
        self.state = "unknown"
        self.rssi = 0
        r = random.randint(10,100)
        self.ip = "192.168.10.%d"  % r
        
    def getIP(self):
        return self.ip
        
    def setIP(self,ip):
        self.ip=ip

    def getBSSID(self):
        return self.bssid

    def setBSSID(self,bssid):
        """docstring for setBSSID"""
        self.bssid = bssid
        
    def getLastRSSI(self):
        """docstring for getLastRSSI"""
        return self.rssi
        
    def setLastRSSI(self,rssi):
       """docstring for getLastRSSI"""
       self.rssi=rssi
        
    def getTimestamp(self):
        return self.timestamp
        
    def setTimestamp(self,timestamp):
        self.timestamp=timestamp
        
    def getLastSeq(self):
            return self.lastsequence

    def setLastSeq(self,seq):
            self.lastsequence=seq    
    
    def getSeq(self):
        return self.sequence
    
    def setSeq(self,seq):
        self.sequence=seq
        
    def getMAC(self):
        return self.mac
    
    def getBSSID(self):
        return self.bssid
        
    def setBSSID(self, remotebssid):
        self.bssid = remotebssid
    
    def getState(self):
        return self.state
    
    def setState(self,state):
        self.state=state
        
class ClientList:
    
    def __init__(self):
        self.data = []
        self.mac = []
        self.index = -1
        
    def isPresent(self, mac):
        i = -1
        try:
            while 1:
                i = self.mac.index(mac, i+1)
        except ValueError:
            pass
        return i
        
    def add(self,mac,current_bssid):
        i = self.isPresent(mac)
        if i < 0:
            #print "[py] Adding new client %s" % mac
            client = Client(mac,current_bssid)
            self.mac.append(mac)
            self.data.append(client)
#        else:
#            print "[py] Client already present"
       
    def delete(self,mac):
        i = self.isPresent(mac)
        if i>=0:
            #print "[py] Remove client %s" % mac
            del self.mac[i]
            del self.data[i]
            
            
    def __iter__(self): 
        self.index = len(self.data)
        return self

    def next(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]
        
    def getClient(self,mac):
        i = self.isPresent(mac)
        if i>=0:
            return self.data[i]
        else:
            return
    
    def getLength(self):
        return len(self.mac)

# clients connected to our AP
clients_list = ClientList()

# clients to be watched, connected to other APs
watch_list = ClientList()



# PACMAP : function called at launch time
def background():
    global bssid
    global index
    global tap_ip_address
    global tap_mac_address
    #bssid = "00:11:22:33:44:55" #str2mac(pacmap.getbssid())
    print "[py] BSSID from MAC Adapter is %s" % bssid
    print "[py] Virtual AP 0.1 native version by Yan Grunenberger"
    print "[py] Written using PacMap"
    index=pacmap.createtap()
    pacmap.settapmac(index,mac2str(tap_mac_address)) #bssid))
    #pacmap.settapip(index,tap_ip_address)
    return 0

# PACMAP : Example of periodic function call : each second, we display a message
#timestamp = 0
# PACMAP : Resolution of timer is 1/512s
def periodic(float1,float2,float3):
    global timestamp
    global expiracy
    i = clients_list.getLength()
    if i == 0:
        i = 1
    beacon_interval = 0x0064 * i
    periodicity = 11.25 
    result = int(float1/512 * periodicity)
    if result > timestamp:
        timestamp = result
        
        for client in clients_list:
            current_timestamp = time.mktime(datetime.datetime.now().timetuple())*1e6+datetime.datetime.now().microsecond
            seq = client.getSeq()
            ssid = "Client-%s" % client.getMAC()
            rssi = "%d" % client.getLastRSSI()
            current_bssid =  client.getBSSID()
            dot11_answer = Dot11(
            FCfield ="retry",
            SC= (seq<<4 & 0xff) |( (seq>>4 & 0xff) << 8 & 0xff00) ,
        	type = "Management",
        	addr1 = "ff:ff:ff:ff:ff:ff",
        	addr2 = current_bssid,
        	addr3 = current_bssid)/Dot11Beacon(timestamp = current_timestamp,cap = 0x0104,beacon_interval = beacon_interval )/Dot11Elt(ID=0,info=ssid)/Dot11Elt(ID=1,info="\x82")/Dot11Elt(ID=3,info="\x06")/Dot11Elt(ID=221,info=rssi)/Dot11Elt(ID=221,info=client.getIP())
            #print "[py] Sending beacon to client %s" % client.getMAC()
            
            pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
            if seq == 4095:
                client.setSeq(0)
            else:
                client.setSeq(seq+1)
            
            diff = timestamp - client.getTimestamp()
            
            if diff>expiracy:
               clients_list.delete(client.getMAC())
  
    return 0

# PACMAP : Function called at packet reception
def packet_recv(packet, length):
    return 0

# PACMAP : Function called at packet reception on tap interface
def packet_ti_recv(packet, length,index):
    ether_frame = Packet(packet)
    ether_frame.decode_payload_as(Ether)
    
    addr1 = ether_frame.getlayer(Ether).dst
    addr2 = bssid
    addr3 = ether_frame.getlayer(Ether).src
    
    if ether_frame[ARP] and ether_frame[ARP].op == 1:
        print "[py] TAP ARP who-has %s "%(ether_frame[ARP].pdst)
        for client in clients_list:
            if client and (client.getIP() == ether_frame[ARP].pdst):
                print "[py] TAP ARP who-has %s -> reply %s" %(client.getIP(),client.getMAC())
                eth_sent_frame = Ether(dst=addr3,src=client.getMAC(),type=ether_frame.getlayer(Ether).type)/ARP(op=2,hwsrc=client.getMAC(),psrc=client.getIP(),hwdst=addr3,pdst=ether_frame[ARP].psrc)
                pacmap.sendtipacket(str(eth_sent_frame),len(str(eth_sent_frame)),index)
                return 1
    
    client = clients_list.getClient(addr1)
    if client:
        addr2 = client.getBSSID()    

    dot11_answer=Dot11(type = "Data",
    addr1 = addr1,
    addr2 = addr2,
    addr3 = addr3,
    FCfield=0x82)/LLC()/SNAP(code=ether_frame.getlayer(Ether).type)/ether_frame.getlayer(Ether).payload
    
    if (client and addr1 != "ff:ff:ff:ff:ff:ff"):
        pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
    return 1

# PACMAP : Function called if 802.11 probe request detected
def proto80211_probereq(packet, length):
    #print "[py] Probe request received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    clientmac = dot11_frame.getlayer(Dot11).addr2
    
    # generate a custom SSID function of mac add of the client
    ssid = "Client-%s" % clientmac
    #print "[py] Scapy interpretation %s" % dot11_frame.summary()
    current_timestamp = time.mktime(datetime.datetime.now().timetuple())*1e6+datetime.datetime.now().microsecond
    
    if dot11_frame.getlayer(Dot11).addr3 == "ff:ff:ff:ff:ff:ff":
        dot11_answer = Dot11(
	type = "Management",
	addr1 = dot11_frame.getlayer(Dot11).addr2,
	addr2 = bssid,
	addr3 = bssid)/Dot11ProbeResp(timestamp=current_timestamp,cap = 0x0104)/Dot11Elt(ID=0,info=ssid)/Dot11Elt(ID=1,info="\x82")/Dot11Elt(ID=3,info="\x06")
    
        pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
    else:
        client = clients_list.getClient(clientmac)
        if client:
            dot11_answer = Dot11(
    	type = "Management",
    	addr1 = dot11_frame.getlayer(Dot11).addr2,
    	addr2 = client.getBSSID(),
    	addr3 = client.getBSSID())/Dot11ProbeResp(timestamp=current_timestamp,cap = 0x0104)/Dot11Elt(ID=0,info=ssid)/Dot11Elt(ID=1,info="\x82")/Dot11Elt(ID=3,info="\x06")

            pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
    
    # PACMAP : return value of 0 for passthrough mode
    return 1

	# PACMAP : Function called if 802.11 probe request detected
def proto80211_auth(packet, length):
    global bssid
    print "[py] Authentification received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)

    if (dot11_frame.getlayer(Dot11).addr3 == bssid):
 
        if (dot11_frame.getlayer(Dot11Auth).seqnum == 0x01):
            dot11_answer = dot11_frame
            addr1=  dot11_frame.getlayer(Dot11).addr2
            dot11_answer.getlayer(Dot11).addr1= addr1
            dot11_answer.getlayer(Dot11).addr2= bssid
            dot11_answer.getlayer(Dot11Auth).seqnum=0x02
            pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
            
    return 1

def proto80211_deauth(packet, length):
    print "[py] Deauthentification received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    #print "[py] Scapy interpretation %s" % dot11_frame.summary()
    client = dot11_frame.getlayer(Dot11).addr2
    clients_list.delete(client)
    return 1


def proto80211_assocreq(packet, length):
    print "[py] Association request received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    # check bssid
    if (dot11_frame.getlayer(Dot11).addr3 == bssid):
        dot11_answer = dot11_frame
        client = dot11_frame.getlayer(Dot11).addr2
        ssid = "Client-%s" % client
        dot11_answer = Dot11(type = "Management",addr1 = dot11_frame.getlayer(Dot11).addr2,addr2 = bssid, addr3 = bssid)/Dot11AssoResp(cap = 0x0104, AID=0xc001)/Dot11Elt(ID=0,info=ssid)/Dot11Elt(ID=1,info="\x02")
        pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
        clients_list.add(client,bssid)


            
    return 1
    
def proto80211_beacon(packet, length, essid):
    global floor
    global delta
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    
    if (essid.startswith("Client-")):
        
        clientmac = essid.replace("Client-", "")

        # Get current sequence number
        seq = (dot11_frame[Dot11].SC >> 4 )
        
        client = clients_list.getClient(clientmac)
        if client:
            if (seq > client.getLastSeq()+delta ):
                print "[py] Client with seq %d was transfered, cancel registration here with seq %d" % (client.getLastSeq(),seq)
                arp_flush = "arp -d %s" % client.getIP()
                clients_list.delete(clientmac)    
                os.system(arp_flush)
 
        current_payload = dot11_frame.getlayer(Dot11Beacon).payload
        
        current_bssid = dot11_frame.getlayer(Dot11).addr3

        while (current_payload[Dot11Elt] and current_payload.getlayer(Dot11Elt).ID != 221):
            current_payload = current_payload.payload
            
        lastRSSI = int(current_payload.getlayer(Dot11Elt).info)
     
        current_payload = current_payload.payload   
        ip = current_payload.getlayer(Dot11Elt).info
        
        watch_list.add(clientmac,current_bssid)
        watched_client = watch_list.getClient(clientmac)
        watched_client.setBSSID(current_bssid)
        print "[py] %d.%06d %d %d " % (time.mktime(datetime.datetime.now().timetuple()),datetime.datetime.now().microsecond,watched_client.getLastRSSI(),lastRSSI)
        if (watched_client.getLastRSSI()-lastRSSI) > floor:
            print "[py] Transfering terminal to this AP"
            clients_list.add(clientmac,current_bssid)
            client = clients_list.getClient(clientmac)
            timestamp = time.mktime(datetime.datetime.now().timetuple())+datetime.datetime.now().microsecond/1e6
            client.setTimestamp(timestamp)
            client.setBSSID(current_bssid)
            print "[py] previous AP was : %s with ip %s" % (current_bssid ,ip)
            client.setLastRSSI(lastRSSI)
            client.setIP(ip)
            arp_flush = "arp -d %s" % client.getIP()
            os.system(arp_flush)
            if seq == 4095:
                client.setSeq(0)
                client.setLastSeq(0)
            else:
                client.setSeq(seq)
                client.setLastSeq(seq)
            watch_list.delete(clientmac)
    
    return 0


def proto80211_ctrl(packet, length):
    return 0

def proto80211_data(packet, length):
    global index
    #print "[py] Data packet received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    
    mac = dot11_frame.getlayer(Dot11).addr2
    
    watched_client = watch_list.getClient(mac)
    
    if watched_client:
        watched_client.setLastRSSI(pacmap.getrxinfo(1))
    
    
    if (dot11_frame.getlayer(Dot11).addr1[2:] == bssid[2:]):
        
        client = clients_list.getClient(mac)
        if client:
            timestamp = time.mktime(datetime.datetime.now().timetuple())+datetime.datetime.now().microsecond/1e6
            client.setTimestamp(timestamp)
            client.setLastRSSI(pacmap.getrxinfo(1))
        
            #print "[py] Data packet with power %d, channel %d, rate %d" % (pacmap.getrxinfo(2)-pacmap.getrxinfo(1),pacmap.getrxinfo(3),pacmap.getrxinfo(5))
        
        
        # Trafic interpretation
            if dot11_frame[ARP] and dot11_frame[ARP].op == 1 and dot11_frame[ARP].pdst==tap_ip_address:
                print "[py] Wireless ARP who-has -> reply"
                dot11_answer=Dot11(type = "Data",addr1 = mac,addr2 = client.getBSSID(),addr3 = tap_mac_address,FCfield=0x02)/LLC()/SNAP()/ARP(op=2,hwsrc=tap_mac_address,psrc=tap_ip_address,hwdst=dot11_frame[ARP].hwsrc,pdst=dot11_frame[ARP].psrc)
                pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
                return 1
        
            # DHCP Management 
            if dot11_frame[DHCP] and dot11_frame[DHCP].options[0][1] == 1:
                print "[py] DHCPDISCOVER"
                #
                dot11_answer=Dot11(type = "Data",addr1 = mac,addr2 = client.getBSSID(),addr3 = tap_mac_address,FCfield=0x02)/LLC()/SNAP()/IP(src=tap_ip_address,dst=client.getIP())/UDP(sport=67,dport=68)/BOOTP(op=2, ciaddr="0.0.0.0",yiaddr=client.getIP(),siaddr="0.0.0.0",giaddr=tap_ip_address,chaddr=mac2str(mac), xid=dot11_frame[BOOTP].xid)/DHCP(options=[('message-type','offer'),('server_id',tap_ip_address), ('lease_time',43200), ('renewal_time',21600),('rebinding_time',37800),('subnet_mask',"255.255.255.0"),('end')]) #('router',tap_ip_address),('name_server',tap_ip_address)
            
                pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
                return 1
        
            if dot11_frame[DHCP] and dot11_frame[DHCP].options[0][1] == 3:
            
                print "[py] DHCPREQUEST -> REPLY with %s" % client.getIP()
                    # answer with dhcp ACK
                dot11_answer=Dot11(type = "Data",addr1 = mac,addr2 = client.getBSSID(),addr3 = tap_mac_address,FCfield=0x02)/LLC()/SNAP()/IP(src=tap_ip_address,dst=client.getIP())/UDP(sport=67,dport=68)/BOOTP(op=2, ciaddr="0.0.0.0",yiaddr=client.getIP(),siaddr="0.0.0.0",giaddr=tap_ip_address,chaddr=mac2str(mac), xid=dot11_frame[BOOTP].xid)/DHCP(options=[('message-type','ack'),('server_id',tap_ip_address), ('lease_time',43200), ('renewal_time',21600),('rebinding_time',37800),('subnet_mask',"255.255.255.0"),('end')])
            
                pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x02)
                return 1
        
            if dot11_frame.haslayer(SNAP):
                eth_sent_frame = Ether(dst=dot11_frame.getlayer(Dot11).addr3,src=dot11_frame.getlayer(Dot11).addr2,type=dot11_frame.getlayer(SNAP).code)
                eth_sent_frame.payload = dot11_frame.getlayer(SNAP).payload
                    
                pacmap.sendtipacket(str(eth_sent_frame),len(str(eth_sent_frame)),index)

        else:
            "Data from unknown station"
    else:
        "Data for another BSSID"
    return 1

