#ifndef __PACMAP_MULTICHANNELVAP_H__
#define __PACMAP_MULTICHANNELVAP_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>
#include <pacmap/wifi/virtualap.h>

#define CHANNEL_SWITCH_COUNT 3
#define NEIGHB_CHANN_TIMER		BEACON_INTERVAL_TIMER*1.5
#define TIME_MONITOR_CHANNELS	BEACON_INTERVAL_TIMER*15

void mcvap_monitor_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_monitor_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_monitor_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_monitor_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_monitor_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_monitor_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_monitor_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_monitor_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_monitor_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_monitor_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_monitor_ap_beacon(struct wifi_fn_t *wfn);
void mcvap_monitor_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length);	
void mcvap_monitor_monitor_channels(struct wifi_fn_t *wfn);
void mcvap_monitor_free(struct wifi_fn_t *wfn);

struct wifi_fn_t *mcvap_monitor_init(struct ConfigValues *config);

#endif /* __PACMAP_MULTICHANNELVAP_H__ */
