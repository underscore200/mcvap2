/*
 *  802.11 access point
 *
 */

#ifndef __PACMAP_AP_80211_H__
#define __PACMAP_AP_80211_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>

void ap_80211_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void ap_80211_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void ap_80211_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void ap_80211_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void ap_80211_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void ap_80211_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void ap_80211_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void ap_80211_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void ap_80211_ap_beacon(struct wifi_fn_t *wfn);
void ap_80211_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length);	
void ap_80211_free(struct wifi_fn_t *wfn);

struct wifi_fn_t *ap_80211_init(struct ConfigValues *config);

#endif
