/*
 *  Main function of pacmap: pacmap_init
 *
 */

#ifdef linux
	#include <linux/rtc.h>
#endif
#include <event.h>
#include <fcntl.h>
#include <getopt.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <pacmap/pacmap.h>
#include "osdep/osdep.h"
#include "pacmap_core.h"

int verbosity = DVERBOSE;
time_t init_timer = 0;
int32_t thiszone;
FILE *fp_log;
struct event_base* eb = NULL;

char *verbosity_level[] = { "DCONSOLE", "DERR", "DINFO", "DVERBOSE", "DDEBUG", "DMAX", "DMAX1", "DMAX2" };

char usage_text[] =
	"\n"
	"  Pacmap - Packet Manipulator for 802.11 access points\n"
	"  http://drakkar.imag.fr/\n"
	"\n"
	"  usage: %s [options] <interface>\n"
	"\n"
	"  Options:\n"
	"\n"
//	"      -a mac_address   : sets Access Point MAC address\n"
//	"      -c channel       : sets the channel the AP is running on\n"
//	"      -e essid         : sets Access Point essid\n"
//	"      -i ip_address    : sets IP address to the tap interface\n"
	"      -f filename      : configuration file name\n"
	"      -v               : verbose (prints more messages)\n"
	"\n"
	"      --help           : displays this usage screen\n"
	"\n";
int parse_arguments(int argc, char** argv, char *config_filename)
{
	int c;
	int option_index = 0;

	/* Parse arguments to set options */
	while (1)
	{
		static struct option long_options[] = {
			{"mac_address",	1, 0, 'a'},
			{"channel",	 	1, 0, 'c'},
			{"essid",	 	1, 0, 'e'},
			{"ip_address",	1, 0, 'i'},
			{"filename",	1, 0, 'f'},
			{"verbose",	 	0, 0, 'v'},
			{"help",	 	0, 0, 'h'},
            {0, 0, 0, 0}
        };

		// "a:c:e:i:vh"
		c = getopt_long(argc, argv, "f:vh", long_options, &option_index);

		if (c < 0) break;

		switch (c)
		{
			case 'a': memcpy(config.mac_address, optarg, MACADDR_TYPE_SZ); break;
			case 'c': config.channel = atoi(optarg); break;
			case 'e': memcpy(config.essid, optarg, strlen(optarg)); break;
			case 'i': memcpy(config.ip_address, optarg, IPADDR_TYPE_SZ); break;

			case 'f': 
				memcpy(config_filename, optarg, strlen(optarg)); 
				break;

			case 'v':
				verbosity++;
				break;

			case 'h':
			default:
				printf(usage_text, argv[0]);
				return 1;
		}
	}
	if (verbosity > DMAX) verbosity = DMAX;
	/* if (argc - optind != 1) {
		if (argc >= 1) {
			printf(usage_text, argv[0]);
			if (argc - optind > 1) {
				DPRINT(DERR, "Too many arguments.");
			}
		}
		if (argc - optind == 0) {
			DPRINT(DERR, "No I/O interface specified.");
		}
		return 1;
	} */
	DPRINT(DDEBUG, "Options read, verbosity level: %s", verbosity_level[verbosity]);
	return 0;
}

/**
 * Read configuration from file pacmap.conf 
 */
#define MAX_KEYWORD	12
char keyword_list[MAX_KEYWORD][200] = { "INTERFACE", "CHANNEL", "MAC_ADDRESS", "IP_ADDRESS", "BR_MACADDR", "ESSID", "ESSID_PREFIX", "AP_MSGS_PORT", "NEIGHBORS", "STATIONS", "RSSI_THRESHOLD", "" };
enum { CONF_INTERFACE = 0, CONF_CHANNEL, CONF_MAC_ADDRESS, CONF_IP_ADDRESS, CONF_BR_MACADDR, CONF_ESSID, CONF_ESSID_PREFIX, CONF_AP_MSGS_PORT, CONF_NEIGHBORS, CONF_STATIONS, CONF_RSSI_THRESHOLD };

int read_config(char *filename, struct ConfigValues *config)
{
	char keyword[200], buffer[200];
	int i;

	/* Default values */
	strcpy(config->wifi_iface, DEFAULT_WIRELESS_IFACE);
	config->channel = DEFAULT_CHANNEL;
	ato_ip_address(config->ip_address, DEFAULT_IP_ADDRESS);
	strcpy(config->essid, DEFAULT_ESSID);
	config->ap_msgs_port = DEFAULT_AP_MSGS_PORT;
	config->rssi_threshold = DEFAULT_RSSI_THRESHOLD;

	if (!strlen(filename)) strcpy(filename, "pacmap.conf");
	FILE *fd_config = fopen(filename, "r"); 
	if (!fd_config)
	{ 
		DPRINT(DERR, "%s not found.", filename);
		return -1;
	}

	memset(keyword, '\0', sizeof(keyword));
	while (fscanf(fd_config, "%s", keyword) >= 0)
	{
		i = 0;
		while (strcmp(keyword, keyword_list[i]) && i < MAX_KEYWORD) i++;
		switch (i)
		{
			case CONF_INTERFACE:
				fscanf(fd_config, "%s", config->wifi_iface);
				break;
			case CONF_CHANNEL:
				fscanf(fd_config, "%d", &config->channel);
				break;
			case CONF_MAC_ADDRESS:
				fscanf(fd_config, "%s", buffer);
				ato_mac_address(config->mac_address, buffer);
				break;
			case CONF_IP_ADDRESS:
				fscanf(fd_config, "%s", buffer);
				ato_ip_address(config->ip_address, buffer);
				break;
			case CONF_BR_MACADDR:
				fscanf(fd_config, "%s", buffer);
				ato_mac_address(config->br_macaddr, buffer);
				break;
			case CONF_ESSID:
				fscanf(fd_config, "%s", config->essid);
				break;
			case CONF_ESSID_PREFIX:
				fscanf(fd_config, "%s", config->essid_prefix);
				break;
			case CONF_NEIGHBORS:
				fscanf(fd_config, "%d", &config->ap_neighbors_nb);
				config->ap_neighbors = (struct Device_t *) malloc(sizeof(struct Device_t)*config->ap_neighbors_nb);
				for (i = 0; i < config->ap_neighbors_nb; i++)
				{
					fscanf(fd_config, "%s", buffer);
					ato_mac_address(config->ap_neighbors[i].mac_address, buffer);
					fscanf(fd_config, "%s", buffer);
					ato_ip_address(config->ap_neighbors[i].ip_address, buffer);
				}
				break;
			case CONF_STATIONS: 
				fscanf(fd_config, "%d", &config->stations_nb);
				config->stations = (struct Device_t *) malloc(sizeof(struct Device_t)*config->stations_nb);
				for (i = 0; i < config->stations_nb; i++)
				{
					fscanf(fd_config, "%s", buffer);
					ato_mac_address(config->stations[i].mac_address, buffer);
					fscanf(fd_config, "%s", buffer);
					ato_ip_address(config->stations[i].ip_address, buffer);
				}
				break;
			case CONF_RSSI_THRESHOLD:
				fscanf(fd_config, "%hd", &config->rssi_threshold);
				break;
		}
	}
	fclose(fd_config);
	return 0;
}

/**
 * Final function, to free memory and close files 
 */
void sigint_handler() 
{
	DPRINT(DINFO, "Exiting...");
	if (eb) 
	{
		/* Should free every evtimer, but takes a lot of effort and adds nothing... */
		event_base_free(eb);
	}
	
	if (_wi_out > 0) 
		wi_close(_wi_out);

	if (_ti_out > 0) 
	{
		system("brctl delif br0 at0");
		ti_close(_ti_out);
	}

	if (_wfn) 
	{
		_wfn->free(_wfn);
	}

	free(config.ap_neighbors);
	free(config.stations);

	if (fp_log) 
		fclose(fp_log);
		
	exit(0);
} 

/* main entry */
int pacmap_init(int argc, char** argv, struct wifi_fn_t *(*wifi_init)(struct ConfigValues *config)) 
{
	char config_filename[200];
	struct event evwi, evti, evsi;
	struct event evtimer, evchngchnn, evscan;

	struct timeval tv;
    init_timer = time(NULL); 

	/* Open log file */
	char buf[80];    
	struct tm *ts = localtime(&init_timer);
    strftime(buf, sizeof(buf), "logs/pacmap-%Y%m%d-%H%M%S.log", ts);
    system("mkdir -p logs");
	fp_log = fopen(buf, "w");

	signal(SIGINT, sigint_handler);

 	/* Random generator (avoid debian-like) */
	srand(time(NULL));

	memset(&config, '\0', sizeof(struct ConfigValues));
	memset(config_filename, '\0', sizeof(config_filename));
	if (parse_arguments(argc, argv, config_filename)) return 1;
	read_config(config_filename, &config);

	/* open output and input interface */
	_wi_out = (struct wif *) wi_open(config.wifi_iface);
	if (!_wi_out)
		return 1;
	dev.fd_out = wi_fd(_wi_out);
	
	/* Same interface for input and output */
	_wi_in = _wi_out;
	dev.fd_in = dev.fd_out;

	/* open output and input tap interface */
	_ti_out = (struct tif *) ti_open(NULL);
	if (!_ti_out)
		return 1;
	dev.ti_out = ti_fd(_ti_out);

	/* Same interface for input and output */
	_ti_in = _ti_out;
	dev.ti_in = dev.ti_out;

	/* drop privileges */
	setuid(getuid());

	if (dev.fd_in == -1) {
		perror("open");
		exit (1);
	}
	
	if (is_same_mac(config.mac_address, MAC_ADDRESS_NULL))
		wi_get_mac(_wi_out, config.mac_address);

	if (wi_set_channel(_wi_out, config.channel))
		DPRINT(DERR, "Couldn't set channel (now in %d)", wi_get_channel(_wi_out));
	/* Tap interface */
	if (ti_set_mac(_ti_in, config.mac_address)) DPRINT(DERR, "Tap mac address could not be set.");
	//if (ti_set_up(_ti_in)) DPRINT(DERR, "Tap interface could not be set up.");
	DPRINT(DINFO, "Tap interface up!");

	/* You can do this manually... */
	system("brctl addif br0 at0");

	assert(wifi_init);
	_wfn = (struct wifi_fn_t *) (*wifi_init)(&config);
	_wfn->dv_ti = _ti_out;
	assert(_wfn->wifi_packet_recv);
	assert(_wfn->wifi_packet_ti_recv);

	int listen_fd;
	ap_msgs_init(&listen_fd);
	
	/* Initalize the event library */
	eb = event_init();

	/* Initalize events */
	event_set(&evwi, dev.fd_in, EV_READ, packet_recv, &evwi);
	event_set(&evti, dev.ti_in, EV_READ, packet_ti_recv, &evti);
	event_set(&evsi, listen_fd, EV_READ|EV_PERSIST, ap_msgs_msg_accept, &evsi);

	/* Add it to the active events, without a timeout */
	event_add(&evwi, NULL);
	event_add(&evti, NULL);
	event_add(&evsi, NULL);
	
	if (_wfn->time_beacon)
	{
		evtimer_set(&evtimer, periodic_fn, &evtimer);
		tv.tv_usec = _wfn->time_beacon; 
		tv.tv_sec = 0;
		evtimer_add(&evtimer, &tv);
	}

	if (_wfn->time_monitor_channels)
	{
		evtimer_set(&evchngchnn, periodic_change_channel, &evchngchnn);
		tv.tv_usec = _wfn->time_monitor_channels; 
		tv.tv_sec = 0;
		evtimer_add(&evchngchnn, &tv);
	}

	if (_wfn->time_scanning)
	{
		evtimer_set(&evscan, periodic_scanning, &evscan);
		tv.tv_usec = _wfn->time_scanning; 
		tv.tv_sec = 0;
		evtimer_add(&evscan, &tv);
	}

	event_dispatch();
	return 0;
}

