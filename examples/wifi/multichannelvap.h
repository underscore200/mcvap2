#ifndef __PACMAP_MULTICHANNELVAP_H__
#define __PACMAP_MULTICHANNELVAP_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>
#include <pacmap/wifi/virtualap.h>
#include <pacmap/wifi/station.h>

#define CHANNEL_SWITCH_COUNT 3
#define TIMER_NEIGHB_CHANN		BEACON_INTERVAL_TIMER*1
#define TIMER_SCAN_RESPONSE		BEACON_INTERVAL_TIMER*15
#define TIMER_MONITOR_CHANNELS	BEACON_INTERVAL_TIMER*15

enum 
{
	STA_WILL_HANDOFF		= 0x10,
	STA_SCAN_RESPONSE		= 0x20,
};

struct APNeighborList
{
	int ap_neighbors_nb;
	struct Device_t *ap_neighbors;
};

typedef struct WatchedClient * pWatchedClient_t;
struct WatchedClient
{
	MACADDR_TYPE(mac_address);
	IPADDR_TYPE(ip_address);
	MACADDR_TYPE(bssid);
	u_int8_t rssi;
	int channel;
	IPADDR_TYPE(ap_ip_address);
	pWatchedClient_t next;
};

struct ClientWatchList
{
	int watched_clients_nb;
	pWatchedClient_t first;
	pWatchedClient_t last;
};

void mcvap_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);
void mcvap_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi);	
void mcvap_ap_beacon(struct wifi_fn_t *wfn);
void mcvap_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length);	
void mcvap_monitor_channels(struct wifi_fn_t *wfn);
void mcvap_free(struct wifi_fn_t *wfn);

void mcvap_ap_msgs_scan_request(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length);
void mcvap_ap_msgs_scan_request_send(struct wifi_fn_t *wfn, pClient_t client);
void mcvap_ap_msgs_scan_response(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length);
void mcvap_ap_msgs_scan_response_send(struct wifi_fn_t *wfn, void *arg);
void mcvap_ap_msgs_client_move(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length);
void mcvap_ap_msgs_client_move_send(struct wifi_fn_t *wfn, void *arg);

void mcvap_queue_packet_send(struct wifi_fn_t *wfn);

struct wifi_fn_t *mcvap_init(struct ConfigValues *config);

#endif /* __PACMAP_MULTICHANNELVAP_H__ */
