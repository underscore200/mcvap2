/*
 *  Virtual access point in a multichannel environment
 *
 */

#include <stdlib.h>
#include <pacmap/wifi/protocol_80211.h>
#include <pacmap/wifi/accesspoint.h>
#include "multichannelvap.h"

#define GET_APNEIGHBORLIST_FROM_PRIV \
	(struct APNeighborList *) (wfn->wfn_priv + sizeof(struct AP_conf) + sizeof(struct VapClientList))
#define GET_CLIENTWATCHLIST_FROM_PRIV \
	(struct ClientWatchList *) (wfn->wfn_priv + sizeof(struct AP_conf) + sizeof(struct VapClientList) + sizeof(struct APNeighborList))

int ap_msgs_msg_id = 0;

#define AP_MSGS_PACKET_OFFSET 2 /* code + id */
u_int8_t * ap_msgs_get_mac(u_int8_t *packet)
{
	return (packet + AP_MSGS_PACKET_OFFSET);
}
u_int8_t ap_msgs_get_rssi(u_int8_t *packet)
{
	if (*packet == AP_MSGS_SCAN_REQUEST)
		return *(ap_msgs_get_mac(packet) + MACADDR_TYPE_SZ*2 + IPADDR_TYPE_SZ);
	else
		return *(ap_msgs_get_mac(packet) + MACADDR_TYPE_SZ + IPADDR_TYPE_SZ);
}
u_int8_t ap_msgs_get_csa_count(u_int8_t *packet)
{
	return ap_msgs_get_rssi(packet);
}
u_int8_t ap_msgs_get_channel(u_int8_t *packet)
{
	if (*packet == AP_MSGS_SCAN_REQUEST)
		return *(ap_msgs_get_mac(packet) + MACADDR_TYPE_SZ*2 + IPADDR_TYPE_SZ + 1);
	else
		return *(ap_msgs_get_mac(packet) + MACADDR_TYPE_SZ + IPADDR_TYPE_SZ + 1);
}
u_int8_t ap_msgs_get_time_beacon(u_int8_t *packet)
{
	return ap_msgs_get_channel(packet);
}

int create_ap_msgs_msg(u_char *ap_msgs_msg, u_int8_t code, MACADDR_TYPE(macaddr), IPADDR_TYPE(ipaddr), MACADDR_TYPE(bssid), u_int8_t rssi, u_int8_t channel)
{
	int length = 0;

	memset(ap_msgs_msg, '\0', sizeof(u_char)*4096);
	memset(ap_msgs_msg + length++, code, 1);
	memset(ap_msgs_msg + length++, ap_msgs_msg_id++, 1);
	memcpy(ap_msgs_msg + length, macaddr, MACADDR_TYPE_SZ);	/* Client MAC address */
	length += MACADDR_TYPE_SZ;
	memcpy(ap_msgs_msg + length, ipaddr, IPADDR_TYPE_SZ);	/* Client IP address */
	length += IPADDR_TYPE_SZ;
	if (bssid)
	{
		memcpy(ap_msgs_msg + length, bssid, MACADDR_TYPE_SZ);	/* Client BSSID address */
		length += MACADDR_TYPE_SZ;	
	}
	memcpy(ap_msgs_msg + length++, &rssi, 1);		/* Client RSSI */
	memcpy(ap_msgs_msg + length++, &channel, 1);		/* Client channel */
	
	return length;
}

pWatchedClient_t wcl_create_client(u_char *packet, IPADDR_TYPE(ip_address))
{
	pWatchedClient_t wc = (pWatchedClient_t ) malloc(sizeof(struct WatchedClient));
	memset(wc, '\0', sizeof(struct WatchedClient));
	
	int offset = AP_MSGS_PACKET_OFFSET;
	memcpy(wc->mac_address, packet + offset, MACADDR_TYPE_SZ);
	offset += MACADDR_TYPE_SZ;
	memcpy(wc->ip_address, packet + offset, IPADDR_TYPE_SZ);
	offset += IPADDR_TYPE_SZ;
	memcpy(wc->bssid, packet + offset, MACADDR_TYPE_SZ);
	offset += MACADDR_TYPE_SZ;
	// wc->rssi = *(packet + offset);
	offset++;
	wc->channel = *(packet + offset);
	offset++;	
	memcpy(wc->ap_ip_address, ip_address, IPADDR_TYPE_SZ);
	
	return wc;
}

void wcl_add_client(struct ClientWatchList *cwl, pWatchedClient_t wc)
{	
	if (!cwl->first)
	{
		cwl->first = wc;
	}

	if (cwl->last)
	{
		cwl->last->next = wc;
	}
	cwl->last = wc;
	cwl->watched_clients_nb++;
}

pWatchedClient_t wcl_get_client_by_mac(struct ClientWatchList *cwl, MACADDR_TYPE(mac_address))
{
	pWatchedClient_t wc = cwl->first;

	while (wc && !is_same_mac(wc->mac_address, mac_address))
	{
		wc = wc->next;
	}
	return wc;
}

void wcl_remove_client(struct ClientWatchList *cwl, MACADDR_TYPE(mac_address))
{
	pWatchedClient_t wc = cwl->first;
	pWatchedClient_t prev = NULL;

	while (wc && !is_same_mac(wc->mac_address, mac_address))
	{
		prev = wc;
		wc = wc->next;
	}
	
	if (!wc) return;

	if (prev)
	{
		prev->next = wc->next;
	}
		
	if (cwl->first == wc)
	{
		if (wc->next)
		{
			cwl->first = wc->next;
		}
		else
		{
			cwl->first = NULL;
		}
	}
	if (cwl->last == wc)
	{
		cwl->last = prev;
	}
	else
	{
		cwl->last = wc->next;
	}

	cwl->watched_clients_nb--;
	free(wc);	
}

void cwl_free(struct ClientWatchList *cwl)
{
	pWatchedClient_t wc = cwl->first;
	pWatchedClient_t aux;

	while (wc)
	{
		aux = wc->next;
		free(wc);
		wc = aux;
		cwl->watched_clients_nb--;
	}
}

void mcvap_send_arp_msg(struct wifi_fn_t *wfn, u_int8_t code, MACADDR_TYPE(macaddr_sender), IPADDR_TYPE(ipaddr_sender), MACADDR_TYPE(macaddr_target), IPADDR_TYPE(ipaddr_target))
{
	unsigned char packet[4096];
	int length = 0;

	/* Destination mac address */
	memcpy(packet, macaddr_target, MACADDR_TYPE_SZ);
	length += MACADDR_TYPE_SZ;
	/* Source mac address */
	memcpy(packet + length, macaddr_sender, MACADDR_TYPE_SZ);
	length += MACADDR_TYPE_SZ;
	/* Ether type */
	u_int16_t ether_type = htons(0x0806);
	memcpy(packet + length, &ether_type, sizeof(ether_type));
	length += sizeof(ether_type);
	
	/* ARP data */
	memcpy(packet + length, "\00\01\10\00\06\04\00", 7);
	length += 7;
	memcpy(packet + length++, &code, 1);
	/* Sender */
	memcpy(packet + length, macaddr_sender, MACADDR_TYPE_SZ);
	length += MACADDR_TYPE_SZ;
	memcpy(packet + length, ipaddr_sender, IPADDR_TYPE_SZ);
	length += IPADDR_TYPE_SZ;
	/* Target */
	memcpy(packet + length, macaddr_target, MACADDR_TYPE_SZ);
	length += MACADDR_TYPE_SZ;
	memcpy(packet + length, ipaddr_target, IPADDR_TYPE_SZ);
	length += IPADDR_TYPE_SZ;
	
	memset(packet + length, '\0', 60 - length);
	length = 60;
	DPRINT(DINFO, "Sending ARP %s "f_IPADDR" -> "f_IPADDR"", code == 1 ? "Request" : "Reply", IPADDR(ipaddr_sender), IPADDR(ipaddr_target));
	proto80211_packet_ti_send(packet, length);
	// mcvap_ti_recv(wfn, packet, length);
}

#define MIN_TIME_LAST_STATE 3
void mcvap_ap_msgs_scan_request_send(struct wifi_fn_t *wfn, pClient_t client)
{
	struct timeval now_t;
	gettimeofday(&now_t, NULL);
	if (now_t.tv_sec - client->tv_last_state.tv_sec < MIN_TIME_LAST_STATE || is_same_ip(client->ip_address, IP_ADDRESS_NULL)) 
	{
		// DPRINT(DDEBUG, "TIME!!!! %ld %ld - %ld %ld", now_t.tv_sec, now_t.tv_usec, client->tv_last_state.tv_sec, client->tv_last_state.tv_usec);
		return;
	}
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	client->state |= (STA_WILL_HANDOFF | STA_SCAN_RESPONSE);
	memcpy(&client->tv_last_state, &now_t, sizeof(struct timeval));

	u_char ap_msgs_msg[4096];
	int length = 0;

	/* ap_msgs message */
	length = create_ap_msgs_msg(ap_msgs_msg, AP_MSGS_SCAN_REQUEST, client->mac_address, client->ip_address, client->bssid, client->rssi, ap->channel);
	DPRINT(DINFO, "IAPP: SCAN_REQUEST_SEND  id: %04d client: "f_MACADDR" rssi: %d", *(ap_msgs_msg+1), MACADDR(client->mac_address), client->rssi);
	
	/* Send to all AP neighbors */
	int i;
	struct APNeighborList *apngbl = GET_APNEIGHBORLIST_FROM_PRIV;
	for (i = 0; i < apngbl->ap_neighbors_nb; i++)
	{
		DPRINT(DINFO, "sending request to IP: "f_IPADDR", mac: "f_MACADDR"", IPADDR(apngbl->ap_neighbors[i].ip_address), \
				MACADDR(apngbl->ap_neighbors[i].mac_address));
		proto80211_ap_msgs_msg_send(ap_msgs_msg, length, apngbl->ap_neighbors[i].ip_address);
	}

	/* Add timer for scan responses */
	wfn->wifi_timer_fn = mcvap_ap_msgs_client_move_send;
	proto80211_add_timer(TIMER_SCAN_RESPONSE, client->mac_address, MACADDR_TYPE_SZ);
}

void mcvap_ap_msgs_scan_timeout(struct wifi_fn_t *wfn, void *arg)
{
	// struct ClientWatchList *cwl = GET_CLIENTWATCHLIST_FROM_PRIV;
	// wcl_remove_client(cwl, arg);
}

void mcvap_ap_msgs_scan_request(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length)
{
	struct ClientWatchList *cwl = GET_CLIENTWATCHLIST_FROM_PRIV;

	pWatchedClient_t wc = wcl_get_client_by_mac(cwl, ap_msgs_get_mac(packet));
	if (!wc)	
	{
		wc = wcl_create_client(packet, ip_address);
		wcl_add_client(cwl, wc);
	}
	wc->rssi = 0;
	
	// change to client channel, scan for a while, and send the result to the AP
	wfn->wifi_timer_fn = mcvap_ap_msgs_scan_response_send;
	proto80211_add_timer(TIMER_NEIGHB_CHANN, wc->mac_address, MACADDR_TYPE_SZ);
	wifi_set_channel(wc->channel);
	DPRINT(DINFO, "IAPP: SCAN_REQUEST_RECV  id: %04d st: "f_MACADDR" chan: %d", *(packet+1), MACADDR(wc->mac_address), wc->channel);

	/* Time out for watching client */
	// wfn->wifi_timer_fn = mcvap_ap_msgs_scan_timeout;
	// proto80211_add_timer(TIMER_SCAN_RESPONSE*3, wc->mac_address, MACADDR_TYPE_SZ);
}

void mcvap_ap_msgs_scan_response_send(struct wifi_fn_t *wfn, void *arg)
{
	struct ClientWatchList *cwl = GET_CLIENTWATCHLIST_FROM_PRIV;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	/* Change to ap channel */
	wifi_set_channel(ap->channel);
	/* Check if there is any queued packet to send */
	mcvap_queue_packet_send(wfn);

	MACADDR_TYPE(mac_address);
	memcpy(mac_address, (u_int8_t *)arg, MACADDR_TYPE_SZ);
	pWatchedClient_t wc = wcl_get_client_by_mac(cwl, mac_address);
	if (!wc || !wc->rssi) return;
	
	u_char ap_msgs_msg[4096];
	int length = 0;

	/* ap_msgs message */
	length = create_ap_msgs_msg(ap_msgs_msg, AP_MSGS_SCAN_RESPONSE, wc->mac_address, wc->ip_address, NULL, wc->rssi, ap->channel);
	
	DPRINT(DINFO, "IAPP: SCAN_RESPONSE_SEND id: %04d st: "f_MACADDR" rssi: %d", *(ap_msgs_msg+1), MACADDR(wc->mac_address), wc->rssi);
	proto80211_ap_msgs_msg_send(ap_msgs_msg, length, wc->ap_ip_address);
}

void mcvap_ap_msgs_scan_response(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	/* Look for the client */
	MACADDR_TYPE(mac_address);
	memcpy(mac_address, ap_msgs_get_mac(packet), MACADDR_TYPE_SZ);
	pClient_t c = ap_search_client(ap, mac_address);
	DPRINT(DINFO, "IAPP: SCAN_RESPONSE_RECV id: %04d client: "f_MACADDR" from: "f_IPADDR"", *(packet+1), MACADDR(mac_address), IPADDR(ip_address));
		u_int8_t rssi = ap_msgs_get_rssi(packet);
	if (c) DPRINT(DINFO, "IAPP: SCAN_RESPONSE_RECV state: %02X/%02X rssi %d/%d", c->state, c->state & (STA_WILL_HANDOFF | STA_SCAN_RESPONSE), rssi, c->rssi);
	if (c && (c->state & (STA_WILL_HANDOFF | STA_SCAN_RESPONSE)))
	{
		DPRINT(DINFO, "IAPP: SCAN_RESPONSE_RECV %04d "f_MACADDR" rssi %d/%d", *(packet+1), MACADDR(mac_address), rssi, c->rssi);
		if (rssi > c->rssi + SIGNAL_FLOOR && rssi > c->new_rssi) 
		{
			DPRINT(DINFO, "IAPP: SCAN_RESPONSE_RECV %04d "f_MACADDR" new_channel %d", *(packet+1), MACADDR(mac_address), c->new_channel);
			c->new_channel = ap_msgs_get_channel(packet);
			c->new_rssi = rssi;
			memcpy(c->new_ap_ipaddr, ip_address, IPADDR_TYPE_SZ);
		}
	}
}

void mcvap_ap_msgs_client_move_send(struct wifi_fn_t *wfn, void *arg)
{
	MACADDR_TYPE(mac_address);
	memcpy(mac_address, (u_int8_t *)arg, MACADDR_TYPE_SZ);

	pAP_t ap = (pAP_t) wfn->wfn_priv;
	pClient_t client = ap_search_client(ap, mac_address);

	if (!client) return;
	
	client->state &= (~STA_SCAN_RESPONSE);
	if (client->new_channel)
	{
		client->csa_count = CHANNEL_SWITCH_COUNT;

		u_char ap_msgs_msg[4096];
		int length = 0;

		/* ap_msgs message */
		length = create_ap_msgs_msg(ap_msgs_msg, AP_MSGS_CLIENT_MOVE, client->mac_address, client->ip_address, NULL, client->csa_count, wfn->time_beacon);
		DPRINT(DINFO, "IAPP: CLIENT_MOVE_SEND %04d "f_MACADDR" new_channel %d (%d*%d)", *(ap_msgs_msg+1), MACADDR(mac_address), client->new_channel, client->csa_count, wfn->time_beacon);

		proto80211_ap_msgs_msg_send(ap_msgs_msg, length, client->new_ap_ipaddr);
	}
	else
	{
		client->state &= (~STA_WILL_HANDOFF);
	}
}

void mcvap_ap_msgs_client_move(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length)
{
	struct ClientWatchList *cwl = GET_CLIENTWATCHLIST_FROM_PRIV;
	pWatchedClient_t wc = wcl_get_client_by_mac(cwl, ap_msgs_get_mac(packet));

	DPRINT(DINFO, "IAPP: CLIENT_MOVE        %04d "f_MACADDR"", *(packet+1), MACADDR(packet + 2));
	if (wc)
	{

		pAP_t ap = (pAP_t) wfn->wfn_priv;
		DPRINT(DINFO, "IAPP: Adding client "f_MACADDR"", MACADDR(wc->mac_address));

		ap_auth_client(ap, wc->mac_address, wc->bssid); 
		ap_assoc_client(ap, wc->mac_address, wc->bssid); 
		system("(brctl showmacs br0; arp -na) > lala.log");
		mcvap_send_arp_msg(wfn, 0x01, wc->mac_address, wc->ip_address, MAC_ADDRESS_NULL, "\300\250\1\1");
		mcvap_send_arp_msg(wfn, 0x01, wc->mac_address, wc->ip_address, MAC_ADDRESS_NULL, wc->ap_ip_address);
		system("(brctl showmacs br0; arp -na) >> lala.log");
		wcl_remove_client(cwl, wc->mac_address);
	}
}

void mcvap_wifi_to_ether_packet(struct wifi_fn_t *wfn, u_char *packet, int length)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	/* set ToDS=0, FromDS=1 for injecting purposes */
	u_int16_t fc = header->fc;
	fc = (fc & ~FC_TO_DS_BIT) | FC_FROM_DS_BIT;
	header->fc = fc;

	unsigned char h80211[4096];
	int trailer = 0;
	int offset = 0;
	
	/* Destination mac address */
	memcpy(h80211, header->bssid, MACADDR_TYPE_SZ);
	offset += MACADDR_TYPE_SZ;
	/* Source mac address */
	memcpy(h80211 + offset, header->sa, MACADDR_TYPE_SZ);
	offset += MACADDR_TYPE_SZ;
	/* Ether type */
	memcpy(h80211 + offset, packet + sizeof(struct mgmt_header_t) + 6, sizeof(u_int16_t));
	offset += sizeof(u_int16_t);

	u_int16_t ether_type;
	memcpy(&ether_type, packet + sizeof(struct mgmt_header_t) + 6, 2);
	ether_type = ntohs(ether_type);
	
	if (header->seq_ctrl % 1 == 0)
	{
		DPRINT(DDEBUG, "DATA_DATA "f_MACADDR" seq %4d type %s %s", MACADDR(header->sa), header->seq_ctrl, packet_type(ether_type), get_l4proto(packet, length, ether_type));
		print_icmp_seq(packet, length, ether_type);
	}

	if (length <= sizeof(struct mgmt_header_t) + 8)
		return;

	/* Check if the packet is ARP for the AP * /
	if(ether_type == 0x0806) {
		// check if code is request
		u_int16_t code;
		memcpy(&code,  packet + sizeof(struct mgmt_header_t) + 14, sizeof(u_int16_t));
		pAP_t ap = (pAP_t) wfn->wfn_priv;
		u_int8_t targetIP[4], senderIP[4];
		memcpy(senderIP, packet + sizeof(struct mgmt_header_t) + 22, 4);
		memcpy(targetIP, packet + sizeof(struct mgmt_header_t) + 32, 4);
		if(is_same_ip(targetIP, ap->ip_address) && code == 0x0100) {
			DPRINT(DINFO, " ARP to "f_IPADDR" with code %x, our IP "f_IPADDR"", IPADDR(targetIP), code, IPADDR(ap->ip_address));
			mcvap_send_arp_msg(wfn, 0x01,  header->sa, senderIP, ap->mac_address, ap->ip_address);
			return;
		}
	}
	/*  */


	/* Copy frame body */
	memcpy(h80211 + 14, packet + sizeof(struct mgmt_header_t) + 8, length - sizeof(struct mgmt_header_t) - 8);
	length = length - sizeof(struct mgmt_header_t) - 8 + 14;

	/* ethernet frame must be atleast 60 bytes without fcs */
	if (length < 60)
	{
		trailer = 60 - length;
		bzero(h80211 + length, trailer);
		length += trailer;
	}
	proto80211_packet_ti_send(h80211, length);
}

void mcvap_ap_beacon(struct wifi_fn_t *wfn)
{
	DPRINT(DMAX2, "%s: mcvap_ap_beacon", __FILE__);
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	u_char h80211[4096];
	u_char essid[64];
	int packet_length;

	memset(h80211, 0, 4096);
	packet_length = 0;
	
	/* For each associated client */
	pClient_t client = ap_get_first_client(ap);
	while (client)
	{
		if (is_same_ip(client->ip_address, IP_ADDRESS_NULL))
		{
		}

		memset(h80211, 0, 4096);
		packet_length = 0;
		
		sprintf((char *)essid, "%s"f_MACADDR"", ESSID_VIRTUALAP_PREFIX, MACADDR(client->mac_address));
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, BROADCAST, client->bssid, client->bssid, ap_get_seq_ctrl(ap), FC_MGMT_BEACON, 314);
		packet_length = ieee80211_mgmt_forge(h80211, packet_length, ST_BEACON, ap->channel, essid, ap->capability, ap->rates_supp, ap->rates_supp_ext);

		/* Channel switch announcement */
		if (client->csa_count > 0)
		{
			DPRINT(DDEBUG, "CSA %d", client->csa_count);
			h80211[packet_length++] = 37;	// Element ID 
			h80211[packet_length++] = 3; 	// Length
			h80211[packet_length++] = 0; 	// Channel Switch Mode
			h80211[packet_length++] = client->new_channel; 	// New Channel Number
			h80211[packet_length++] = client->csa_count--; 	// Channel Switch Count
		}

		client->seq = (client->seq + 1) % 4096;
		
		memcpy(h80211 + 24, &client->timestamp, sizeof(client->timestamp));

		if (client->seq % 50 == 0)
		{
			struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
			DPRINT(DINFO, "Sending BEACON from: "f_MACADDR" to: "f_MACADDR" BSSID: "f_MACADDR" essid: %s seq: %d rssi: %d", MACADDR(header->sa), MACADDR(header->da), MACADDR(header->bssid), essid, client->seq, client->rssi);
			/* if (ap_client_check_timeout(ap, client)) DPRINT(DINFO, "Client timeout "f_MACADDR"", MACADDR(mac_address)); */
		}
		
		/*  send packet at 1 Mbit/s */
		proto80211_packet_send(h80211, packet_length, 0x02);

		if (client->new_channel && client->csa_count == 0 && !(client->state & STA_SCAN_RESPONSE))
		{
			DPRINT(DINFO, "Client "f_MACADDR" is gone", MACADDR(client->mac_address));
			// mcvap_send_arp_msg(wfn, 0x01, client->mac_address, client->ip_address, MAC_ADDRESS_NULL, ap->ip_address);
			ap_delete_client(ap, client->mac_address);
			//mcvap_send_arp_msg(wfn, 0x01, client->mac_address, client->ip_address, MAC_ADDRESS_NULL, client->new_ap_ipaddr);
			//mcvap_send_arp_msg(wfn, 0x01, client->mac_address, client->ip_address, MAC_ADDRESS_NULL, ap->ip_address);
			client = NULL;			
		}
		client = ap_get_next_client(ap);
	} 

}

void mcvap_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_auth(wfn, packet, length, rssi);
	return;
	DPRINT(DINFO, "somehow got here");
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	pClient_t c = ap_search_client(ap, header->sa);
	if (!c) return;

	struct ClientWatchList *cwl = GET_CLIENTWATCHLIST_FROM_PRIV;
	pWatchedClient_t wc = wcl_get_client_by_mac(cwl, header->sa);
	if (wc)
	{
		wcl_remove_client(cwl, wc->mac_address);
	} 
}

void mcvap_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_deauth(wfn, packet, length, rssi);
}

void mcvap_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_assoc_request(wfn, packet, length, rssi);
}

void mcvap_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_disassoc(wfn, packet, length, rssi);
}

void mcvap_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_reassoc_request(wfn, packet, length, rssi);
}

void mcvap_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_reassoc_response(wfn, packet, length, rssi);
}

void mcvap_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_probe_request(wfn, packet, length, rssi);
}

void mcvap_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	/* struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	pVapClient_t st = vapl_get_station(vapl, header->sa);
	if (st) 
	{
		if (!st->rssi) DPRINT(DINFO, "Updated station "f_MACADDR" rssi %d", MACADDR(header->sa), rssi);
		st->rssi = rssi;
	} */

	struct ClientWatchList *cwl = GET_CLIENTWATCHLIST_FROM_PRIV;
	pWatchedClient_t wc = wcl_get_client_by_mac(cwl, header->sa);
	if (wc)
	{
		wc->rssi = rssi;
		DPRINT(DDEBUG, "DATA Watched Client "f_MACADDR" rssi %d", MACADDR(header->sa), wc->rssi);
		return;
	} 

	/* Check if packet is for one of our clients */
	pClient_t c = ap_search_client(ap, header->sa);
	if (!c) return;
	
	/* Check if packet is duplicated */
	if (c->frame_seq == header->seq_ctrl) return;
	
	if (is_same_ip(c->ip_address, IP_ADDRESS_NULL))
	{
		u_int16_t ether_type = *(packet + sizeof(struct mgmt_header_t) + 6) | *(packet + sizeof(struct mgmt_header_t) + 7) << 8;
		if (ether_type == 0x0008)
		{

			u_char *p = NULL;
			// if (*(packet + 41) == 1)
			p = packet + 44;
			memcpy(c->ip_address, p, IPADDR_TYPE_SZ);
			mcvap_send_arp_msg(wfn, 0x01, c->mac_address, c->ip_address, BROADCAST, "\300\250\1\1");
		}
	}
	c->rssi = rssi;
	mcvap_wifi_to_ether_packet(wfn, packet, length);
	c->frame_seq = header->seq_ctrl;

	/* Check if client needs to change of AP */
	rssi += 256;
	DPRINT(DDEBUG, "ap->rssi_threshold %d rssi %d c->state %X / %X", ap->rssi_threshold, rssi, c->state, STA_WILL_HANDOFF);
	if ((rssi < ap->rssi_threshold) && !(c->state & STA_WILL_HANDOFF))
	{
		mcvap_ap_msgs_scan_request_send(wfn, c);
	}
	
}

void mcvap_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	/*
	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	pVapClient_t st = vapl_get_station(vapl, header->sa);
	if (st) 
	{
		if (!st->rssi) DPRINT(DINFO, "Updated station "f_MACADDR" rssi %d", MACADDR(header->sa), rssi);
		st->rssi = rssi;
	} */

	struct ClientWatchList *cwl = GET_CLIENTWATCHLIST_FROM_PRIV;
	pWatchedClient_t wc = wcl_get_client_by_mac(cwl, header->sa);
	if (wc)
	{
		wc->rssi = rssi;
	}
	
	pClient_t c = ap_search_client(ap, header->sa);
	if (c)
	{
		if (header->seq_ctrl % 500 == 0)
		{
			DPRINT(DDEBUG, "DATA_NODATA "f_MACADDR" seq %4d rssi %3d", MACADDR(header->sa), header->seq_ctrl, rssi);
		}
		ap_update_client_rxinfo(ap, header->sa, rssi);

		/* Check if client needs to change of AP */
		rssi += 256;
		if (rssi < ap->rssi_threshold && !(c->state & STA_WILL_HANDOFF))
		{
			DPRINT(DINFO, "rssi %d < ap->rssi %d", rssi, ap->rssi_threshold);
			mcvap_ap_msgs_scan_request_send(wfn, c);
		}
	}
}

void mcvap_queue_packet_send(struct wifi_fn_t *wfn)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	struct ePacket packet;
	while (!ap_queue_is_empty(ap))
	{
		ap_queue_remove_packet(ap, &packet);
		struct mgmt_header_t *header = (struct mgmt_header_t *) packet.buffer;
		
		u_int16_t ether_type;
		memcpy(&ether_type, packet.buffer + 30, 2);
		ether_type = ntohs(ether_type);
				
		DPRINT(DDEBUG, "TAP: da: "f_MACADDR", sa: "f_MACADDR", bssid: "f_MACADDR", type %s%s", MACADDR(header->da), MACADDR(header->sa), MACADDR(header->bssid), packet_type(ether_type), get_l4proto(packet.buffer, packet.length, ether_type));
		print_icmp_seq(packet.buffer, packet.length, ether_type);

		proto80211_packet_send(packet.buffer, packet.length, packet.rate);
	}
}

void mcvap_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length)
{	
	/*  802.11 header for sending operations, reusable */
	unsigned char h80211[4096];
	int offset = 0;

	if (packet == NULL) return;
	if (length < 38) return;

	pAP_t ap = (pAP_t) wfn->wfn_priv;

	MACADDR_TYPE(dst_mac);
	memcpy(dst_mac, packet, MACADDR_TYPE_SZ);
	pClient_t client = ap_search_client(ap, dst_mac);
	if (!client && !is_same_mac(dst_mac, BROADCAST))
	{
		// DPRINT(DDEBUG, "TAP Packet to "f_MACADDR" (but it's not a client!)", MACADDR(dst_mac));
		return;
	}

	int packet_length = length;

	memcpy(h80211, IEEE80211_LLC_SNAP, 32);
	memcpy(h80211 + 32, packet + 14, packet_length - 14);
	
	/* Ether type */
	memcpy(h80211 + 30, packet + 12, sizeof(u_int16_t));
	offset += sizeof(u_int16_t);

	u_int16_t ether_type;
	memcpy(&ether_type, packet + 12, 2);
	ether_type = ntohs(ether_type);

	h80211[1] |= 0x02; /* FC_DATA_DATA */
	
	/* Copy MAC address to header */
	if (client)
		memcpy(h80211 + 10, client->bssid, MACADDR_TYPE_SZ); /* BSSID */
	else
		memcpy(h80211 + 10, ap->bssid, MACADDR_TYPE_SZ); /* BSSID */
	memcpy(h80211 + 16, packet + 6,	6); /* SRC_MAC */
	memcpy(h80211 + 4, packet, 6); /* DST_MAC */

	packet_length += (32 - 14); /* 32=IEEE80211+LLC/SNAP; 14=SRC_MAC+DST_MAC+TYPE */

	struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
	DPRINT(DDEBUG, "TAP: da: "f_MACADDR", sa: "f_MACADDR", from: "f_MACADDR", type %s %s", MACADDR(header->da), MACADDR(header->sa), MACADDR(header->bssid), packet_type(ether_type), get_l4proto(h80211, packet_length, ether_type));
	print_icmp_seq(h80211, packet_length, ether_type);

	/* Am I in the good channel ? */
	if (wifi_get_channel() != ap->channel)
	{
		ap_queue_insert_packet(ap, h80211, packet_length, RATE_54M/500000);
		return;
	}

	proto80211_packet_send(h80211, packet_length, RATE_54M/500000);
}

void mcvap_free(struct wifi_fn_t *wfn)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	ap_free(ap);

	struct APNeighborList *apngbl = GET_APNEIGHBORLIST_FROM_PRIV;
	free(apngbl->ap_neighbors);

	struct ClientWatchList *cwl = GET_CLIENTWATCHLIST_FROM_PRIV;
	cwl_free(cwl);	
}

struct wifi_fn_t *mcvap_init(struct ConfigValues *config)
{
	struct wifi_fn_t *wfn;

	DPRINT(DMAX, "%s: mcvap_init", __FILE__);
	wfn = wfn_alloc(sizeof(struct AP_conf) + sizeof(struct VapClientList) + sizeof(struct APNeighborList) + sizeof(struct ClientWatchList));

	if (!wfn)
		return NULL;
		
	wfn->wifi_packet_recv = proto80211_packet_recv;
	wfn->wifi_packet_ti_recv = proto80211_packet_ti_recv;
	wfn->wifi_st_assoc_request	= mcvap_st_assoc_request;
	wfn->wifi_st_disassoc		= mcvap_st_disassoc;
	wfn->wifi_st_reassoc_request	= mcvap_st_reassoc_request;
	wfn->wifi_st_reassoc_response	= mcvap_st_reassoc_response;
	wfn->wifi_st_probe_request	= mcvap_st_probe_request;
	wfn->wifi_st_auth	= mcvap_st_auth;
	wfn->wifi_st_deauth	= mcvap_st_deauth;

	wfn->wifi_data_data	= mcvap_data_data;	
	wfn->wifi_data_nodata	= mcvap_data_nodata;	
	wfn->wifi_ti_recv = mcvap_ti_recv;

	wfn->wifi_ap_msgs_scan_request = mcvap_ap_msgs_scan_request;
	wfn->wifi_ap_msgs_scan_response = mcvap_ap_msgs_scan_response;
	wfn->wifi_ap_msgs_client_move = mcvap_ap_msgs_client_move;

	wfn->wifi_ap_beacon	= mcvap_ap_beacon;
	wfn->time_beacon = BEACON_INTERVAL_TIMER;

	wfn->wifi_free = mcvap_free;

	/*Access point information */
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	ap_init(ap, config);
	
	struct APNeighborList *apngbl = GET_APNEIGHBORLIST_FROM_PRIV;
	memset(apngbl, '\0', sizeof(struct APNeighborList));
	apngbl->ap_neighbors_nb = config->ap_neighbors_nb;
	apngbl->ap_neighbors = (struct Device_t *) malloc(sizeof(struct Device_t)*config->ap_neighbors_nb);
	memcpy(apngbl->ap_neighbors, config->ap_neighbors, sizeof(struct Device_t)*config->ap_neighbors_nb);

	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	memset(vapl, '\0', sizeof(struct VapClientList));

	struct ClientWatchList *cwl =  GET_CLIENTWATCHLIST_FROM_PRIV;
	memset(cwl, '\0', sizeof(struct ClientWatchList));	

	DPRINT(DINFO, "MULTI-CHANNEL VIRTUAL ACCESS POINT");
	DPRINT(DINFO, "mac: "f_MACADDR" channel: %d essid: '%s'", MACADDR(ap->mac_address), ap->channel, ap->essid);
	return wfn;
}

