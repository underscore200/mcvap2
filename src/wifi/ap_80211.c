/*
 *  802.11 access point
 *
 */

#include <pacmap/wifi/protocol_80211.h>
#include <pacmap/wifi/accesspoint.h>
#include "ap_80211.h"

void ap_80211_ap_beacon(struct wifi_fn_t *wfn)
{
	DPRINT(DMAX2, "%s: ap_80211_ap_beacon", __FILE__);
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	u_char h80211[4096];
	int packet_length = 0;
	
	memset(h80211, 0, 4096);
	
	packet_length = ieee80211_framectrl_forge(h80211, packet_length, BROADCAST, ap->mac_address, ap->bssid, ap_get_seq_ctrl(ap), FC_MGMT_BEACON, 314);
	packet_length = ieee80211_mgmt_forge(h80211, packet_length, ST_BEACON, ap->channel, ap->essid, ap->capability, ap->rates_supp, ap->rates_supp_ext);
	
	/*  send packet at 1 Mbit/s */
    proto80211_packet_send(h80211, packet_length, 0x02);    
}

void ap_80211_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	DPRINT(DMAX, "%s: ap_80211_st_auth", __FILE__);

	/* Authorization request is for us */
	if (is_same_mac(header->bssid, ap->mac_address) && !is_same_mac(header->sa, ap->mac_address))
	{
		/* Add station to authorizated list */
		int status = ap_auth_client(ap, header->sa, header->bssid);

		DPRINT(DINFO, "AUTH request by "f_MACADDR" (%d)", MACADDR(header->sa), status);
		u_char h80211[4096];
		int packet_length = 0;

		memset(h80211, 0, 4096);
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, header->sa, ap->mac_address, ap->bssid, ap_get_seq_ctrl(ap), FC_MGMT_AUTH, 314);
		packet_length = ieee80211_auth_forge(h80211, packet_length, AUTH_ALGO_OPEN_SYSTEM, IEEE80211_AUTH_OPEN_RESPONSE, IEEE80211_STATUS_SUCCESS);

		/*  send packet at 1 Mbit/s */
	    proto80211_packet_send(h80211, packet_length, 0x02);
	}
}

void ap_80211_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;

	DPRINT(DMAX, "ap_80211_st_deauth,\tsmac: "f_MACADDR", dmac: "f_MACADDR", bssid: "f_MACADDR"", MACADDR(header->sa), MACADDR(header->da), MACADDR(header->bssid));
}

void ap_80211_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	if (is_same_mac(ap->mac_address, header->bssid))
	{
		u_char h80211[4096];
		int packet_length = 0;

		u_int16_t status = IEEE80211_STATUS_SUCCESS;
		u_int16_t aid = 0x0100;
		
		memset(h80211, 0, 4096);
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, header->sa, ap->mac_address, ap->bssid, ap_get_seq_ctrl(ap), FC_MGMT_ASSOC_RESPONSE, 314);

		/* Add client */
		if (!ap_assoc_client(ap, header->sa, header->bssid))
			status = IEEE80211_STATUS_UNSPECIFIED;
		DPRINT(DINFO, "ASSOC request by "f_MACADDR" (%d)", MACADDR(header->sa), status);

		packet_length = ieee80211_assoc_forge(h80211, packet_length, ap->capability, status, aid, ap->rates_supp, ap->rates_supp_ext);

		/*  send packet at 1 Mbit/s */
		proto80211_packet_send(h80211, packet_length, 0x02);
	}
}

void ap_80211_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	if (is_same_mac(ap->mac_address, header->bssid))
	{		
		DPRINT(DINFO, "DISASSOC "f_MACADDR"", MACADDR(header->sa));
		ap_delete_client(ap, header->sa);
	}
}

void ap_80211_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	if (is_same_mac(ap->mac_address, header->bssid))
	{
		u_char *current_ap = (u_char *) packet + sizeof(struct mgmt_header_t) + sizeof(u_int16_t)*2;
		DPRINT(DINFO, "REASSOC request by "f_MACADDR" (current ap: "f_MACADDR")", MACADDR(header->sa), MACADDR(current_ap));
		ap_delete_client(ap, header->sa);
		ap_80211_st_assoc_request(wfn, packet, length, rssi);
	}
}

void ap_80211_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	if (is_same_mac(ap->mac_address, header->bssid))
	{
		DPRINT(DINFO, "REASSOC response by "f_MACADDR"", MACADDR(header->sa));
	}
}

/* Handles 802.11 probe request */
void ap_80211_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	
	/* Parsing frame body */
	struct mgmt_body_t mgmt_body_fields;
	ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
	
	/* Looking for any AP */
	if ((!mgmt_body_fields.ssid.length || is_same_ssid(mgmt_body_fields.ssid.ssid, ap->essid)) && is_same_mac(header->bssid, BROADCAST))
	{
		DPRINT(DMAX, "ST_PROBE_REQUEST\tsmac: "f_MACADDR", essid: '%s', dmac: "f_MACADDR", bssid: "f_MACADDR"", MACADDR(header->sa), mgmt_body_fields.ssid.ssid, MACADDR(header->da), MACADDR(header->bssid));
		u_char h80211[4096];
		int packet_length = 0;

		DPRINT(DDEBUG, "Station "f_MACADDR" is looking for '%s' on "f_MACADDR" with BSSID "f_MACADDR" (it's us!)", MACADDR(header->sa), mgmt_body_fields.ssid.ssid, MACADDR(header->da), MACADDR(header->bssid));

		memset(h80211, 0, 4096);
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, header->sa, ap->mac_address, ap->bssid, ap_get_seq_ctrl(ap), FC_MGMT_PROBE_RESPONSE, 314);
		packet_length = ieee80211_mgmt_forge(h80211, packet_length, ST_PROBE_RESPONSE, ap->channel, ap->essid, ap->capability, ap->rates_supp, ap->rates_supp_ext);

		/*  send packet at 1 Mbit/s */
	    proto80211_packet_send(h80211, packet_length, 0x02);
	}
}


void ap_80211_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	/* Check if packet is for us */
	if (is_same_mac(header->da, ap->mac_address))
	{
		/* set ToDS=0, FromDS=1 for injecting purposes */
		u_int16_t fc = header->fc;
		fc = (fc & ~FC_TO_DS_BIT) | FC_FROM_DS_BIT;
		header->fc = fc;

        unsigned char h80211[4096];
		int trailer = 0;
		int offset = 0;
		u_int16_t ether_type;
		
		/* Destination mac address */
		memcpy(h80211, header->bssid, MACADDR_TYPE_SZ);
		offset += MACADDR_TYPE_SZ;
		/* Source mac address */
		memcpy(h80211 + offset, header->sa, MACADDR_TYPE_SZ);
		offset += MACADDR_TYPE_SZ;
		/* Ether type */
		memcpy(h80211 + offset, packet + sizeof(struct mgmt_header_t) + 6, sizeof(u_int16_t));
		offset += sizeof(u_int16_t);

		memcpy(&ether_type, packet + sizeof(struct mgmt_header_t) + 6, 2);
		ether_type = ntohs(ether_type);
		
		DPRINT(DDEBUG, "DATA from: "f_MACADDR" type: %s", MACADDR(header->sa), packet_type(ether_type));

		if (length <= sizeof(struct mgmt_header_t) + 8)
			return;

		/* Copy frame body */
		memcpy(h80211 + 14, packet + sizeof(struct mgmt_header_t) + 8, length - sizeof(struct mgmt_header_t) - 8);
		length = length - sizeof(struct mgmt_header_t) - 8 + 14;

		/* ethernet frame must be atleast 60 bytes without fcs */
		if (length < 60)
		{
			trailer = 60 - length;
			bzero(h80211 + length, trailer);
			length += trailer;
		}
		proto80211_packet_ti_send(h80211, length);
	}
}

void ap_80211_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length)
{
	/*  802.11 header for sending operations, reusable */
	unsigned char h80211[4096];

	if (packet == NULL) return;
	if (length < 38) return;

	memcpy(h80211, IEEE80211_LLC_SNAP, 32);
	memcpy(h80211 + 32, packet + 14, length - 14);
	memcpy(h80211 + 30, packet + 12, 2);
	h80211[1] |= 0x02; /*   data packet */

	pAP_t ap = (pAP_t) wfn->wfn_priv;

	memcpy(h80211 + 10, ap->bssid, MACADDR_TYPE_SZ); /* BSSID */
	memcpy(h80211 + 16, packet + 6,	6); /* SRC_MAC */
	memcpy(h80211 + 4, packet, 6); /* DST_MAC */

	struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
	u_int16_t ether_type;
	memcpy(&ether_type, packet + 12, 2);
	ether_type = ntohs(ether_type);

	DPRINT(DDEBUG, "TAP to: "f_MACADDR" type %s", MACADDR(header->da), packet_type(ether_type));
	length += (32 - 14); /* 32=IEEE80211+LLC/SNAP; 14=SRC_MAC+DST_MAC+TYPE */

	/*  Passthrough mode */
	proto80211_packet_send(h80211, length, RATE_54M/500000);
}

void ap_80211_free(struct wifi_fn_t *wfn)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	ap_free(ap);
}

struct wifi_fn_t *ap_80211_init(struct ConfigValues *config)
{
	struct wifi_fn_t *wfn;
	pAP_t ap;

	wfn = wfn_alloc(sizeof(struct AP_conf));

	if (!wfn)
		return NULL;
		
	wfn->wifi_st_assoc_request	= ap_80211_st_assoc_request;
	wfn->wifi_st_disassoc		= ap_80211_st_disassoc;
	wfn->wifi_st_reassoc_request	= ap_80211_st_reassoc_request;
	wfn->wifi_st_reassoc_response	= ap_80211_st_reassoc_response;
	wfn->wifi_st_probe_request	= ap_80211_st_probe_request;
	wfn->wifi_st_auth	= ap_80211_st_auth;
	wfn->wifi_st_deauth	= ap_80211_st_deauth;
	wfn->wifi_data_data	= ap_80211_data_data;	
	wfn->wifi_ap_beacon	= ap_80211_ap_beacon;
	wfn->wifi_ti_recv = ap_80211_ti_recv;
	wfn->time_beacon = BEACON_INTERVAL_TIMER;
	wfn->wifi_free = ap_80211_free;
	
	/* Access point information */
	ap = (pAP_t) wfn->wfn_priv;
	ap_init(ap, config);

	DPRINT(DINFO, "Mode: Access Point IEEE 802.11");
	DPRINT(DINFO, "mac: "f_MACADDR" channel: %d essid: '%s'", MACADDR(ap->mac_address), ap->channel, ap->essid);	
	return wfn;
}
	
