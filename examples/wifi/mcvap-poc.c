/*
 *  Virtual access point
 *
 */

#include <stdlib.h>
#include <pacmap/wifi/protocol_80211.h>
#include <pacmap/wifi/accesspoint.h>
#include "multichannelvap.h"
#include "mcvap-poc.h"

void mcvap_poc_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_beacon(wfn, packet, length, rssi);
}

void mcvap_poc_ap_beacon(struct wifi_fn_t *wfn)
{
	DPRINT(DMAX, "%s: vap_ap_beacon", __FILE__);
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	u_char h80211[4096];
	u_char essid[64];
	int packet_length;

	pClient_t client = ap_get_first_client(ap);
	int channel = ap->channel;
	int apch = wifi_get_channel();
	struct timeval tv;
	gettimeofday(&tv, NULL);

	if (client && tv.tv_sec - init_timer <= 30) 
	{
		channel = ap->channel;
		client->csa_count = 10;
	} 
	else if (client)
	{
		channel = (ap->channel == CHANNEL_AP1) ? CHANNEL_AP2 : CHANNEL_AP1; 
		if (client->csa_count == 0 && apch != channel)
		{
			apch = channel;
			ap->channel = channel;
			wifi_set_channel(channel);
			client->csa_count = -1;
			DPRINT(DINFO, "Channel Switch Announcement %d", channel);
		}
	} 
	
	while (client)
	{
		memset(h80211, 0, 4096);
		packet_length = 0;
		
		sprintf((char *)essid, "%s"f_MACADDR"", ESSID_VIRTUALAP_PREFIX, MACADDR(client->mac_address));
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, BROADCAST, client->bssid, client->bssid, ap_get_seq_ctrl(ap), FC_MGMT_BEACON, 314);
		packet_length = ieee80211_mgmt_forge(h80211, packet_length, ST_BEACON, ap->channel, essid, ap->capability, ap->rates_supp, ap->rates_supp_ext);

		/* Channel switch announcement */
		if (channel != ap->channel && client->csa_count > 0)
		{
			h80211[packet_length++] = 37;	// Element ID 
			h80211[packet_length++] = 3; 	// Length
			h80211[packet_length++] = 0; 	// Channel Switch Mode
			h80211[packet_length++] = channel; 	// New Channel Number
			h80211[packet_length++] = client->csa_count--; 	// Channel Switch Count
		}
		
		// Virtual AP TAG
		h80211[packet_length++] = ELEMENT_PACMAP_RSSI;
		h80211[packet_length++] = sizeof(client->rssi); 
		memcpy(h80211 + packet_length, (char *) &client->rssi, sizeof(client->rssi));
		packet_length += sizeof(client->rssi); 

		h80211[packet_length++] = ELEMENT_PACMAP_SEQ;
		h80211[packet_length++] = sizeof(client->seq); 
		memcpy(h80211 + packet_length, (char *) &client->seq, sizeof(client->seq));
		packet_length += sizeof(client->seq); 

		//copy timestamp into beacon; a mod 2^64 counter incremented each microsecond
		int i;
		for (i = 0; i < 8; i++)
			h80211[24 + i] = (client->timestamp >> (i*8) ) & 0xFF;

		struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
		if (client->seq % 15 == 0 || (channel != ap->channel && client->csa_count > 0))
		{
			DPRINT(DDEBUG, "Sending BEACON from: "f_MACADDR" to: "f_MACADDR" BSSID: "f_MACADDR" essid: %s seq: %d rssi: %d channel: %d/%d", MACADDR(header->sa), MACADDR(header->da), MACADDR(header->bssid), essid, client->seq, client->rssi, channel, apch);
			// if (ap_client_check_timeout(ap, client)) client = NULL;
		}
		
		proto80211_packet_send(h80211, packet_length, 0x02);

		if (client)
			client->seq = (client->seq + 1) % 4096;
		client = ap_get_next_client(ap);
	} 
}

void mcvap_poc_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_auth(wfn, packet, length, rssi);
}

void mcvap_poc_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_deauth(wfn, packet, length, rssi);
}

void mcvap_poc_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_assoc_request(wfn, packet, length, rssi);
}

void mcvap_poc_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_disassoc(wfn, packet, length, rssi);
}

void mcvap_poc_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_reassoc_request(wfn, packet, length, rssi);
}

void mcvap_poc_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_reassoc_response(wfn, packet, length, rssi);
}

/* Handles 802.11 probe request */
void mcvap_poc_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_probe_request(wfn, packet, length, rssi);
}

void mcvap_poc_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_data_data(wfn, packet, length, rssi);
}

void mcvap_poc_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_data_nodata(wfn, packet, length, rssi);
}

void mcvap_poc_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int packet_length)
{
	vap_ti_recv(wfn, packet, packet_length);
}

void mcvap_poc_free(struct wifi_fn_t *wfn)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	ap_free(ap);
}

struct wifi_fn_t *mcvap_poc_init(struct ConfigValues *config)
{
	struct wifi_fn_t *wfn;
	pAP_t ap;

	DPRINT(DMAX, "%s: mcvap_poc_init", __FILE__);
	wfn = wfn_alloc(sizeof(struct AP_conf) + sizeof(struct VapClientList));

	if (!wfn)
		return NULL;
		
	wfn->wifi_st_assoc_request	= mcvap_poc_st_assoc_request;
	wfn->wifi_st_disassoc		= mcvap_poc_st_disassoc;
	wfn->wifi_st_reassoc_request	= mcvap_poc_st_reassoc_request;
	wfn->wifi_st_reassoc_response	= mcvap_poc_st_reassoc_response;
	wfn->wifi_st_probe_request	= mcvap_poc_st_probe_request;
	wfn->wifi_st_beacon	= mcvap_poc_st_beacon;
	wfn->wifi_st_auth	= mcvap_poc_st_auth;
	wfn->wifi_st_deauth	= mcvap_poc_st_deauth;

	wfn->wifi_data_data	= mcvap_poc_data_data;	
	wfn->wifi_data_nodata	= mcvap_poc_data_nodata;	
	wfn->wifi_ti_recv = mcvap_poc_ti_recv;

	wfn->wifi_ap_beacon	= mcvap_poc_ap_beacon;
	wfn->time_beacon = BEACON_INTERVAL_TIMER;

	wfn->wifi_free = mcvap_poc_free;

	
	/*Access point information */
	ap = (pAP_t) wfn->wfn_priv;
	ap_init(ap, config);

	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	memset(vapl, '\0', sizeof(struct VapClientList));

	DPRINT(DINFO, "MULTI-CHANNEL VIRTUAL ACCESS POINT -- PROOF OF CONCEPT");
	DPRINT(DDEBUG, "mac: "f_MACADDR" channel: %d essid: '%s'", MACADDR(ap->mac_address), ap->channel, ap->essid);
	return wfn;
}

