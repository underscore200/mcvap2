#ifndef __PACMAP_ACCESSPOINT_H__
#define __PACMAP_ACCESSPOINT_H__

#include <sys/types.h>
#include <string.h>
#include <pacmap/pacmap.h>
#include <pacmap/wifi/station.h>

#define CHANNEL_AP1		6
#define CHANNEL_AP2		1
#define CLIENT_TIMEOUT_SEC	5

typedef struct AP_conf *pAP_t;
typedef struct ePacket *pPacket_t;

struct PacketQueue
{
	pPacket_t first;
	pPacket_t last;
	int nb_packets;
};

struct ePacket
{
	u_char buffer[4096];
	int length;
	int rate;
	pPacket_t next;
};

struct ClientList 
{
	pClient_t first;
	pClient_t last;
	pClient_t current;
	int nb_clients;
};

/* Access point structure */
struct AP_conf
{
	MACADDR_TYPE(bssid);
	MACADDR_TYPE(mac_address);
	IPADDR_TYPE(ip_address);
	MACADDR_TYPE(br_macaddr);
	unsigned char essid[33];
	int channel;
	int freq;
	int rate;
	int tx_power;
	int encryption;
	char key_wep[256];
	unsigned short interval;
	u_int16_t capability;
	u_int64_t timestamp;
	int seq_ctrl; // we must track sequence number in a per-AP basis, if possible
	struct ClientList client_list;
	int nbpackets_ti;
	u_char rates_supp[EID_SUPPORTED_RATES_LENGHT];
	u_char rates_supp_ext[EID_SUPPORTED_RATES_EXT_LENGHT];
	u_int8_t rssi_threshold;
	struct PacketQueue packet_queue;
};


void ap_update_client_rxinfo(pAP_t ap, MACADDR_TYPE(mac_address), int rssi);
pClient_t ap_get_first_client(pAP_t ap);
pClient_t ap_get_next_client(pAP_t ap);
int ap_client_check_timeout(pAP_t ap, pClient_t client);

pClient_t ap_search_client(pAP_t ap, MACADDR_TYPE(mac_address));

pClient_t ap_add_client(pAP_t ap, MACADDR_TYPE(mac_address), MACADDR_TYPE(bssid));
int ap_delete_client(pAP_t ap, MACADDR_TYPE(mac_address));
// void ap_clean_arp_entry(pAP_t ap, IPADDR_TYPE(ip_address));

int ap_queue_is_empty(pAP_t ap);
void ap_queue_insert_packet(pAP_t ap, void* buf, size_t count, int rate);
int ap_queue_remove_packet(pAP_t ap, pPacket_t packet);

int ap_get_seq_ctrl(pAP_t ap);
int ap_auth_client(pAP_t ap, MACADDR_TYPE(mac_address), MACADDR_TYPE(bssid));
int ap_assoc_client(pAP_t ap, MACADDR_TYPE(mac_address), MACADDR_TYPE(bssid));

void ap_free(pAP_t ap);
void ap_init(pAP_t ap, struct ConfigValues *config);



#endif
