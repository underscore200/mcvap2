/*
 * Example for testing purposes
 * 
 * 15/01/2010	 Maria Eugenia Berezin <berezin @ imag.fr>
 *
 */
 
#include <pacmap/pacmap.h>
#include "wifi/sta_80211.h"

int main(int argc, char** argv) 
{
	pacmap_init(argc, argv, sta_80211_init);
	return 0;
} 

