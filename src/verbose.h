/*
 *  Verbose macros
 *  Based on verbose.h from prawn
 *
 *  05/01/2010	Maria Eugenia Berezin <berezin @ imag.fr>
 *
 */

#ifndef _VERBOSE_H_
#define _VERBOSE_H_

#include <stdio.h>
#include <assert.h>
#include <sys/time.h>

/* Debug and print levels */
#define DCONSOLE 0
#define DERR	 1
#define DINFO	 2
#define DVERBOSE 3
#define DDEBUG   4
#define DMAX	 5
#define DMAX1	 6
#define DMAX2	 7

extern int verbosity;
extern time_t init_timer;
extern FILE *fp_log;

#define DPRINT(level,format,args...) 								\
if (level <= verbosity) {											\
	struct timeval tv1; \
	gettimeofday(&tv1, NULL); \
	int sec = (tv1.tv_sec) % 86400; \
	int usec = tv1.tv_usec; \
	fprintf(stderr, "[%02d:%02d:%02d.%06u] "format"\n", sec / 3600, (sec % 3600) / 60, sec % 60, usec, ## args); \
	if (fp_log) fprintf(fp_log, "[%02d:%02d:%02d.%06u] "format"\n", sec / 3600, (sec % 3600) / 60, sec % 60, usec, ## args); \
} \


#endif /* _VERBOSE_H_ */
