/*
 * Example for testing purposes
 * 
 * 15/01/2010    Maria Eugenia Berezin <berezin @ imag.fr>
 *
 */

#include <pacmap/pacmap.h>
#include <pacmap/wifi/virtualap.h>

int main(int argc, char** argv)
{
        pacmap_init(argc, argv, vap_init);
        return 0;
}

