#!/usr/bin/python

"""
essid = "Test"
It creates a TAP interface 192.168.10.10

@maru
05/01/2010	Connected

"""

import pacmap
try:
	from scapy.all import *
except:
	from scapy import *
import signal
import time, calendar,datetime 

bssid = ""
timestamp = 0

def handler(signal, stackframe):
    print "[py] OUCH"

signal.signal(signal.SIGSEGV, handler)



# PACMAP : function called at launch time
def background():
    global bssid
    bssid = pacmap.getbssid()
    essid = "Test"
    print "[py] Create AP %s" % essid
    pacmap.createvap(essid,bssid,0x6c,"192.168.10.10")
    return 0

# PACMAP : Example of periodic function call : each second, we display a message
#timestamp = 0
# PACMAP : Resolution of timer is 1/512s
def periodic(float1,float2,float3):
    return 0

# PACMAP : Function called at packet reception
def packet_recv(packet, length):
    # PACMAP : return value of 0 for passthrough mode
    return 0

# PACMAP : Function called at packet reception on tap interface
def packet_ti_recv(packet, length,index):
    # PACMAP : return value of 0 for passthrough mode
    return 0

# PACMAP : Function called if 802.11 probe request detected
def proto80211_probereq(packet, length):
    return 0

	# PACMAP : Function called if 802.11 probe request detected
def proto80211_auth(packet, length):

    return 0

def proto80211_deauth(packet, length):
    return 0


def proto80211_assocreq(packet, length):
    return 0

def proto80211_ctrl(packet, length):
	return 0

def proto80211_data(packet, length):
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    return 0

def proto80211_beacon(packet, length, essid):
    return 0

