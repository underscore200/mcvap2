 /*
 *  Virtual access point
 *
 *  TODO:
 *  - More clients. Now we can have just one (192.168.1.123)
 */

#include <stdlib.h>
#include <pacmap/wifi/protocol_80211.h>
#include <pacmap/wifi/accesspoint.h>
#include "virtualap.h"

pVapClient_t vapl_get_station(struct VapClientList *vapl, u_int8_t mac_address[6])
{
	DPRINT(DMAX2, "%s: vapl_get_station", __FILE__);
	pVapClient_t c = vapl->first;

	/* Searching client... */
	while(c && !is_same_mac(mac_address, c->mac))
	{
		c = c->next;
	}
	/* Found client */
	if (c && is_same_mac(mac_address, c->mac))
		return c;
	/* Not found */
	return NULL;
}

int vapl_remove_station(struct VapClientList *vapl, u_int8_t mac_address[6])
{
	DPRINT(DMAX2, "%s: vapl_remove_station", __FILE__);
	pVapClient_t c = vapl->first;
	pVapClient_t prev = NULL;

	/* Search client */
	if (c) 
	{
		while (c && !is_same_mac(mac_address, c->mac)) 
		{
			prev = c;
			c = c->next;
		}
	}

	if (c && is_same_mac(mac_address, c->mac))
	{
		/* Arrange pointers */
		if (vapl->first == c)
		{
			vapl->first = c->next;
			vapl->current = vapl->first;
		}
		/* Client could be the only one */
		if (vapl->last == c)
		{
			vapl->last = prev;
			vapl->current = prev;
		}
		/* Client is in the middle */
		else if (vapl->first != c)
		{
			prev->next = c->next;
			vapl->current = prev;
		}
		vapl->nb_clients--;
		DPRINT(DMAX2, "DEL: vapl->nb_clients %d", vapl->nb_clients);		
		if (c) free(c);
		return 1;
	}
	return 0;
}

int vapl_add_station(struct VapClientList *vapl, u_int8_t mac_address[6], u_int8_t bssid[6], int seq, int channel)
{
	DPRINT(DMAX2, "%s: ap_add_client", __FILE__);
	
	pVapClient_t client = (pVapClient_t) malloc (sizeof(struct vap_client));
	
	/* Copy client information */
	bzero(client ,sizeof(struct vap_client));	
	memcpy(client->mac, mac_address, 6);
	memcpy(client->bssid, bssid, 6);
	client->seq = seq; /* not used */
	client->channel = channel;
	client->rssi = 0;

	if (vapl->nb_clients == 0)
	{
		vapl->first = client;
	}
	else
	{
		vapl->last->next = client;		
	}
	
	vapl->last = client;
	vapl->current = client;
	vapl->nb_clients++;
	DPRINT(DMAX2, "ADD: vapl->nb_clients %d", vapl->nb_clients);		
	return 1;
}

void vapl_free(struct VapClientList *vapl)
{
	pVapClient_t f, c = vapl->first;
	while (c)
	{
		f = c;
		c = f->next;
		free(f);
	}
}

void vap_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	
	/* Parsing frame body */
	struct mgmt_body_t mgmt_body_fields;
	ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
	
	short int rssi_beacon = mgmt_body_fields.pacmap_rssi;
	u_int16_t seq = mgmt_body_fields.pacmap_seq;

	/* Looking for any VAP */
	if ((mgmt_body_fields.ssid.length > 0) && (is_same_ssid_prefix((u_char *)ESSID_VIRTUALAP_PREFIX, mgmt_body_fields.ssid.ssid)))
	{
		MACADDR_TYPE(client_macaddr);
		ato_mac_address(client_macaddr, (char *)mgmt_body_fields.ssid.ssid + strlen(ESSID_VIRTUALAP_PREFIX));

		DPRINT(DMAX2, "BEACON ap "f_MACADDR" is sending beacons essid '%s' for "f_MACADDR" with BSSID "f_MACADDR"", MACADDR(header->sa), mgmt_body_fields.ssid.ssid, MACADDR(client_macaddr), MACADDR(header->bssid));

		pAP_t ap = (pAP_t) wfn->wfn_priv;

		/* Is an associated station? */
		pClient_t c = ap_search_client(ap, client_macaddr);
		
		/* Case 1: client is associated to another AP. Bye. (Added check for case seq=4096) */
		if (c) DPRINT(DMAX, "BEACON-VAP for "f_MACADDR" seq %X c->seq %X (%d)", MACADDR(client_macaddr), seq, c->seq, (seq - c->seq));
		if (c && (seq > c->seq + DELTA_SEQUENCE_CTRL) && (seq < c->seq + DELTA_SEQUENCE_CTRL*10))
		{
			DPRINT(DINFO, "Client "f_MACADDR" is gone", MACADDR(client_macaddr));
			// ap_clean_arp_entry(ap, c->ip_address);
			ap_delete_client(ap, client_macaddr);
			c = NULL;
		}

		struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
		pVapClient_t st = vapl_get_station(vapl, client_macaddr);

		if (st)
		{
			/* Case 2: station was near us, but now is associated to another AP. Bye. */
			if ((st->rssi > rssi_beacon + SIGNAL_FLOOR))
			{
				DPRINT(DINFO, "New VAP Client "f_MACADDR" local RSSI %d > distant RSSI %d (%d)", MACADDR(client_macaddr), st->rssi, rssi_beacon, ( st->rssi- rssi_beacon));

				ap_auth_client(ap, client_macaddr, header->bssid);
				ap_assoc_client(ap, client_macaddr, header->bssid); 
				// ap_clean_arp_entry(ap, st->ip_address);
				
				pClient_t c = ap_search_client(ap, client_macaddr);
				c->seq = (seq + 5) % 4096;
				c->rssi = st->rssi;
				vapl_remove_station(vapl, client_macaddr);
			}
			else DPRINT(DDEBUG, "rssi-st: %d, rssi-beacon: %d (%d)", st->rssi, rssi_beacon, st->rssi - rssi_beacon);
		}
		else if (!c)
		{
			DPRINT(DDEBUG, "vap_add_station "f_MACADDR" AP-rssi %d", MACADDR(client_macaddr), rssi_beacon);
			/* Case 3: first time we hear about this station. Hello! */
			vapl_add_station(vapl, client_macaddr, header->bssid, seq, 0);
		}
		
		if (c && !is_same_mac(header->sa, c->bssid))
			DPRINT(DDEBUG, "Client is associated to "f_MACADDR"!", MACADDR(header->sa));
	}

}

void vap_ap_beacon(struct wifi_fn_t *wfn)
{
	DPRINT(DMAX2, "%s: vap_ap_beacon", __FILE__);
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	u_char h80211[4096];
	u_char essid[64];
	int packet_length;

	memset(h80211, 0, 4096);
	packet_length = 0;
	
	/* For each associated client */
	pClient_t client = ap_get_first_client(ap);
	while (client)
	{
		memset(h80211, 0, 4096);
		packet_length = 0;
		
		sprintf((char *)essid, "%s"f_MACADDR"", ESSID_VIRTUALAP_PREFIX, MACADDR(client->mac_address));
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, BROADCAST, client->bssid, client->bssid, ap_get_seq_ctrl(ap), FC_MGMT_BEACON, 314);
		packet_length = ieee80211_mgmt_forge(h80211, packet_length, ST_BEACON, ap->channel, essid, ap->capability, ap->rates_supp, ap->rates_supp_ext);

		// Virtual AP TAG
		h80211[packet_length++] = ELEMENT_PACMAP_RSSI;
		h80211[packet_length++] = sizeof(client->rssi); 
		memcpy(h80211 + packet_length, (char *) &client->rssi, sizeof(client->rssi));
		packet_length += sizeof(client->rssi); 

		h80211[packet_length++] = ELEMENT_PACMAP_SEQ;
		h80211[packet_length++] = sizeof(client->seq); 
		memcpy(h80211 + packet_length, (char *) &client->seq, sizeof(client->seq));
		packet_length += sizeof(client->seq); 

		client->seq = (client->seq + 1) % 4096;

		//copy timestamp into beacon; a mod 2^64 counter incremented each microsecond
		int i;
		for (i = 0; i < 8; i++)
			h80211[24 + i] = (client->timestamp >> (i*8) ) & 0xFF;

		struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
		if (client->seq % 50 == 0) {
			DPRINT(DDEBUG, "Sending BEACON from: "f_MACADDR" to: "f_MACADDR" BSSID: "f_MACADDR" essid: %s seq: %d rssi: %d", MACADDR(header->sa), MACADDR(header->da), MACADDR(header->bssid), essid, client->seq, client->rssi);
			/* if (ap_client_check_timeout(ap, client)) DPRINT(DINFO, "Client timeout "f_MACADDR"", MACADDR(mac_address)); */
		}
		
		/*  send packet at 1 Mbit/s */
		proto80211_packet_send(h80211, packet_length, 0x02);    

		client = ap_get_next_client(ap);
	}
}

void vap_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	DPRINT(DMAX2, "%s: vap_st_auth bssid: "f_MACADDR" sa: "f_MACADDR"", __FILE__, MACADDR(header->bssid), MACADDR(header->sa));
	//DPRINT(DINFO, "Client asking AUTH: "f_MACADDR" fc: %x", MACADDR(header->sa), header->fc);

	/* Authorization request is for us */
	if (is_same_mac(header->bssid, ap->bssid) && !is_same_mac(header->sa, ap->mac_address))
	{

		/* Add station to authorizated list */
		int status = ap_auth_client(ap, header->sa, header->bssid);

		/* Search client from the list and check sequence number */
		pClient_t c = ap_search_client(ap, header->sa);
		if(c && c->seq == header->seq_ctrl >> 4) {
			DPRINT(DINFO, "Been here with "f_MACADDR" seq: %d", MACADDR(header->sa), header->seq_ctrl >> 4);
			return;
		} else if(c) {
			c->seq = header->seq_ctrl >> 4;
		}
		/* */
		DPRINT(DINFO, "AUTH request by "f_MACADDR" (%s) bssid: "f_MACADDR" seq: %d", MACADDR(header->sa), status?"SUCCESS":"FAIL", MACADDR(header->bssid), header->seq_ctrl);
		if(!status) return; // Don't send auth back

		u_char h80211[4096];
		int packet_length = 0;

		memset(h80211, 0, 4096);
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, header->sa, ap->mac_address, ap->bssid, ap_get_seq_ctrl(ap), FC_MGMT_AUTH, 314);
		packet_length = ieee80211_auth_forge(h80211, packet_length, AUTH_ALGO_OPEN_SYSTEM, IEEE80211_AUTH_OPEN_RESPONSE, IEEE80211_STATUS_SUCCESS);

		/*  send packet at 1 Mbit/s */
		proto80211_packet_send(h80211, packet_length, 0x02);

	}
}

void vap_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	DPRINT(DMAX, "vap_st_deauth,\tsmac: "f_MACADDR", dmac: "f_MACADDR", bssid: "f_MACADDR"", MACADDR(header->sa), MACADDR(header->da), MACADDR(header->bssid));
	pClient_t c = ap_search_client(ap, header->sa);
	if (c)
	{		
		DPRINT(DINFO, "DEAUTH "f_MACADDR"", MACADDR(header->sa));
		ap_delete_client(ap, header->sa);
	}
}

void vap_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	if (is_same_mac(ap->bssid, header->bssid))
	{
		u_char h80211[4096];
		int packet_length = 0;

		u_int16_t status = IEEE80211_STATUS_SUCCESS;
		u_int16_t aid = 0x0100;
		
		memset(h80211, 0, 4096);
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, header->sa, ap->mac_address, ap->bssid, ap_get_seq_ctrl(ap), FC_MGMT_ASSOC_RESPONSE, 314);

		/* Add client */
		if (!ap_assoc_client(ap, header->sa, header->bssid))
			status = IEEE80211_STATUS_UNSPECIFIED;
		else
		{
			ap_update_client_rxinfo(ap, header->sa, rssi);
		}

		/* Search client from the list and check sequence number */
		pClient_t c = ap_search_client(ap, header->sa);
		if(c && c->seq == header->seq_ctrl >> 4) {
			DPRINT(DINFO, "Been here with "f_MACADDR" seq: %d", MACADDR(header->sa), header->seq_ctrl >> 4);
			return;
		} else if(c) {
			c->seq = header->seq_ctrl >> 4;
		} else {
			DPRINT(DINFO, "Asking ASSOC but not in the list, "f_MACADDR" seq: %d", MACADDR(header->sa), header->seq_ctrl >> 4);
		}
		/* */
		DPRINT(DINFO, "ASSOC request by "f_MACADDR" (%s)", MACADDR(header->sa), !status?"SUCCESS":"FAIL");
		packet_length = ieee80211_assoc_forge(h80211, packet_length, ap->capability, status, aid, ap->rates_supp, ap->rates_supp_ext);

	    /*  send packet at 1 Mbit/s */
		proto80211_packet_send(h80211, packet_length, 0x02);
	}
}

void vap_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	pClient_t c = ap_search_client(ap, header->sa);
	if (c)
	{		
		ap_delete_client(ap, header->sa);
		DPRINT(DINFO, "DISASSOC client "f_MACADDR"", MACADDR(header->sa));
	}
}

void vap_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	u_char *current_ap = (u_char *) packet + sizeof(struct mgmt_header_t) + sizeof(u_int16_t)*2;
	DPRINT(DINFO, "REASSOC request by "f_MACADDR" (current ap: "f_MACADDR") to "f_MACADDR", bssid:"f_MACADDR", ap bssid: "f_MACADDR", ap mac: "f_MACADDR"", MACADDR(header->sa), MACADDR(current_ap), MACADDR(header->da), MACADDR(header->bssid), MACADDR(ap->bssid), MACADDR(ap->mac_address));

	// changed header->da to header->sa (client's mac address should be searched)
	pClient_t c = ap_search_client(ap, header->sa);
	if (is_same_mac(ap->mac_address, header->bssid) && c)
	{
		DPRINT(DINFO, "REASSOC for us");
		vap_st_assoc_request(wfn, packet, length, rssi);
	} else if(is_same_mac(ap->mac_address, header->bssid)) {
		DPRINT(DINFO, "ap_search_client FAILED");
	} else {
		DPRINT(DINFO, "REASSOC "f_MACADDR" and "f_MACADDR" are not same", MACADDR(ap->mac_address), MACADDR(header->bssid));
	}
}

void vap_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	DPRINT(DINFO, "REASSOC response by "f_MACADDR"", MACADDR(header->sa));
	if (is_same_mac(ap->mac_address, header->bssid))
	{
	}
}

/* Handles 802.11 probe request */
void vap_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	/* Parsing frame body */
	struct mgmt_body_t mgmt_body_fields;
	ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
	
	u_char essid[64];
	sprintf((char *)essid, "%s"f_MACADDR"", ESSID_VIRTUALAP_PREFIX, MACADDR(header->sa));

	/* Looking for any AP */
	DPRINT(DMAX2, "ST_PROBE_REQUEST\tsmac: "f_MACADDR", essid: '%s', dmac: "f_MACADDR", bssid: "f_MACADDR", rssi: %d", MACADDR(header->sa), 
		mgmt_body_fields.ssid.ssid, MACADDR(header->da), MACADDR(header->bssid), rssi);
	if (((!mgmt_body_fields.ssid.length && is_same_mac(header->bssid, BROADCAST)) || is_same_ssid(mgmt_body_fields.ssid.ssid, essid)))
	{
		struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
		pVapClient_t st = vapl_get_station(vapl, header->sa);
		pClient_t c = ap_search_client(ap, header->sa);
		if (st || c) 
		{
			DPRINT(DDEBUG, "ST_PROBE_REQUEST from: "f_MACADDR" rssi: %d",  MACADDR(header->sa), rssi);
		}
		
		u_char h80211[4096];
		int packet_length = 0;

		DPRINT(DMAX, "Station "f_MACADDR" is looking for '%s' on "f_MACADDR" with BSSID "f_MACADDR" (it's us!)", MACADDR(header->sa), mgmt_body_fields.ssid.ssid, MACADDR(header->da), MACADDR(header->bssid));

		memset(h80211, 0, 4096);
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, header->sa, ap->mac_address, ap->bssid, ap_get_seq_ctrl(ap), FC_MGMT_PROBE_RESPONSE, 314);

		packet_length = ieee80211_mgmt_forge(h80211, packet_length, ST_PROBE_RESPONSE, ap->channel, essid, ap->capability, ap->rates_supp, ap->rates_supp_ext);

		/*  send packet at 1 Mbit/s */
	    proto80211_packet_send(h80211, packet_length, 0x02);
	}
}


void vap_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	pVapClient_t st = vapl_get_station(vapl, header->sa);
	if (st) 
	{
		if (!st->rssi) DPRINT(DINFO, "RSSI "f_MACADDR" %d", MACADDR(header->sa), rssi);
		st->rssi = rssi;
	}

	/* Check if packet is for one of our clients */
	pClient_t c = ap_search_client(ap, header->sa);
	if (!c) return;
	
	/* Check if packet is duplicated */
	if (c->frame_seq != header->seq_ctrl);

	c->rssi = rssi;
	// gettimeofday(&c->tv, NULL);
	
	/* set ToDS=0, FromDS=1 for injecting purposes */
	u_int16_t fc = header->fc;
	fc = (fc & ~FC_TO_DS_BIT) | FC_FROM_DS_BIT;
	header->fc = fc;

	unsigned char h80211[4096];
	int trailer = 0;
	int offset = 0;
	
	/* Destination mac address */
	memcpy(h80211, header->bssid, MACADDR_TYPE_SZ);
	offset += MACADDR_TYPE_SZ;
	/* Source mac address */
	memcpy(h80211 + offset, header->sa, MACADDR_TYPE_SZ);
	offset += MACADDR_TYPE_SZ;
	/* Ether type */
	memcpy(h80211 + offset, packet + sizeof(struct mgmt_header_t) + 6, sizeof(u_int16_t));
	offset += sizeof(u_int16_t);

	u_int16_t ether_type;
	memcpy(&ether_type, packet + sizeof(struct mgmt_header_t) + 6, 2);
	ether_type = ntohs(ether_type);
	
	if (header->seq_ctrl % 10 == 0)
	{
		DPRINT(DDEBUG, "DATA_DATA "f_MACADDR" seq %4d type %s rssi %3d %s", MACADDR(header->sa), header->seq_ctrl, packet_type(ether_type), rssi, get_l4proto(packet, length, ether_type));
		print_icmp_seq(packet, length, ether_type);
	}

	if (length <= sizeof(struct mgmt_header_t) + 8)
		return;

	/* Copy frame body */
	memcpy(h80211 + 14, packet + sizeof(struct mgmt_header_t) + 8, length - sizeof(struct mgmt_header_t) - 8);
	length = length - sizeof(struct mgmt_header_t) - 8 + 14;

	/* ethernet frame must be atleast 60 bytes without fcs */
	if (length < 60)
	{
		trailer = 60 - length;
		bzero(h80211 + length, trailer);
		length += trailer;
	}
	
	c->frame_seq = header->seq_ctrl;
	proto80211_packet_ti_send(h80211, length);
}

void vap_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	pVapClient_t st = vapl_get_station(vapl, header->sa);
	if (st) 
	{
		if (!st->rssi) DPRINT(DINFO, "Updated station "f_MACADDR" rssi %d", MACADDR(header->sa), rssi);
		st->rssi = rssi;
	}
	
	pClient_t c = ap_search_client(ap, header->sa);
	if (c)
	{
		if (header->seq_ctrl % 80 == 0)
		{
			DPRINT(DDEBUG, "DATA_NODATA "f_MACADDR" seq %4d rssi %3d", MACADDR(header->sa), header->seq_ctrl, rssi);
		}
		ap_update_client_rxinfo(ap, header->sa, rssi);
	}	
}
void vap_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length)
{
	/*  802.11 header for sending operations, reusable */
	unsigned char h80211[4096];
	int offset = 0;

	if (packet == NULL) return;
	if (length < 38) return;

	pAP_t ap = (pAP_t) wfn->wfn_priv;

	MACADDR_TYPE(dst_mac);
	memcpy(dst_mac, packet, MACADDR_TYPE_SZ);
	pClient_t client = ap_search_client(ap, dst_mac);
	if (!client && !is_same_mac(dst_mac, BROADCAST))
	{
		// DPRINT(DDEBUG, "TAP Packet to "f_MACADDR" (but it's not a client!)", MACADDR(dst_mac));
		return;
	}

	int packet_length = length;

	memcpy(h80211, IEEE80211_LLC_SNAP, 32);
	memcpy(h80211 + 32, packet + 14, packet_length - 14);
	
	/* Ether type */
	memcpy(h80211 + 30, packet + 12, sizeof(u_int16_t));
	offset += sizeof(u_int16_t);

	u_int16_t ether_type;
	memcpy(&ether_type, packet + 12, 2);
	ether_type = ntohs(ether_type);

	h80211[1] |= 0x02; /* FC_DATA_DATA */
	
	/* Copy MAC address to header */
	if (client)
		memcpy(h80211 + 10, client->bssid, MACADDR_TYPE_SZ); /* BSSID */
	else
		memcpy(h80211 + 10, ap->bssid, MACADDR_TYPE_SZ); /* BSSID */
	memcpy(h80211 + 16, packet + 6,	6); /* SRC_MAC */
	memcpy(h80211 + 4, packet, 6); /* DST_MAC */

	packet_length += (32 - 14); /* 32=IEEE80211+LLC/SNAP; 14=SRC_MAC+DST_MAC+TYPE */

	struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
	DPRINT(DDEBUG, "TAP: da: "f_MACADDR", sa: "f_MACADDR", bssid: "f_MACADDR", type %s %s", MACADDR(header->da), MACADDR(header->sa), MACADDR(header->bssid), packet_type(ether_type), get_l4proto(h80211, packet_length, ether_type));
	print_icmp_seq(h80211, packet_length, ether_type);

	/*  Passthrough mode */
	proto80211_packet_send(h80211, packet_length, RATE_54M/500000);
}
void vap_free(struct wifi_fn_t *wfn)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	ap_free(ap);
	
	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	vapl_free(vapl);
}

void send_ack(u_char *  ra ) {
	unsigned char ackbuf[14];
	 memcpy( ackbuf, "\xD4\x00\x00\x00", 4 );
	 memcpy( ackbuf +  4, ra, 6 );
	 memset( ackbuf + 10, 0, 4 );
	 proto80211_packet_send(ackbuf, 14, 2);
	 return;
	u_char ack_h80211[14];
	int ack_packet_length = 0;

	memset(ack_h80211, 0, 14);
	ack_packet_length = ieee80211_ack_forge(ack_h80211, ack_packet_length, ra, FC_CTRL_ACK, 0);
    DPRINT(DINFO, "sending ACK with length %d", ack_packet_length);
	proto80211_packet_send(ack_h80211, ack_packet_length, 0x02);

}

struct wifi_fn_t *vap_init(struct ConfigValues *config)
{
	struct wifi_fn_t *wfn;
	pAP_t ap;

	DPRINT(DMAX2, "%s: vap_init", __FILE__);
	wfn = wfn_alloc(sizeof(struct AP_conf) + sizeof(struct VapClientList));

	if (!wfn)
		return NULL;
		
	wfn->wifi_st_assoc_request	= vap_st_assoc_request;
	wfn->wifi_st_disassoc		= vap_st_disassoc;
	wfn->wifi_st_reassoc_request	= vap_st_reassoc_request;
	wfn->wifi_st_reassoc_response	= vap_st_reassoc_response;
	wfn->wifi_st_probe_request	= vap_st_probe_request;
	wfn->wifi_st_beacon	= vap_st_beacon;
	wfn->wifi_st_auth	= vap_st_auth;
	wfn->wifi_st_deauth	= vap_st_deauth;
	wfn->wifi_data_data	= vap_data_data;	
	wfn->wifi_data_nodata	= vap_data_nodata;	
	wfn->wifi_ap_beacon	= vap_ap_beacon;
	wfn->wifi_ti_recv = vap_ti_recv;
	wfn->wifi_free = vap_free;

	wfn->time_beacon = BEACON_INTERVAL_TIMER;
	
	/*Access point information */
	ap = (pAP_t) wfn->wfn_priv;
	ap_init(ap, config);

	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	memset(vapl, '\0', sizeof(struct VapClientList));

	DPRINT(DINFO, "Mode: Virtual Access Point (VAP)");
	DPRINT(DINFO, "mac: "f_MACADDR" channel: %d essid prefix: '%s'", MACADDR(ap->mac_address), ap->channel, config->essid_prefix);
	return wfn;
}

