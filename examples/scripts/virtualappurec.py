#!/usr/bin/python

"""
@maru
05/01/2010	Unable to join the network "Client-00:25:00:02:c8:30"
"""

import pacmap
try:
	from scapy.all import *
except:
	from scapy import *
import signal
import time, calendar,datetime 

# Parameters for the AP
tap_ip_address="192.168.10.10"
tap_mac_address="00:11:22:33:44:55"
bssid = "00:11:22:33:44:55"
floor = 15 # seuil de detection de mobilite
expiracy = 10 # expiration des clients connectes
delta = 2 # nombre de balises recues avant disparition locale du terminal

# global variables
timestamp = 0
index=-1
def handler(signal, stackframe):
	print "[py] OUCH"

signal.signal(signal.SIGSEGV, handler)



# PACMAP : function called at launch time
def background():
	global bssid
	global index
	global tap_ip_address
	global tap_mac_address
	print "[py] BSSID from MAC Adapter is %s" % bssid
	print "[py] Virtual AP 0.1 native version by Yan Grunenberger"
	print "[py] Written using PacMap"
	pacmap.setupvirtualap(tap_mac_address,tap_ip_address)
	return 0

# PACMAP : Example of periodic function call : each second, we display a message
#timestamp = 0
# PACMAP : Resolution of timer is 1/512s
def periodic(float1,float2,float3):
	# print "[py] periodic"
	return 0

# PACMAP : Function called at packet reception
def packet_recv(packet, length):
	# PACMAP : return value of 0 for passthrough mode
	# print "[py] packet_recv"
	return 0

# PACMAP : Function called at packet reception on tap interface
def packet_ti_recv(packet, length,index):
	# PACMAP : return value of 0 for passthrough mode
	# print "[py] packet_ti_recv"
	return 0

# PACMAP : Function called if 802.11 probe request detected
def proto80211_probereq(packet, length):
	# dot11_frame = Packet(packet)
	# dot11_frame.decode_payload_as(Dot11)
	# print "[py] Scapy interpretation %s" % dot11_frame.summary()
	# print "[py] proto80211_probereq"
	return 0

	# PACMAP : Function called if 802.11 probe request detected
def proto80211_auth(packet, length):
	print "[py] proto80211_auth"
	return 0

def proto80211_deauth(packet, length):
	print "[py] proto80211_deauth"
	return 0


def proto80211_assocreq(packet, length):
	print "[py] proto80211_assocreq"
	return 0
	
def proto80211_beacon(packet, length, essid):
	# print "[py] proto80211_beacon"
	return 0

def proto80211_ctrl(packet, length):
	# print "[py] proto80211_ctrl"
	return 0

def proto80211_data(packet, length):
	# print "[py] proto80211_data"
	return 0

