  
/*
 *  Virtual access point
 *
 */

#include <stdlib.h>
#include <pacmap/wifi/protocol_80211.h>
#include <pacmap/wifi/accesspoint.h>
#include "mcvap-monitor.h"

void mcvap_monitor_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	
	/* Parsing frame body */
	struct mgmt_body_t mgmt_body_fields;
	ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
	
	short int rssi_beacon = mgmt_body_fields.pacmap_rssi;
	u_int16_t seq = mgmt_body_fields.pacmap_seq;

	pAP_t ap = (pAP_t) wfn->wfn_priv;
	int neighbour_channel = ap->channel == 1 ? 6 : 1;
	
	/* Looking for any VAP */
	if ((mgmt_body_fields.ssid.length > 0) && (is_same_ssid_prefix((u_char *)ESSID_VIRTUALAP_PREFIX, mgmt_body_fields.ssid.ssid)))
	{
		MACADDR_TYPE(client_macaddr);
		ato_mac_address(client_macaddr, (char *)mgmt_body_fields.ssid.ssid + strlen(ESSID_VIRTUALAP_PREFIX));

		DPRINT(DMAX, "BEACON ap "f_MACADDR" is sending beacons essid '%s' for "f_MACADDR" with BSSID "f_MACADDR"", MACADDR(header->sa), mgmt_body_fields.ssid.ssid, MACADDR(client_macaddr), MACADDR(header->bssid));

		/* Is an associated station? */
		pClient_t c = ap_search_client(ap, client_macaddr);
		
		/* Case 1: client is associated to another AP. Bye. (Added check for case seq=4096) */
		if (c && (seq > c->seq + DELTA_SEQUENCE_CTRL) && (seq < c->seq + DELTA_SEQUENCE_CTRL*10) && c->csa_count == 0)
		{
			c->new_channel = neighbour_channel;	// New Channel Number
			c->csa_count = CHANNEL_SWITCH_COUNT;	// Channel Switch Count
			DPRINT(DINFO, "CSA (Channel Switch Announcement) for "f_MACADDR" to channel %d", MACADDR(client_macaddr), c->new_channel);
		}

		struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
		pVapClient_t st = vapl_get_station(vapl, client_macaddr);

		if (st)
		{
			/* Case 2: station was near us, but now is associated to another AP. Bye. */
			if ((st->rssi > rssi_beacon + SIGNAL_FLOOR))
			{
				DPRINT(DINFO, "New VAP Station "f_MACADDR"", MACADDR(client_macaddr));
				DPRINT(DDEBUG, "Client "f_MACADDR" has local RSSI %d > distant RSSI %d (%d) clients: %d seq: %d", MACADDR(client_macaddr), st->rssi, rssi_beacon, ( st->rssi- rssi_beacon), vapl->nb_clients, seq);

				ap_auth_client(ap, client_macaddr, header->bssid); 
				ap_assoc_client(ap, client_macaddr, header->bssid); 
				// ap_clean_arp_entry(ap, st->ip_address); 
				
				pClient_t c = ap_search_client(ap, client_macaddr);
				c->seq = (seq + 5) % 4096;
				c->rssi = st->rssi;
				vapl_remove_station(vapl, client_macaddr);
			}
		}
		else if (!c)
		{
			DPRINT(DDEBUG, "vap_add_client "f_MACADDR" AP-rssi %d", MACADDR(client_macaddr), rssi_beacon);
			/* Case 3: first time we hear about this station. Hello! */
			vapl_add_station(vapl, client_macaddr, header->bssid, seq, 0);
		}
		
		if (c && !is_same_mac(header->sa, c->bssid))
			DPRINT(DDEBUG, "Client is associated to "f_MACADDR"!", MACADDR(header->sa));
	}
}

void mcvap_monitor_ap_beacon(struct wifi_fn_t *wfn)
{
	DPRINT(DMAX, "%s: vap_ap_beacon", __FILE__);
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	u_char h80211[4096];
	u_char essid[64];
	int packet_length;

	memset(h80211, 0, 4096);
	packet_length = 0;
	int apch = wifi_get_channel();
	
	/* For each associated client */
	pClient_t client = ap_get_first_client(ap);
	while (client)
	{
		memset(h80211, 0, 4096);
		packet_length = 0;
		
		sprintf((char *)essid, "%s"f_MACADDR"", ESSID_VIRTUALAP_PREFIX, MACADDR(client->mac_address));
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, BROADCAST, client->bssid, client->bssid, ap_get_seq_ctrl(ap), FC_MGMT_BEACON, 314);
		packet_length = ieee80211_mgmt_forge(h80211, packet_length, ST_BEACON, ap->channel, essid, ap->capability, ap->rates_supp, ap->rates_supp_ext);

		/* Channel switch announcement */
		if (client->csa_count > 0)
		{
			h80211[packet_length++] = 37;	// Element ID 
			h80211[packet_length++] = 3; 	// Length
			h80211[packet_length++] = 0; 	// Channel Switch Mode
			h80211[packet_length++] = client->new_channel; 	// New Channel Number
			h80211[packet_length++] = client->csa_count--; 	// Channel Switch Count
		}

		// Virtual AP TAG
		h80211[packet_length++] = ELEMENT_PACMAP_RSSI;
		h80211[packet_length++] = sizeof(client->rssi); 
		memcpy(h80211 + packet_length, (char *) &client->rssi, sizeof(client->rssi));
		packet_length += sizeof(client->rssi); 

		h80211[packet_length++] = ELEMENT_PACMAP_SEQ;
		h80211[packet_length++] = sizeof(client->seq); 
		memcpy(h80211 + packet_length, (char *) &client->seq, sizeof(client->seq));
		packet_length += sizeof(client->seq); 

		client->seq = (client->seq + 1) % 4096;
		
		//copy timestamp into beacon; a mod 2^64 counter incremented each microsecond
		int i;
		for (i = 0; i < 8; i++)
			h80211[24 + i] = (client->timestamp >> (i*8) ) & 0xFF;

		
		if (client->seq % 50 == 0)
		{
			struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
			DPRINT(DDEBUG, "Sending BEACON from: "f_MACADDR" to: "f_MACADDR" BSSID: "f_MACADDR" essid: %s seq: %d rssi: %d", MACADDR(header->sa), MACADDR(header->da), MACADDR(header->bssid), essid, client->seq, client->rssi);
			/* if (ap_client_check_timeout(ap, client)) DPRINT(DINFO, "Client timeout "f_MACADDR"", MACADDR(mac_address)); */
		}
		
		if (apch != ap->channel)
		{
			wifi_set_channel(ap->channel);
		}
		/*  send packet at 1 Mbit/s */
		proto80211_packet_send(h80211, packet_length, 0x02);
		if (apch != ap->channel)
		{
			int neighbour_channel = ap->channel == 1 ? 6 : 1;
			wifi_set_channel(neighbour_channel);
		}

		if (client->new_channel && client->csa_count == 0)
		{
			DPRINT(DINFO, "Client "f_MACADDR" is gone", MACADDR(client->mac_address));
			// ap_clean_arp_entry(ap, client->ip_address);
			ap_delete_client(ap, client->mac_address);
			client = NULL;
		}
		client = ap_get_next_client(ap);
	} 

}

void mcvap_monitor_queue_packet_send(struct wifi_fn_t *wfn)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	struct ePacket packet;
	while (!ap_queue_is_empty(ap))
	{
		ap_queue_remove_packet(ap, &packet);
		struct mgmt_header_t *header = (struct mgmt_header_t *) packet.buffer;

		u_int16_t ether_type;
		memcpy(&ether_type, packet.buffer + 30, 2);
		ether_type = ntohs(ether_type);
		
		DPRINT(DDEBUG, "TAP: da: "f_MACADDR", sa: "f_MACADDR", bssid: "f_MACADDR", type %s%s", MACADDR(header->da), MACADDR(header->sa), MACADDR(header->bssid), packet_type(ether_type), get_l4proto(packet.buffer, packet.length, ether_type));
		print_icmp_seq(packet.buffer, packet.length, ether_type);

		proto80211_packet_send(packet.buffer, packet.length, packet.rate);
	}
}



void mcvap_monitor_monitor_channels(struct wifi_fn_t *wfn)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	if (wifi_get_channel() != ap->channel || wfn->time_monitor_channels != NEIGHB_CHANN_TIMER)
	{
		wfn->time_monitor_channels = NEIGHB_CHANN_TIMER;
		DPRINT(DDEBUG, "Set Channel to %d", ap->channel);
		wifi_set_channel(ap->channel);
		/* Check if there is any queued packet to send */
		mcvap_monitor_queue_packet_send(wfn);
	}
	else
	{
		wfn->time_monitor_channels = TIME_MONITOR_CHANNELS;
		if (NEIGHB_CHANN_TIMER)
		{
			int neighbour_channel = ap->channel == 1 ? 6 : 1;
			DPRINT(DDEBUG, "Set Channel to %d (neighb)", neighbour_channel);
			wifi_set_channel(neighbour_channel);
		}
	}
}

void mcvap_monitor_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_auth(wfn, packet, length, rssi);
}

void mcvap_monitor_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_deauth(wfn, packet, length, rssi);
}

void mcvap_monitor_st_assoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_assoc_request(wfn, packet, length, rssi);
}

void mcvap_monitor_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_disassoc(wfn, packet, length, rssi);
}

void mcvap_monitor_st_reassoc_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_reassoc_request(wfn, packet, length, rssi);
}

void mcvap_monitor_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_reassoc_response(wfn, packet, length, rssi);
}

void mcvap_monitor_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_st_probe_request(wfn, packet, length, rssi);
}

void mcvap_monitor_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pAP_t ap = (pAP_t) wfn->wfn_priv;

	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	pVapClient_t st = vapl_get_station(vapl, header->sa);
	if (st) 
	{
		if (!st->rssi) DPRINT(DINFO, "RSSI "f_MACADDR" %d", MACADDR(header->sa), rssi);
		st->rssi = rssi;
	}

	/* Check if packet is for one of our clients */
	pClient_t c = ap_search_client(ap, header->sa);
	if (!c) return;
	
	/* Check if packet is duplicated */
	if (c->frame_seq != header->seq_ctrl)
	{
		c->rssi = rssi;
		gettimeofday(&c->tv, NULL);
		
		/* set ToDS=0, FromDS=1 for injecting purposes */
		u_int16_t fc = header->fc;
		fc = (fc & ~FC_TO_DS_BIT) | FC_FROM_DS_BIT;
		header->fc = fc;

        unsigned char h80211[4096];
		int trailer = 0;
		int offset = 0;
		u_int16_t ether_type;
		
		/* Destination mac address */
		memcpy(h80211, header->bssid, sizeof(header->bssid));
		offset += sizeof(header->bssid);
		/* Source mac address */
		memcpy(h80211 + offset, header->sa, sizeof(header->sa));
		offset += sizeof(header->sa);
		/* Ether type */
		memcpy(h80211 + offset, packet + sizeof(struct mgmt_header_t) + 6, sizeof(u_int16_t));
		offset += sizeof(u_int16_t);

		memcpy(&ether_type, packet + sizeof(struct mgmt_header_t) + 6, 2);
		ether_type = ntohs(ether_type);
		
		if (header->seq_ctrl % 10 == 0)
		{
			DPRINT(DDEBUG, "DATA_DATA "f_MACADDR" seq %4d type %s rssi %3d %s", MACADDR(header->sa), header->seq_ctrl, packet_type(ether_type), rssi, get_l4proto(packet, length, ether_type));
			print_icmp_seq(packet, length, ether_type);
		}

		if (length <= sizeof(struct mgmt_header_t) + 8)
			return;

		/* Copy frame body */
 		memcpy(h80211 + 14, packet + sizeof(struct mgmt_header_t) + 8, length - sizeof(struct mgmt_header_t) - 8);
		length = length - sizeof(struct mgmt_header_t) - 8 + 14;

		/* ethernet frame must be atleast 60 bytes without fcs */
		if (length < 60)
		{
			trailer = 60 - length;
			bzero(h80211 + length, trailer);
			length += trailer;
		}
		
		c->frame_seq = header->seq_ctrl;
		proto80211_packet_ti_send(h80211, length);
	}
}

void mcvap_monitor_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	vap_data_nodata(wfn, packet, length, rssi);
}

void mcvap_monitor_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length)
{	
	/*  802.11 header for sending operations, reusable */
	unsigned char h80211[4096];
	int offset = 0;

	if (packet == NULL) return;
	if (length < 38) return;

	pAP_t ap = (pAP_t) wfn->wfn_priv;

	MACADDR_TYPE(dst_mac);
	memcpy(dst_mac, packet, sizeof(u_int8_t)*6);
	pClient_t client = ap_search_client(ap, dst_mac);
	if (!client && !is_same_mac(dst_mac, BROADCAST))
	{
		// DPRINT(DDEBUG, "TAP Packet to "f_MACADDR" (but it's not a client!)", MACADDR(dst_mac));
		return;
	}

	int packet_length = length;

	memcpy(h80211, IEEE80211_LLC_SNAP, 32);
	memcpy(h80211 + 32, packet + 14, packet_length - 14);
	
	/* Ether type */
	memcpy(h80211 + 30, packet + 12, sizeof(u_int16_t));
	offset += sizeof(u_int16_t);

	u_int16_t ether_type;
	memcpy(&ether_type, packet + 12, 2);
	ether_type = ntohs(ether_type);

	h80211[1] |= 0x02; /* FC_DATA_DATA */
	
	/* Copy MAC address to header */
	if (client)
		memcpy(h80211 + 10, client->bssid, sizeof(u_int8_t)*6); /* BSSID */
	else
		memcpy(h80211 + 10, ap->bssid, sizeof(u_int8_t)*6); /* BSSID */
	memcpy(h80211 + 16, packet + 6,	6); /* SRC_MAC */
	memcpy(h80211 + 4, packet, 6); /* DST_MAC */

	packet_length += (32 - 14); /* 32=IEEE80211+LLC/SNAP; 14=SRC_MAC+DST_MAC+TYPE */

	/* Am I in the good channel ? */
	if (wifi_get_channel() != ap->channel)
	{
		ap_queue_insert_packet(ap, h80211, packet_length, RATE_54M/500000);
		return;
	}

	struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
	DPRINT(DDEBUG, "TAP: da: "f_MACADDR", sa: "f_MACADDR", bssid: "f_MACADDR", type %s%s", MACADDR(header->da), MACADDR(header->sa), MACADDR(header->bssid), packet_type(ether_type), get_l4proto(h80211, packet_length, ether_type));
	print_icmp_seq(h80211, packet_length, ether_type);

	/*  Passthrough mode */
	proto80211_packet_send(h80211, packet_length, RATE_54M/500000);
}

void mcvap_monitor_free(struct wifi_fn_t *wfn)
{
	pAP_t ap = (pAP_t) wfn->wfn_priv;
	ap_free(ap);
}

struct wifi_fn_t *mcvap_monitor_init(struct ConfigValues *config)
{
	struct wifi_fn_t *wfn;
	pAP_t ap;

	DPRINT(DMAX, "%s: mcvap_monitor_init", __FILE__);
	wfn = wfn_alloc(sizeof(struct AP_conf) + sizeof(struct VapClientList));

	if (!wfn)
		return NULL;
		
	wfn->wifi_packet_recv = proto80211_packet_recv;
	wfn->wifi_packet_ti_recv = proto80211_packet_ti_recv;
	wfn->wifi_st_assoc_request	= mcvap_monitor_st_assoc_request;
	wfn->wifi_st_disassoc		= mcvap_monitor_st_disassoc;
	wfn->wifi_st_reassoc_request	= mcvap_monitor_st_reassoc_request;
	wfn->wifi_st_reassoc_response	= mcvap_monitor_st_reassoc_response;
	wfn->wifi_st_probe_request	= mcvap_monitor_st_probe_request;
	wfn->wifi_st_beacon	= mcvap_monitor_st_beacon;
	wfn->wifi_st_auth	= mcvap_monitor_st_auth;
	wfn->wifi_st_deauth	= mcvap_monitor_st_deauth;

	wfn->wifi_data_data	= mcvap_monitor_data_data;	
	wfn->wifi_data_nodata	= mcvap_monitor_data_nodata;	
	wfn->wifi_ti_recv = mcvap_monitor_ti_recv;

	wfn->wifi_ap_beacon	= mcvap_monitor_ap_beacon;
	wfn->time_beacon = BEACON_INTERVAL_TIMER;

	wfn->wifi_monitor_channels = mcvap_monitor_monitor_channels;	
	wfn->time_monitor_channels = BEACON_INTERVAL_TIMER*3;

	wfn->wifi_free = mcvap_monitor_free;

	/*Access point information */
	ap = (pAP_t) wfn->wfn_priv;
	ap_init(ap, config);

	struct VapClientList *vapl = (struct VapClientList *) (wfn->wfn_priv + sizeof(struct AP_conf));
	memset(vapl, '\0', sizeof(struct VapClientList));

	DPRINT(DINFO, "MULTI-CHANNEL VIRTUAL ACCESS POINT");
	DPRINT(DDEBUG, "mac: "f_MACADDR" channel: %d essid: '%s'", MACADDR(ap->mac_address), ap->channel, ap->essid);
	return wfn;
}

