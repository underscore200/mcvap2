#include <stdio.h>
#include <pacmap/wifi/protocol_80211.h>
#include "station.h"

void _read_from_console(unsigned char s[])
{
	int max_len = 32, i;
	char buffer[1000], c;

	for (i = 0; i < max_len; i++)
	{
		c = getchar();
		if (c == '\n') 
		{
			buffer[i] = '\0';
			break;
		}
		buffer[i] = c;
	}
	if (i > 0)
	{
		memset(s, '\0', max_len);
		memcpy(s, buffer, i*sizeof(unsigned char));
	}
}

void sta_read_options(pClient_t sta)
{
	DPRINT(DCONSOLE, "\n\nYou can choose an ESSID to connect to.");
	DPRINT(DCONSOLE, "Please write the ESSID (%s): ", strlen((char *)sta->ap->essid) ? (char *)sta->ap->essid : "or just press enter");
	_read_from_console(sta->ap->essid);	
}

int sta_get_next_channel(pClient_t sta)
{
	int ch = sta->channel + 1;
	if (ch > 11) ch = 1;
	sta->channel = ch;
	return ch;
}

pAP_info sta_new_ap_info(u_int8_t bssid[6], u_int8_t mac_address[6], unsigned char essid[33], int channel, u_int8_t rssi, u_int64_t timestamp, u_int16_t capability)
{
	pAP_info new_ap = (pAP_info) malloc(sizeof(struct AP_info));
	memset(new_ap, 0, sizeof(struct AP_info));
	memcpy(new_ap->bssid, bssid, MACADDR_TYPE_SZ);
	memcpy(new_ap->mac_address, mac_address, MACADDR_TYPE_SZ);
	memcpy(new_ap->essid, essid, strlen((char *)essid));
	new_ap->channel = channel;
	new_ap->timestamp = timestamp;
	new_ap->capability = capability;
	new_ap->rssi = rssi;
	gettimeofday(&new_ap->tv, NULL);
	return new_ap;
}

int is_same_ap(pAP_info ap1, pAP_info ap2)
{
	if (is_same_mac(ap1->bssid, ap2->bssid) && is_same_mac(ap1->mac_address, ap2->mac_address) \
	 	&& is_same_ssid(ap1->essid, ap2->essid) && (ap1->channel == ap2->channel))
		return 1;
	return 0;
}

int sta_get_ap_info(pClient_t sta, pAP_info myap)
{
	pAP_info ap = sta->ap_info_list.first;
	while (ap)
	{
		if (is_same_ssid(ap->essid, myap->essid) || is_same_mac(ap->mac_address, myap->mac_address))
		{
			strcpy((char *)myap->essid, (char *)ap->essid);
			memcpy(myap->mac_address, ap->mac_address, MACADDR_TYPE_SZ);
			memcpy(myap->bssid, ap->bssid, MACADDR_TYPE_SZ);
			myap->channel = ap->channel;
			return 1;
		}
		ap = ap->next;
	}
	return 0;
}

void sta_ap_list_timeout(pClient_t sta)
{
	pAP_info ap = sta->ap_info_list.first;
	pAP_info prev_ap = NULL;

	/* First, looks for the ap */
	while (ap)
	{
		if (sta_ap_timeout(sta, ap))
		{
			sta_ap_free(sta, ap, prev_ap);
			break;
		}
		prev_ap = ap;
		ap = ap->next;
	}
}

void sta_ap_free(pClient_t sta, pAP_info ap, pAP_info prev_ap)
{
	if (prev_ap)
	{
		prev_ap->next = ap->next;
	}
	if (sta->ap_info_list.first == ap)
	{
		sta->ap_info_list.first = ap->next;
	}
	if (sta->ap_info_list.last == ap)
	{
		sta->ap_info_list.last = prev_ap;
	}
	sta->ap_info_list.nb_aps--;
	free(ap);
}

void sta_save_ap_info(pClient_t sta, pAP_info new_ap)
{
	pAP_info ap = sta->ap_info_list.first;
	pAP_info prev_ap = NULL;

	/* First, looks for the ap */
	while (ap)
	{
		if (is_same_ap(ap, new_ap))
		{
			sta_ap_free(sta, ap, prev_ap);
			break;
		}
		prev_ap = ap;
		ap = ap->next;
	}

	/* Then inserts new ap */
	prev_ap = NULL;
	ap = sta->ap_info_list.first;
	while (ap != NULL && ap->rssi >= new_ap->rssi)
	{
		prev_ap = ap;
		ap = ap->next;
	}
	/* Case 0 elements
	 * Case 1-N elements, new_ap in 1st place */
	if (sta->ap_info_list.first == NULL || prev_ap == NULL)
	{
		sta->ap_info_list.first = new_ap;
	}
	/* Case 0 elements
	 * Case 1-N elements, new_ap is last place */
	if (sta->ap_info_list.last == NULL || ap == NULL)
	{
		sta->ap_info_list.last = new_ap;
	}
	/* Case 1 element, new_ap in 2nd place
	 * Case N elements, new_ap in Xth place */
	if (prev_ap)
	{
		prev_ap->next = new_ap;
	}
	/* Case 1-N elements, new_ap is Xth place */
	new_ap->next = ap;
	sta->ap_info_list.nb_aps++;
}

void sta_print_ap_list(pClient_t sta)
{
	pAP_info ap = sta->ap_info_list.first;
	DPRINT(DCONSOLE, "-(%02d)-------------------------------------------------------------------------------------", sta->ap_info_list.nb_aps);
	DPRINT(DCONSOLE, "   %-25s %-17s %-17s  %s  %s  %s", "ESSID", "BSSID", "MAC_ADDRESS", "CH", "RSSI", "WEP");
	DPRINT(DCONSOLE, "------------------------------------------------------------------------------------------");
	int i = 1;
	while (ap)
	{
		DPRINT(DCONSOLE, "%02d %-25s "f_MACADDR" "f_MACADDR"  %02d  %d   %s", i++, ap->essid, MACADDR(ap->bssid), MACADDR(ap->mac_address), ap->channel, ap->rssi, CAPABILITY_PRIVACY(ap->capability) ? "Y" : "N");
		ap = ap->next;
	}
}

void sta_save_ap_rssi(pClient_t sta, u_int8_t rssi)
{
	pAP_info ap = sta->ap;
	ap->rssi = rssi;
	gettimeofday(&ap->tv, NULL);
}

int sta_ap_timeout(pClient_t sta, pAP_info ap)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	if (tv.tv_sec - ap->tv.tv_sec > AP_TIMEOUT_SEC)
	{
		return 1;
	}
	return 0;
}

void sta_free(pClient_t sta)
{
	if (sta->ap)
		free(sta->ap);

	pAP_info _ap, ap = sta->ap_info_list.first;
	while (ap)
	{
		_ap = ap;
		ap = ap->next;
		free(_ap);
	}		
}
void sta_init(pClient_t sta, u_int8_t mac_address[6])
{
	/*  at this point, we have to fill sta_conf structure	 */
	memset(sta, 0, sizeof(struct ST_info));
	memcpy(sta->mac_address, mac_address, MACADDR_TYPE_SZ);

	sta->ap = (pAP_info) malloc(sizeof(struct AP_info));
	memset(sta->ap, 0, sizeof(struct AP_info));
	memset(&sta->ap_info_list, 0, sizeof(struct APInfoList));

	sta->capability = 0x421;
	sta->interval = BEACON_INTERVAL;
	
	int i = 0;
	memset(sta->rates_supp, 0, sizeof(sta->rates_supp)); 
	memset(sta->rates_supp + i++, RATE_1M/500000 | 0x80, 1); 
	memset(sta->rates_supp + i++, RATE_2M/500000 | 0x80, 1);
	memset(sta->rates_supp + i++, RATE_5_5M/500000 | 0x80, 1); 
	memset(sta->rates_supp + i++, RATE_11M/500000 | 0x80, 1); 
	memset(sta->rates_supp + i++, RATE_6M/500000, 1); 
	memset(sta->rates_supp + i++, RATE_9M/500000, 1); 
	memset(sta->rates_supp + i++, RATE_12M/500000, 1); 
	memset(sta->rates_supp + i++, RATE_18M/500000, 1); 
	
	i = 0;
	memset(sta->rates_supp_ext, 0, sizeof(sta->rates_supp_ext)); 
	memset(sta->rates_supp_ext + i++, RATE_24M/500000, 1); 
	memset(sta->rates_supp_ext + i++, RATE_36M/500000, 1); 
	memset(sta->rates_supp_ext + i++, RATE_48M/500000, 1); 
	memset(sta->rates_supp_ext + i++, RATE_54M/500000, 1); 
		
	/* XXX One client only... */
	memcpy(sta->ip_address, "\300\250\001\173", IPADDR_TYPE_SZ);
}
