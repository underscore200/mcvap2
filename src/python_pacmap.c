#include <Python.h>

static PyObject *PacMapError;

static PyMethodDef PacMapMethods[] = {
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


// Module initialization
PyMODINIT_FUNC initpacmap(void) 
{
    PyObject *m;

    m = Py_InitModule("pacmap", PacMapMethods);
    if (m == NULL)
        return;

    PacMapError = PyErr_NewException("pacmap.error", NULL, NULL);
    Py_INCREF(PacMapError);
    PyModule_AddObject(m, "error", PacMapError);
}


/*
static PyObject* pacmap_sendtipacket(PyObject *self, PyObject *args);
static PyObject* pacmap_sendtipacketbybssid(PyObject *self, PyObject *args);
static PyObject* pacmap_getrxinfo(PyObject *self, PyObject *args);
static PyObject* pacmap_sendpacket(PyObject *self, PyObject *args);
static PyObject* pacmap_createvap(PyObject *self, PyObject *args);
static PyObject* pacmap_destroyvap(PyObject *self, PyObject *args);
static PyObject* pacmap_getbssid(PyObject *self, PyObject *args);
static PyObject* pacmap_getsmac(PyObject *self, PyObject *args);
static PyObject* pacmap_createtap(PyObject *self, PyObject *args);
static PyObject* pacmap_destroytap(PyObject *self, PyObject *args);
static PyObject* pacmap_settapmac(PyObject *self, PyObject *args);
static PyObject* pacmap_settapip(PyObject *self, PyObject *args);
static PyObject* pacmap_setupvirtualap(PyObject *self, PyObject *args);
* /

/ * Send arbitrary packet on tap interface * /
static PyObject* pacmap_sendtipacket(PyObject *self, PyObject *args)
{
	char* packet;
	int length;
	int index;
	int result = 1;
	int rate;
	struct tif *dv_ti = NULL;

	/ *  get essid and bssid from arguments * /
	DPRINT(DDEBUG, "%s: pacmap_sendtipacket", __FILE__);
	if (!PyArg_ParseTuple(args, "s#ii", &packet, &length, &length, &index))
		return NULL;
	else
	{
		dv_ti = virtualintf[index];

		if (dv_ti)
		{
			result = ti_write(dv_ti, packet, length);
		}

		return Py_BuildValue("i", result);
	}

}



static PyObject* pacmap_sendtipacketbybssid(PyObject *self, PyObject *args)
{
	char* packet;
	char* bssid;
	int length;
	int len = 6;
	int result = 1;
	int rate;
	struct tif *dv_ti = NULL;

	/ *  get essid and bssid from arguments * /
	DPRINT(DDEBUG, "%s: pacmap_sendtipacketbybssid", __FILE__);
	if (!PyArg_ParseTuple(args, "s#is#", &packet, &length, &length, &bssid, &len))
		return NULL;
	else
	{
		/ * warning: assignment makes pointer from integer without a cast * /
		dv_ti = getdevbybssid(bssid); 

		if (dv_ti)
		{
			result = ti_write(dv_ti, packet, length);
		}

		return Py_BuildValue("i", result);
	}

}

static PyObject* pacmap_getrxinfo(PyObject *self, PyObject *args)
{
	int result;
	int param;

	DPRINT(DDEBUG, "%s: pacmap_getrxinfo", __FILE__);
	if (!PyArg_ParseTuple(args, "i", &param))
		return NULL;
	else
	{

		/ * uint64_t ri_mactime;
		uint32_t ri_power;
		uint32_t ri_noise;
		uint32_t ri_channel;
		uint32_t ri_freq;
		uint32_t ri_rate;
		uint32_t ri_antenna;* /


		switch (param)
		{
		case 0: / *  MAC time * /
			result = last_rx_info->ri_mactime;
			break;
		case 1: / *  Power * /
			result = last_rx_info->ri_power;
			break;
		case 2: / *  Noise` * /
			result = last_rx_info->ri_noise;
			break;
		case 3: / *  Channel * /
			result = last_rx_info->ri_channel;
			break;
		case 4: / *  Freq * /
			result = last_rx_info->ri_freq;
			break;
		case 5: / *  Rate * /
			result = last_rx_info->ri_rate;
			break;
		case 6: / *  Antenna * /
			result = last_rx_info->ri_antenna;
			break;
		default:
			result = 0;
		}

		return Py_BuildValue("i", result);
	}

}

/ * send_packet(packet, length); * /
/ * Send arbitrary packet * /
static PyObject* pacmap_sendpacket(PyObject *self, PyObject *args)
{
	char* packet;
	int length;
	int rate;
	/ * int str_length; * /
	int result = 1;

	DPRINT(DDEBUG, "%s: pacmap_sendpacket", __FILE__);
	/ *  get essid and bssid from arguments * /
	if (!PyArg_ParseTuple(args, "s#ii", &packet, &length, &length, &rate))
		return NULL;
	else {
		/ * DPRINT(DINFO, "Sending packet with length %d",length); * /
		result = send_packet(packet, length, rate);

		return Py_BuildValue("i", result);
	}
}

/ * Create an Access point and return result * /
static PyObject* pacmap_createvap(PyObject *self, PyObject *args)
{
	char* essid;
	char* bssid;
	char* ip;
	int len = 6;
	int result = 1;
	int rate;
	
	/ *  get essid and bssid from arguments * /
	DPRINT(DDEBUG, "%s: pacmap_createvap", __FILE__);
	if (!PyArg_ParseTuple(args, "ss#is", &essid, &bssid, &len, &rate, &ip))
		return NULL;
	else {
		DPRINT(DINFO, "Creating AP with %s", essid);
		pthread_mutex_lock( &mx_pending_op );
		result = create_vap(essid, bssid, bssid, rate, ip);
		pthread_mutex_unlock( &mx_pending_op );

		return Py_BuildValue("i", result);
	}
}

static PyObject* pacmap_destroyvap(PyObject *self, PyObject *args)
{
	char* essid;
	char* bssid;
	int len = 6;
	int result = 1;

	/ *  get essid and bssid from arguments * /
	DPRINT(DDEBUG, "%s: pacmap_destroyvap", __FILE__);
	if (!PyArg_ParseTuple(args, "ss#", &essid, &bssid, &len))
		return NULL;
	else {

		DPRINT(DINFO, "Destroy AP with %s", essid);
		pthread_mutex_lock( &mx_pending_op );
		result = destroy_vap(essid);
		pthread_mutex_unlock( &mx_pending_op );

		return Py_BuildValue("i", result);
	}
}


/ * Return the bssid of the wireless card * /
static PyObject* pacmap_getbssid(PyObject *self, PyObject *args)
{
	DPRINT(DDEBUG, "%s: pacmap_getbssid", __FILE__);
	return Py_BuildValue("s#", opt.r_bssid, 6);
}

/ * Return the source mac of the wireless card * /
static PyObject* pacmap_getsmac(PyObject *self, PyObject *args)
{
	DPRINT(DDEBUG, "%s: pacmap_getsmac", __FILE__);
	return Py_BuildValue("s#", opt.r_smac, 6);
}

/ * Creates the tap interface and return result * /
static PyObject* pacmap_createtap(PyObject *self, PyObject *args)
{
	int result = -1;

	DPRINT(DDEBUG, "%s: pacmap_createtap", __FILE__);
	result = addvirtualinterface();

	return Py_BuildValue("i", result);

}


/ * Destroys the tap interface and return result * /
static PyObject* pacmap_destroytap(PyObject *self, PyObject *args)
{
	int index = 256;
	int result = 1;
	
	/ *  get essid and bssid from arguments * /
	DPRINT(DDEBUG, "%s: pacmap_destroytap", __FILE__);
	if (!PyArg_ParseTuple(args, "i", &index))
		return NULL;
	else {

		result = removevirtualinterface(index);
		return Py_BuildValue("i", result);
	}
}

/ * Set MAC address for TAP interface * /
static PyObject* pacmap_settapmac(PyObject *self, PyObject *args)
{
	char * bssid;
	int index = -1;
	int len = 6;
	int result = 0;

	/ *  get essid and bssid from arguments * /
	DPRINT(DDEBUG, "%s: pacmap_settapmac", __FILE__);
	if (!PyArg_ParseTuple(args, "is#", &index, &bssid, &len))
		return NULL;
	else {

		if (ti_set_mac(virtualintf[index], bssid) != 0)
		{
			DPRINT(DINFO, "");
			perror("ti_set_mac failed");
			DPRINT(DINFO, "You most probably want to set the MAC of your TAP interface.");
			DPRINT(DINFO, "ifconfig <iface> hw ether %02X:%02X:%02X:%02X:%02X:%02X\n\n\n",
				 bssid[0],bssid[1], bssid[2],
				 bssid[3],bssid[4], bssid[5]);
			result=-1;
		}
		else
		{
			/ *  save MAC address * /
			memcpy(virtualintfmac[index], bssid, 6);
		}

		return Py_BuildValue("i",result);
	}
}

/ * Set IP address for TAP interface * /
static PyObject* pacmap_settapip(PyObject *self, PyObject *args)
{
	char* ip;
	struct in_addr ip_addr;
	int index = -1;
	int result = 0;

	/ *  get essid and bssid from arguments * /
	DPRINT(DDEBUG, "%s: pacmap_settapip", __FILE__);
	if (!PyArg_ParseTuple(args, "is",&index,&ip))
		return NULL;
	else {
		/ * struct tip_linux *priv = ti_priv(virtualintf[index])); * /
		memset(&ip_addr,0,sizeof(struct in_addr));
		if (!inet_aton(ip, &ip_addr.s_addr))
			perror("inet_aton");
		result = ti_set_ip(virtualintf[index], (struct in_addr *) &ip_addr);

		ti_set_up(virtualintf[index]);

		return Py_BuildValue("i", result);
	}
}

/ * Setup virtual AP mode * /
static PyObject* pacmap_setupvirtualap(PyObject *self, PyObject *args)
{
	char* ip;
	char* bssid;
	int result = 0;

	/ *  get essid and bssid from arguments * /
	DPRINT(DDEBUG, "%s: pacmap_setupvirtualap", __FILE__);
	if (!PyArg_ParseTuple(args, "ss", &bssid, &ip))
		return NULL;
	else {
		result = virtualap_setup(ip, bssid);

		return Py_BuildValue("i", result);
	}
}

/ *  Python-wrapped functions * /
static PyMethodDef PacMapMethods[] = {
	{"setupvirtualap", pacmap_setupvirtualap, METH_VARARGS, "Set virtual ap management mode"},
	{"createtap", pacmap_createtap, METH_VARARGS, "Create TAP interface"},
	{"destroytap", pacmap_destroytap, METH_VARARGS, "Destroy TAP interface"},
	{"settapmac", pacmap_settapmac, METH_VARARGS, "set MAC address for TAP interface"},
	{"settapip", pacmap_settapip, METH_VARARGS, "set IP address for TAP interface"},
	{"getrxinfo", pacmap_getrxinfo, METH_VARARGS, "Get a specific field of RX radiotap header"},
	{"sendtipacketbybssid", pacmap_sendtipacketbybssid, METH_VARARGS, "Send arbitrary packet on tap interfacebyBSSID"},
	{"sendtipacket", pacmap_sendtipacket, METH_VARARGS, "Send arbitrary packet on tap interface"},
	{"sendpacket", pacmap_sendpacket, METH_VARARGS, "Send arbitrary packet"},
	{"createvap", pacmap_createvap, METH_VARARGS, "Create Virtual Access point with essid and bssid"},
	{"destroyvap", pacmap_destroyvap, METH_VARARGS, "Destroy Virtual Access point with essid and bssid"},
	{"getbssid", pacmap_getbssid, METH_VARARGS, "Get the bssid MAC from the adapter"},
	{"getsmac", pacmap_getsmac, METH_VARARGS, "Get the source MAC from the adapter"},
	{NULL, NULL, 0, NULL}
};
*/

#ifndef __PYTHON_PACMAP__
#define __PYTHON_PACMAP__

#endif
