/*
 *  802.11 station
 *
 */

#include <pacmap/wifi/protocol_80211.h>
#include <pacmap/wifi/station.h>
#include "sta_80211.h"

void sta_80211_start_scan(struct wifi_fn_t *wfn, void *arg)
{
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	int channel;

	DPRINT(DMAX, "sta_80211_start_scan %s ap_rssi: %d", sta->state == STA_IDLE ? "STA_IDLE" : "STA_SCANNING", sta->ap->rssi);
	if (sta->ap->is_valid && sta_ap_timeout(sta, sta->ap))
	{
		/* AP timeout */
		sta->ap->rssi = 0;
		sta->state = STA_IDLE;
		sta->ap->is_valid = 0;
		
	}
	if ((sta->ap->rssi < AP_THRESHOLD || !sta->ap->is_valid) && sta->state == STA_IDLE)
	{
		/* Starts active scan */
		sta_ap_list_timeout(sta);
		sta->state = STA_SCANNING;
		channel = 1;
		wfn->time_scanning = TIME_SCANNING;
	}
	else if (sta->state == STA_SCANNING)
	{
		int ch = wifi_get_channel();
		if (ch == 11)
		{
			DPRINT(DINFO, "Scanning finished");
			sta_print_ap_list(sta);
			sta_read_options(sta);
			sta_80211_send_auth_request(wfn);
		}
		else 
		{
			channel = ch + 1;
		}
	}
	
	if (sta->state == STA_SCANNING)
	{
		wifi_set_channel(channel);
		sta->got_probe_requests = 0;
		DPRINT(DMAX, "Scanning channel %d", channel);
		sta_80211_send_probe_request(wfn);
	} 
}

void sta_80211_probe_request(struct wifi_fn_t *wfn, void *arg)
{
	DPRINT(DMAX, "sta_80211_probe_request");
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	if (sta->got_probe_requests)
	{
		wfn->wifi_timer_fn = sta_80211_start_scan;
		proto80211_add_timer(MAX_CHANNEL_TIME, NULL, 0);
	}
	else
	{
		sta_80211_start_scan(wfn, arg);
	}
}

void sta_80211_send_probe_request(struct wifi_fn_t *wfn)
{
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	u_char h80211[4096];
	int packet_length = 0;
	memset(h80211, 0, 4096);
	
	packet_length = ieee80211_framectrl_forge(h80211, packet_length, BROADCAST, sta->mac_address, BROADCAST, 0, FC_MGMT_PROBE_REQUEST, 314);
	packet_length = ieee80211_mgmt_forge(h80211, packet_length, ST_PROBE_REQUEST, NO_CHANNEL, (unsigned char *) NO_ESSID, NO_CAPABILITY, sta->rates_supp, sta->rates_supp_ext);

	/*  send packet at 1 Mbit/s */
	proto80211_packet_send(h80211, packet_length, 0x02);
	wfn->wifi_timer_fn = sta_80211_probe_request;
	proto80211_add_timer(MIN_CHANNEL_TIME, NULL, 0);
}

/* Sends 802.11 probe request */
void sta_80211_st_probe_request(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	if (is_same_mac(header->da, sta->mac_address))
	{
		DPRINT(DINFO, "Probe Request from "f_MACADDR" rssi %d", MACADDR(header->sa), rssi);
	}
}

void sta_80211_st_probe_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	/* Station makes a list of possible aps */
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pClient_t sta = (pClient_t) wfn->wfn_priv;

	/* active scanning only */
	if (sta->state == STA_SCANNING) 
	{
		sta->got_probe_requests++;
		/* Parsing frame body */
		struct mgmt_body_t mgmt_body_fields;
		ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
		DPRINT(DDEBUG, "Probe Response from "f_MACADDR" essid '%s'\t\t channel %02d rssi %d", MACADDR(header->sa), mgmt_body_fields.ssid.ssid, mgmt_body_fields.channel, rssi);
		pAP_info new_ap = sta_new_ap_info(header->bssid, header->sa, mgmt_body_fields.ssid.ssid, mgmt_body_fields.channel, rssi, mgmt_body_fields.timestamp, mgmt_body_fields.capability_info);
		sta_save_ap_info(sta, new_ap);
	}
}

void sta_80211_send_auth_request(struct wifi_fn_t *wfn)
{
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	pAP_info ap = sta->ap;

	if (!sta_get_ap_info(sta, sta->ap))
	{
		sta->state = STA_IDLE;
		return;
	}
	wifi_set_channel(ap->channel);
	sta->state = STA_AUTH_REQUEST;
	
	DPRINT(DINFO, "Auth Request to '%s' ("f_MACADDR") essid %s channel %d", sta->ap->essid, MACADDR(ap->mac_address), ap->essid, ap->channel);

	u_char h80211[4096];
	int packet_length = 0;
	memset(h80211, 0, 4096);
	
	packet_length = ieee80211_framectrl_forge(h80211, packet_length, ap->mac_address, sta->mac_address, ap->bssid, 0, FC_MGMT_AUTH, 314);
	packet_length= ieee80211_auth_forge(h80211, packet_length, AUTH_ALGO_OPEN_SYSTEM, IEEE80211_AUTH_OPEN_REQUEST, IEEE80211_STATUS_SUCCESS);

	/*  send packet at 1 Mbit/s */
	proto80211_packet_send(h80211, packet_length, 0x02);

	wfn->wifi_timer_fn = sta_80211_auth_assoc_timeout;
	proto80211_add_timer(AUTH_ASSOC_TIMEOUT, NULL, 0);
}

void sta_80211_auth_assoc_timeout(struct wifi_fn_t *wfn, void *arg)
{
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	sta->state = STA_IDLE;
}

void sta_80211_st_auth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	length = length;
	/* station wants to auth to an ap */
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	
	if (is_same_mac(header->da, sta->mac_address) && is_same_mac(sta->ap->mac_address, header->sa))
	{
		if (ieee80211_status_code_auth(packet) == IEEE80211_STATUS_SUCCESS)
		{
			DPRINT(DINFO, "Auth Response SUCCESS from '%s' ("f_MACADDR") rssi %d", sta->ap->essid, MACADDR(header->sa), rssi);
			sta->ap->is_valid = STA_AUTH_REQUEST;
			sta_80211_send_assoc_request(wfn);
		}
		else
		{
			DPRINT(DINFO, "Auth Response FAILED from '%s' ("f_MACADDR") rssi %d", sta->ap->essid, MACADDR(header->sa), rssi);
		}
	}
}

void sta_80211_send_assoc_request(struct wifi_fn_t *wfn)
{
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	pAP_info ap = sta->ap;

	DPRINT(DINFO, "Assoc Request to '%s' ("f_MACADDR") essid %s channel %d", sta->ap->essid, MACADDR(ap->mac_address), ap->essid, ap->channel);
	sta->state = STA_ASSOC_REQUEST;
	
	u_char h80211[4096];
	int packet_length = 0;
	memset(h80211, 0, 4096);

	packet_length = ieee80211_framectrl_forge(h80211, packet_length, ap->mac_address, sta->mac_address, ap->bssid, 0, FC_MGMT_ASSOC_REQUEST, 314);
	packet_length = ieee80211_mgmt_forge(h80211, packet_length, FC_MGMT_ASSOC_REQUEST, 0, sta->ap->essid, sta->capability, sta->rates_supp, sta->rates_supp_ext);

	/*  send packet at 1 Mbit/s */
	proto80211_packet_send(h80211, packet_length, 0x02);

	wfn->wifi_timer_fn = sta_80211_auth_assoc_timeout;
	proto80211_add_timer(AUTH_ASSOC_TIMEOUT, NULL, 0);
}

void sta_80211_st_assoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	/* ap answers the assoc request */
	length = length;
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	
	if (is_same_mac(header->da, sta->mac_address) && is_same_mac(sta->ap->mac_address, header->sa))
	{
		int code = ieee80211_status_code_assoc(packet);
		if (code == IEEE80211_STATUS_SUCCESS)
		{
			DPRINT(DINFO, "Assoc Response SUCCESS from '%s' ("f_MACADDR") rssi %d", sta->ap->essid, MACADDR(header->sa), rssi);
			sta->ap->is_valid = STA_ASSOC_REQUEST;
			sta_save_ap_rssi(sta, rssi);
		}
		else
		{
			DPRINT(DINFO, "Assoc Response FAILED (%d) from "f_MACADDR" rssi %d", code, MACADDR(header->sa), rssi);
		}
	}
	sta->state = STA_IDLE;
}


void sta_80211_st_beacon(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	/* ap answers the assoc request */
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pClient_t sta = (pClient_t) wfn->wfn_priv;

	struct mgmt_body_t mgmt_body_fields;
	ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
	
	if (is_same_ssid(mgmt_body_fields.ssid.ssid, sta->ap->essid) && is_same_mac(sta->ap->mac_address, header->sa) && sta->ap->is_valid == STA_ASSOC_REQUEST)
	{
		if (header->seq_ctrl % 50 == 0) DPRINT(DINFO, "Beacon from '%s' ("f_MACADDR") rssi %d", sta->ap->essid, MACADDR(header->sa), rssi);
		sta_save_ap_rssi(sta, rssi);
		
		if (mgmt_body_fields.channel_switch.element_id == ELEMENT_ID_CHANSWITCHANN)
		{
			/* Switch channel */
			sta->channel = mgmt_body_fields.channel_switch.new_channel;
			wifi_set_channel(sta->channel);
			DPRINT(DINFO, "Changing to channel %d", sta->channel);
			// mgmt_body_fields->channel_switch.count;			
		}
	}
}

void sta_80211_st_deauth(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	struct mgmt_body_t mgmt_body_fields;
	ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
	DPRINT(DINFO, "DEAUTH from '%s' ("f_MACADDR") -> ("f_MACADDR") rssi %d", mgmt_body_fields.ssid.ssid, MACADDR(header->sa), MACADDR(header->da), rssi);
}

void sta_80211_st_disassoc(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	/* station wants to disassoc from an ap */
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	struct mgmt_body_t mgmt_body_fields;
	ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
	DPRINT(DINFO, "DISASSOC from '%s' ("f_MACADDR") -> ("f_MACADDR") rssi %d", mgmt_body_fields.ssid.ssid, MACADDR(header->sa), MACADDR(header->da), rssi);
}

void sta_80211_st_reassoc_response(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	struct mgmt_body_t mgmt_body_fields;
	ieee80211_parse_mgmt_frame_body(packet, length, &mgmt_body_fields);
	DPRINT(DINFO, "REASSOC RESPONSE from '%s' ("f_MACADDR") -> ("f_MACADDR") rssi %d", mgmt_body_fields.ssid.ssid, MACADDR(header->sa), MACADDR(header->da), rssi);
}

void sta_80211_data_data(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pClient_t sta = (pClient_t) wfn->wfn_priv;

	/* Check if packet is for us */
	if (is_same_mac(header->da, sta->mac_address))
	{
		/* set ToDS=0, FromDS=1 for injecting purposes */
		u_int16_t fc = header->fc;
		fc = (fc & ~FC_TO_DS_BIT) | FC_FROM_DS_BIT;
		header->fc = fc;

		sta_save_ap_rssi(sta, rssi);

        unsigned char h80211[4096];
		int trailer = 0;
		int offset = 0;
		u_int16_t ether_type;
		
		/* Destination mac address */
		memcpy(h80211, header->bssid, sizeof(header->bssid));
		offset += sizeof(header->bssid);
		/* Source mac address */
		memcpy(h80211 + offset, header->sa, sizeof(header->sa));
		offset += sizeof(header->sa);
		/* Ether type */
		memcpy(h80211 + offset, packet + sizeof(struct mgmt_header_t) + 6, sizeof(u_int16_t));
		offset += sizeof(u_int16_t);

		memcpy(&ether_type, packet + sizeof(struct mgmt_header_t) + 6, 2);
		ether_type = ntohs(ether_type);
		
		DPRINT(DINFO, "DATA from: "f_MACADDR" type: %s", MACADDR(header->sa), packet_type(ether_type));

		if (length <= sizeof(struct mgmt_header_t) + 8)
			return;

		/* Copy frame body */
		memcpy(h80211 + 14, packet + sizeof(struct mgmt_header_t) + 8, length - sizeof(struct mgmt_header_t) - 8);
		length = length - sizeof(struct mgmt_header_t) - 8 + 14;

		/* ethernet frame must be atleast 60 bytes without fcs */
		if (length < 60)
		{
			trailer = 60 - length;
			bzero(h80211 + length, trailer);
			length += trailer;
		}
		proto80211_packet_ti_send(h80211, length);
	}
}

void sta_80211_data_nodata(struct wifi_fn_t *wfn, u_char *packet, int length, int rssi)
{
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	pClient_t sta = (pClient_t) wfn->wfn_priv;

	/* Check if packet is for us */
	if (is_same_mac(header->da, sta->mac_address))
	{
		sta_save_ap_rssi(sta, rssi);
		DPRINT(DINFO, "DATA_NODATA from ("f_MACADDR") -> ("f_MACADDR") rssi %d", MACADDR(header->sa), MACADDR(header->da), rssi);
	}
}

void sta_80211_send_data_nodata(struct wifi_fn_t *wfn)
{
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	
	if (sta->ap->is_valid)
	{
		u_char h80211[4096];
		int packet_length = 0;
		memset(h80211, 0, 4096);

		u_int16_t fc = FC_DATA_NODATA | FC_TO_DS_BIT;
		packet_length = ieee80211_framectrl_forge(h80211, packet_length, sta->ap->mac_address, sta->mac_address, sta->ap->bssid, 0, fc, 48);
	
		/*  send packet at 1 Mbit/s */
		proto80211_packet_send(h80211, packet_length, RATE_54M/500000);
	}
}

void sta_80211_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length)
{
	/*  802.11 header for sending operations, reusable */
	unsigned char h80211[4096];
	int offset = 0;
	
	if (packet == NULL) return;
	if (length < 38) return;

	memcpy(h80211, IEEE80211_LLC_SNAP, 32);
	memcpy(h80211 + 32, packet + 14, length - 14);

	/* Ether type */
	memcpy(h80211 + 30, packet + 12, sizeof(u_int16_t));
	offset += sizeof(u_int16_t);

	u_int16_t ether_type;
	memcpy(&ether_type, packet + 12, 2);
	ether_type = ntohs(ether_type);

	h80211[1] |= 0x02; /*   data packet */

	pClient_t sta = (pClient_t) wfn->wfn_priv;
	pAP_info ap = (pAP_info) sta->ap;

	memcpy(h80211 + 10, ap->bssid, sizeof(ap->bssid)); /* BSSID */
	memcpy(h80211 + 16, packet + 6,	6); /* SRC_MAC */
	memcpy(h80211 + 4, packet, 6); /* DST_MAC */

	struct mgmt_header_t *header = (struct mgmt_header_t *) h80211;
	DPRINT(DINFO, "TAP to: "f_MACADDR" type %s", MACADDR(header->da), packet_type(ether_type));
	length += (32 - 14); /* 32=IEEE80211+LLC/SNAP; 14=SRC_MAC+DST_MAC+TYPE */

	/*  Passthrough mode */
	proto80211_packet_send(h80211, length, RATE_54M/500000);
}

void sta_80211_free(struct wifi_fn_t *wfn)
{
	pClient_t sta = (pClient_t) wfn->wfn_priv;
	sta_free(sta);
}

struct wifi_fn_t *sta_80211_init(struct ConfigValues *config)
{
	struct wifi_fn_t *wfn;
	pClient_t sta;
	
	DPRINT(DMAX, "%s: sta_80211_init", __FILE__);
	wfn = wfn_alloc(sizeof(struct ST_info));

	if (!wfn)
		return NULL;
		
	wfn->wifi_st_assoc_response	= sta_80211_st_assoc_response;
	wfn->wifi_st_disassoc		= sta_80211_st_disassoc;
	wfn->wifi_st_reassoc_response	= sta_80211_st_reassoc_response;
	wfn->wifi_st_probe_response	= sta_80211_st_probe_response;
	wfn->wifi_st_beacon	= sta_80211_st_beacon;
	wfn->wifi_st_auth	= sta_80211_st_auth;
	wfn->wifi_st_deauth	= sta_80211_st_deauth;
	wfn->wifi_data_data	= sta_80211_data_data;	
	wfn->wifi_data_nodata	= sta_80211_data_nodata;	
	wfn->wifi_ti_recv = sta_80211_ti_recv;

	wfn->wifi_ap_beacon	= sta_80211_send_data_nodata;
	wfn->time_beacon = BEACON_INTERVAL_TIMER*2.5;

	wfn->wifi_scanning = sta_80211_start_scan;
	wfn->time_scanning = BEACON_INTERVAL_TIMER;
	
	wfn->wifi_free = sta_80211_free;

	/* Access point information */
	sta = (pClient_t) wfn->wfn_priv;
	sta_init(sta, config->mac_address);
	
	sta->channel = config->channel;
	sta->state = STA_IDLE;
	
	DPRINT(DINFO, "Mode: Station IEEE 802.11");
	DPRINT(DINFO, "mac: "f_MACADDR"", MACADDR(sta->mac_address));
	
	return wfn;
}
	
