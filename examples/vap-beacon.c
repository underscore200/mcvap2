/*
 * Example for testing purposes
 * 
 * 15/01/2010    Maria Eugenia Berezin <berezin @ imag.fr>
 *
 */
 
#include <pacmap/pacmap.h>
#include "wifi/vap-beacon.h"

int main(int argc, char** argv)
{
        /*  Testing beacon des-synchronization */
        pacmap_init(argc, argv, vap_beacon_init);
        return 0;
}

