#ifndef _AIRCRACK_NG_H
#define _AIRCRACK_NG_H

struct WPA_hdsk
{
	unsigned char stmac[6];				 /* supplicant MAC               */
	unsigned char snonce[32];			 /* supplicant nonce             */
	unsigned char anonce[32];			 /* authenticator nonce          */
	unsigned char keymic[16];			 /* eapol frame MIC              */
	unsigned char eapol[256];			 /* eapol frame contents         */
	int eapol_size;				 /* eapol frame size             */
	int keyver;					 /* key version (TKIP / AES)     */
	int state;					 /* handshake completion         */
};

struct AP_info
{
	struct AP_info *next;		 /* next AP in linked list       */
	unsigned char bssid[6];				 /* access point MAC address     */
	char essid[33];				 /* access point identifier      */
	unsigned char lanip[4];				 /* IP address if unencrypted    */
	unsigned char *ivbuf;				 /* table holding WEP IV data    */
	unsigned char **uiv_root;			 /* IV uniqueness root struct    */
	long ivbuf_size;			 /* IV buffer allocated size     */
	long nb_ivs;				 /* total number of unique IVs   */
	long nb_ivs_clean;			 /* total number of unique IVs   */
	long nb_ivs_vague;				 /* total number of unique IVs   */
	int crypt;					 /* encryption algorithm         */
	int eapol;					 /* set if EAPOL is present      */
	int target;					 /* flag set if AP is a target   */
	struct ST_info *st_1st;		 /* linked list of stations      */
	struct WPA_hdsk wpa;		 /* valid WPA handshake data     */
    //    PTW_attackstate *ptw_clean;
    //    PTW_attackstate *ptw_vague;
};


#endif /* _AIRCRACK_NG_H */
