#!/usr/bin/python

"""
essid = "Test"
It creates a TAP interface 192.168.10.10

@maru
05/01/2010	Connected
"""

import psyco
import pacmap
try:
	from scapy.all import *
except:
	from scapy import *
import signal
import time, calendar,datetime 

bssid = ""
timestamp = 0
index=-1

def handler(signal, stackframe):
    print "[py] OUCH"

signal.signal(signal.SIGSEGV, handler)




class Client:
    
    def __init__(self, mac, bssid):
        self.bssid = bssid
        self.mac = mac
        self.timestamp = time.mktime(datetime.datetime.now().timetuple())
        self.sequence = 0
        self.state = "unknown"
        
    def getTimestamp(self):
        return self.timestamp
        
    def setTimestamp(self,timestamp):
        self.timestamp=timestamp
    
    def getSeq(self):
        return self.sequence
    
    def setSeq(self,seq):
        self.sequence=seq
        
    def getMAC(self):
        return self.mac
    
    def getBSSID(self):
        return self.bssid
        
    def setBSSID(self, bssid):
        self.bssid = bssid
    
    def getState(self):
        return self.state
    
    def setState(self,state):
        self.state=state
        
class ClientList:
    
    def __init__(self):
        self.data = []
        self.mac = []
        self.index = -1
        
    def isPresent(self, mac):
        i = -1
        try:
            while 1:
                i = self.mac.index(mac, i+1)
        except ValueError:
            pass
        return i
    
    def add(self,mac):
        i = self.isPresent(mac)
        if i < 0:
            print "[py] Adding new client %s" % mac
            self.mac.append(mac)
            self.data.append(Client(mac,bssid))
        else:
            print "[py] Client already present"
       
    def delete(self,mac):
        i = self.isPresent(mac)
        if i>=0:
            print "[py] Remove client %s" % mac
            del self.mac[i]
            del self.data[i]
            
            
    def __iter__(self): 
        self.index = len(self.data)
        return self

    def next(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]
        
    def getClient(self,mac):
        i = self.isPresent(mac)
        if i>=0:
            return self.data[i]
        else:
            return
    
    def getLength(self):
        return len(self.mac)

clients_list = ClientList()



# PACMAP : function called at launch time
def background():
    global bssid
    global index 
    bssid = pacmap.getbssid()
    print "[py] BSSID from MAC Adapter is %02X:%02X:%02X:%02X:%02X:%02X " % (ord(bssid[0]),ord(bssid[1]),ord(bssid[2]),ord(bssid[3]),ord(bssid[4]),ord(bssid[5])) 
    essid = "Test"
    #print "[py] Create Virtual access point %s" % essid
    #pacmap.createvap(essid,bssid)
    print "[py] Virtual AP 0.1 native version by Yan Grunenberger"
    print "[py] Written using PacMap"
    index=pacmap.createtap()
    pacmap.settapmac(index,bssid)
    pacmap.settapip(index,"192.168.10.10")
    return 0

# PACMAP : Example of periodic function call : each second, we display a message
#timestamp = 0
# PACMAP : Resolution of timer is 1/512s
def periodic(float1,float2,float3):
    global timestamp
    i = clients_list.getLength()
    if i == 0:
        i = 1
    beacon_interval = 0x0064 * i
    periodicity = 11.25 # period in 1/x seconds
    result = int(float1/512 * periodicity)
    if result > timestamp:
        timestamp = result
        
        for client in clients_list:
            current_timestamp = time.mktime(datetime.datetime.now().timetuple())*1e6+datetime.datetime.now().microsecond
            seq = client.getSeq()
            ssid = "Test"
            
            channel = "\x06"
            if seq > 500 and seq < 700:
                print "[py] Engaging channel switching..."
                channel = "\x01"

            dot11_answer = Dot11(
            FCfield ="retry",
            SC= (seq<<4 & 0xff) |( (seq>>4 & 0xff) << 8 & 0xff00) ,
        	type = "Management",
        	addr1 = "ff:ff:ff:ff:ff:ff",
        	addr2 = bssid,
        	addr3 = bssid)/Dot11Beacon(timestamp = current_timestamp,cap = 0x0104,beacon_interval = beacon_interval )/Dot11Elt(ID=0,info=ssid)/Dot11Elt(ID=1,info="\x82\x84\x8b\x96")/Dot11Elt(ID=3,info=channel)/Dot11Elt(ID=50,info="\x0c\x12\x18\x24\x30\x48\x60\x6c")
            #print "[py] Sending beacon to client %s" % client.getMAC()
            
            pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),1)
            if seq == 4095:
                client.setSeq(0)
            else:
                client.setSeq(seq+1)
            
            diff = timestamp - client.getTimestamp()
            
            if diff>10:
               clients_list.delete(client.getMAC())
  
    return 0

# PACMAP : Function called at packet reception
def packet_recv(packet, length):
    #dot11_frame = Packet(packet)
    #dot11_frame.decode_payload_as(Dot11)
    #print "[py] Scapy interpretation %s" % dot11_frame.summary()
    #print "[py] packet %02X " % ord(packet[length-4])
    #newpacket = "yangrunenberger"
    #pacmap.sendpacket(newpacket,len(newpacket),48)
    #pacmap.sendtipacket(newpacket,len(newpacket),bssid)
    # PACMAP : return value of 0 for passthrough mode
    return 0

# PACMAP : Function called at packet reception on tap interface
def packet_ti_recv(packet, length,index):
    ether_frame = Packet(packet)
    ether_frame.decode_payload_as(Ether)

    dot11_answer=Dot11(type = "Data",
    addr1 = ether_frame.getlayer(Ether).dst,
    addr2 = ether_frame.getlayer(Ether).src,
    addr3 = bssid,
    FCfield=0x02)/LLC()/SNAP(code=ether_frame.getlayer(Ether).type)/ether_frame.getlayer(Ether).payload
    
    pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),0x6c)
    # PACMAP : return value of 0 for passthrough mode
    return 1

# PACMAP : Function called if 802.11 probe request detected
def proto80211_probereq(packet, length):
    #print "[py] Probe request received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    client = dot11_frame.getlayer(Dot11).addr2
    # generate a custom SSID function of mac add of the client
    ssid = "Test" 
    #print "[py] Scapy interpretation %s" % dot11_frame.summary()
    current_timestamp = time.mktime(datetime.datetime.now().timetuple())*1e6+datetime.datetime.now().microsecond
    dot11_answer = Dot11(
	type = "Management",
	addr1 = dot11_frame.getlayer(Dot11).addr2,
	addr2 = bssid,
	addr3 = bssid)/Dot11ProbeResp(timestamp=current_timestamp,cap = 0x0104)/Dot11Elt(ID=0,info=ssid)/Dot11Elt(ID=1,info="\x82\x84\x8b\x96")/Dot11Elt(ID=3,info="\x06")/Dot11Elt(ID=50,info="\x0c\x12\x18\x24\x30\x48\x60\x6c")
    
    pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),1)
    
    # PACMAP : return value of 0 for passthrough mode
    return 1

	# PACMAP : Function called if 802.11 probe request detected
def proto80211_auth(packet, length):
    print "[py] Authentification received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    dot11_answer = dot11_frame
    addr1=  dot11_frame.getlayer(Dot11).addr2
    dot11_answer.getlayer(Dot11).addr1= addr1
    dot11_answer.getlayer(Dot11).addr2= bssid
    dot11_answer.getlayer(Dot11Auth).seqnum=0x02
    pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),1)
    

    return 1

def proto80211_deauth(packet, length):
    print "[py] Deauthentification received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    #print "[py] Scapy interpretation %s" % dot11_frame.summary()
    client = dot11_frame.getlayer(Dot11).addr2
    clients_list.delete(client)
    return 1


def proto80211_assocreq(packet, length):
    print "[py] Association request received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    dot11_answer = dot11_frame
    client = dot11_frame.getlayer(Dot11).addr2
    ssid = "Client-%s" % client
    dot11_answer = Dot11(type = "Management",addr1 = dot11_frame.getlayer(Dot11).addr2,addr2 = bssid, addr3 = bssid)/Dot11AssoResp(cap = 0x0104, AID=0xc001)/Dot11Elt(ID=0,info=ssid)/Dot11Elt(ID=1,info="\x02\x04\x0b\x16\x0c\x12\x18\x24")/Dot11Elt(ID=50,info="\x30\x48\x60\x6c")
    pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),1)

    clients_list.add(client)


            
    return 1
    
def proto80211_beacon(packet, length, essid):
    return 0


def proto80211_ctrl(packet, length):
    return 0

def proto80211_data(packet, length):
    global index
    #print "[py] Data packet received"
    dot11_frame = Packet(packet)
    dot11_frame.decode_payload_as(Dot11)
    
    mac = dot11_frame.getlayer(Dot11).addr2
    client = clients_list.getClient(mac)
    if client:
        
        timestamp = time.mktime(datetime.datetime.now().timetuple())+datetime.datetime.now().microsecond/1e6
        client.setTimestamp(timestamp)
        
        #print "[py] Data packet with power %d, channel %d, rate %d" % (pacmap.getrxinfo(2)-pacmap.getrxinfo(1),pacmap.getrxinfo(3),pacmap.getrxinfo(5))
        
        
        # Trafic interpretation
#        if dot11_frame[ARP] and dot11_frame[ARP].op == 1:
#            print "[py] ARP who-has -> reply"
#            dot11_answer=Dot11(type = "Data",addr1 = mac,addr2 = bssid,addr3 = bssid,FCfield=0x02)/LLC()/SNAP()/ARP(op=2,hwsrc=bssid,psrc="192.168.10.10",hwdst=dot11_frame[ARP].hwsrc,pdst=dot11_frame[ARP].psrc)
#            pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),1)
        
        # DHCP Management 
        if dot11_frame[DHCP] and dot11_frame[DHCP].options[0][1] == 1:
            print "[py] DHCPDISCOVER"
            #
            dot11_answer=Dot11(type = "Data",addr1 = mac,addr2 = bssid,addr3 = bssid,FCfield=0x02)/LLC()/SNAP()/IP(src="192.168.10.10",dst="192.168.10.2")/UDP(sport=67,dport=68)/BOOTP(op=2, ciaddr="0.0.0.0",yiaddr="192.168.10.2",siaddr="0.0.0.0",giaddr="192.168.10.10",chaddr=mac2str(mac), xid=dot11_frame[BOOTP].xid)/DHCP(options=[('message-type','offer'),('server_id',"192.168.10.10"), ('lease_time',43200), ('renewal_time',21600),('rebinding_time',37800),('subnet_mask',"255.255.255.0"),('router',"192.168.10.10"),('name_server',"192.168.10.10"),('end')])
            
            pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),1)
        
        if dot11_frame[DHCP] and dot11_frame[DHCP].options[0][1] == 3:
            
            print "[py] DHCPREQUEST"
            # answer with dhcp ACK
            dot11_answer=Dot11(type = "Data",addr1 = mac,addr2 = bssid,addr3 = bssid,FCfield=0x02)/LLC()/SNAP()/IP(src="192.168.10.10",dst="192.168.10.2")/UDP(sport=67,dport=68)/BOOTP(op=2, ciaddr="0.0.0.0",yiaddr="192.168.10.2",siaddr="0.0.0.0",giaddr="192.168.10.10",chaddr=mac2str(mac), xid=dot11_frame[BOOTP].xid)/DHCP(options=[('message-type','ack'),('server_id',"192.168.10.10"), ('lease_time',43200), ('renewal_time',21600),('rebinding_time',37800),('subnet_mask',"255.255.255.0"),('router',"192.168.10.10"),('name_server',"192.168.10.10"),('end')])
            
            pacmap.sendpacket(str(dot11_answer),len(str(dot11_answer)),1)
        
        if dot11_frame.haslayer(SNAP):
            eth_sent_frame = Ether(dst=dot11_frame.getlayer(Dot11).addr3,src=dot11_frame.getlayer(Dot11).addr2,type=dot11_frame.getlayer(SNAP).code)
            eth_sent_frame.payload = dot11_frame.getlayer(SNAP).payload
            pacmap.sendtipacket(str(eth_sent_frame),len(str(eth_sent_frame)),index)

    else:
        # print "[py] Data from unknown station"
        pass
        
    return 1

