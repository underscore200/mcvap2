/*
 * Example for testing purposes
 * 
 * 15/01/2010    Maria Eugenia Berezin <berezin @ imag.fr>
 *
 */

#include <pacmap/pacmap.h>
#include "wifi/mcvap-monitor.h"

int main(int argc, char** argv)
{
        pacmap_init(argc, argv, mcvap_monitor_init);
        return 0;
}

