/*
 *  802.11 protocol implementation
 *
 *  Copyright (C) 2010 Maria Eugenia Berezin
 *  Copyright (C) 2008, 2009 Yan Grunenberger
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  In addition, as a special exception, the copyright holders give
 *  permission to link the code of portions of this program with the
 *  OpenSSL library under certain conditions as described in each
 *  individual source file, and distribute linked combinations
 *  including the two.
 *  You must obey the GNU General Public License in all respects
 *  for all of the code used other than OpenSSL. *  If you modify
 *  file(s) with this exception, you may extend this exception to your
 *  version of the file(s), but you are not obligated to do so. *  If you
 *  do not wish to do so, delete this exception statement from your
 *  version. *  If you delete this exception statement from all source
 *  files in the program, then also delete it here.
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "pacmap_core.h"
#include "protocol_80211.h"

struct wifi_fn_t *wfn_alloc(int sz)
{
	struct wifi_fn_t *wfn;
	void *priv;

	DPRINT(DMAX2, "%s: wfn_alloc", __FILE__);
	/* Allocate wif & private state */
	wfn = malloc(sizeof(struct wifi_fn_t));
	if (!wfn)
		return NULL;
	memset(wfn, 0, sizeof(struct wifi_fn_t));

	if (sz) 
	{
		priv = malloc(sz);
		if (!priv) {
			free(wfn);
			return NULL;
		}
		memset(priv, 0, sz);
		wfn->wfn_priv = priv;
	}

	wfn->wifi_packet_recv = proto80211_packet_recv;
	wfn->wifi_packet_ti_recv = proto80211_packet_ti_recv;
	wfn->wifi_ap_msgs_msg_recv = proto80211_ap_msgs_msg_recv;
	wfn->free = wfn_free;
	
	return wfn;
}
void wfn_free(struct wifi_fn_t *wfn)
{
	if (!wfn)
		return;

	if (wfn->wifi_free) 
		wfn->wifi_free(wfn);

	if (wfn->wfn_priv)
		free(wfn->wfn_priv);

	free(wfn);	
}

void proto80211_add_timer(int timer_usec, void *arg, int sz_arg)
{
	pacmap_add_timer(timer_usec, arg, sz_arg);
}

int proto80211_packet_ti_send(void* buf, size_t count)
{
	return packet_ti_send(buf, count);	
}

int proto80211_packet_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length, struct wif *wi)
{
	if (wfn->wifi_ti_recv)
		wfn->wifi_ti_recv(wfn, packet, length);
	return 0;
}

int proto80211_packet_send(void* buf, size_t count, int rate)
{
	return packet_send(buf, count, rate);
}

int proto80211_packet_recv(struct wifi_fn_t *wfn, u_char *packet, int length, struct wif *wi, int rssi)
{

	//if(rssi < 0) rssi += 255;
	/*  802.11 header */
	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;

	switch(FC_TYPE(header->fc)) {
		case T_MGMT: 
			switch (FC_SUBTYPE(header->fc)) {						
				case ST_ASSOC_REQUEST:
					if (wfn->wifi_st_assoc_request)
						wfn->wifi_st_assoc_request(wfn, packet, length, rssi);
					break;

				case ST_ASSOC_RESPONSE:
					if (wfn->wifi_st_assoc_response)
						wfn->wifi_st_assoc_response(wfn, packet, length, rssi);
					break;

				case ST_REASSOC_REQUEST:
					if (wfn->wifi_st_reassoc_request)
						wfn->wifi_st_reassoc_request(wfn, packet, length, rssi);
					break;

				case ST_REASSOC_RESPONSE:
					if (wfn->wifi_st_reassoc_response)
						wfn->wifi_st_reassoc_response(wfn, packet, length, rssi);
					break;

				case ST_PROBE_REQUEST:
					if (wfn->wifi_st_probe_request)
						wfn->wifi_st_probe_request(wfn, packet, length, rssi);
					break;

				case ST_PROBE_RESPONSE:
					if (wfn->wifi_st_probe_response)
						wfn->wifi_st_probe_response(wfn, packet, length, rssi);
					break;

				case ST_BEACON:
					if (wfn->wifi_st_beacon)
						wfn->wifi_st_beacon(wfn, packet, length, rssi);
					break;

				case ST_AUTH:
					if (wfn->wifi_st_auth)
						wfn->wifi_st_auth(wfn, packet, length, rssi);
					break;

				case ST_DEAUTH:
					if (wfn->wifi_st_deauth)
						wfn->wifi_st_deauth(wfn, packet, length, rssi);
					break;

				case ST_DISASSOC:
					if (wfn->wifi_st_disassoc)
						wfn->wifi_st_disassoc(wfn, packet, length, rssi);
					break;

				default:
					DPRINT(DMAX2, "ST_RESERVED_%.2X", FC_SUBTYPE(header->fc));
					break;
			}
			break;

		case T_CTRL: 
			switch (FC_SUBTYPE(header->fc)) {			
				case CTRL_PS_POLL:
					DPRINT(DMAX2, "CTRL_PS_POLL");
					break;

				case CTRL_RTS:
					DPRINT(DMAX2, "CTRL_RTS");
					if (wfn->wifi_ctrl_rts)
						wfn->wifi_ctrl_rts(wfn, packet, length, rssi);
					break;

				case CTRL_CTS:
					DPRINT(DMAX2, "CTRL_CTS");
					if (wfn->wifi_ctrl_cts)
						wfn->wifi_ctrl_cts(wfn, packet, length, rssi);
					break;

				case CTRL_ACK:
					DPRINT(DMAX2, "CTRL_ACK");
					if (wfn->wifi_ctrl_ack)
						wfn->wifi_ctrl_ack(wfn, packet, length, rssi);
					break;

				case CTRL_CF_END:
					DPRINT(DMAX2, "CTRL_CF_END");
					break;

				case CTRL_END_ACK:
					DPRINT(DMAX2, "CTRL_END_ACK");
					break;

				default:
					DPRINT(DMAX2, "CTRL_RESERVED_%.2X", FC_SUBTYPE(header->fc));
					break;
			}
			break;
		
		case T_DATA: 
			switch (FC_SUBTYPE(header->fc)) {			
				case DATA_DATA:
					if (wfn->wifi_data_data)
						wfn->wifi_data_data(wfn, packet, length, rssi);
					break;

				case DATA_DATA_CF_ACK:
					DPRINT(DMAX2, "DATA_DATA_CF_ACK");
					break;

				case DATA_DATA_CF_POLL:
					DPRINT(DMAX2, "DATA_DATA_CF_POLL");
					break;

				case DATA_DATA_CF_ACK_POLL:
					DPRINT(DMAX2, "DATA_DATA_CF_ACK_POLL");
					break;

				case DATA_NODATA:
					if (wfn->wifi_data_nodata)
						wfn->wifi_data_nodata(wfn, packet, length, rssi);
					break;

				case DATA_NODATA_CF_ACK:
					DPRINT(DMAX2, "DATA_NODATA_CF_ACK");
					break;

				case DATA_NODATA_CF_POLL:
					DPRINT(DMAX2, "DATA_NODATA_CF_POLL");
					break;

				case DATA_NODATA_CF_ACK_POLL:
					DPRINT(DMAX2, "DATA_NODATA_CF_ACK_POLL");
					break;

				case DATA_QOS_DATA:
					DPRINT(DMAX2, "DATA_QOS_DATA");
					break;

				default:
					DPRINT(DMAX2, "DATA_RESERVED_%.2X", FC_SUBTYPE(header->fc));
					break;
			}
			break;
	}
	return 0;
}

int proto80211_ap_msgs_msg_send(void* buf, size_t count, IPADDR_TYPE(ip_address))
{
	return ap_msgs_msg_send(buf, count, ip_address);
}

int proto80211_ap_msgs_msg_recv(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length)
{
	u_int8_t code = packet[0];
	switch(code) {
		case AP_MSGS_SCAN_REQUEST: 
			DPRINT(DDEBUG, "AP_MSGS_SCAN_REQUEST");
			if (wfn->wifi_ap_msgs_scan_request)
				wfn->wifi_ap_msgs_scan_request(wfn, ip_address, packet, length);
			break;
		case AP_MSGS_SCAN_RESPONSE: 
			DPRINT(DDEBUG, "AP_MSGS_SCAN_RESPONSE");
			if (wfn->wifi_ap_msgs_scan_response)
				wfn->wifi_ap_msgs_scan_response(wfn, ip_address, packet, length);
			break;
		case AP_MSGS_CLIENT_MOVE: 
			DPRINT(DDEBUG, "AP_MSGS_CLIENT_MOVE");
			if (wfn->wifi_ap_msgs_client_move)
				wfn->wifi_ap_msgs_client_move(wfn, ip_address, packet, length);
			break;
		default:
			DPRINT(DDEBUG, "AP_MSGS_????");
			break;
	}
	return 0;
}

/*** Frame functions ***/
int ieee80211_framectrl_forge(u_char *packet, int length, u_char *addr1, u_char *addr2, u_char *addr3, u_int16_t sequence_ctrl, u_int16_t fc, u_int16_t duration)
{

	/*sequence_ctrl = sequence number (12 bit) + fragment number (4 bit)
	 * we need to set fragment number to zero (no idea why it wasnt zero in the first place)
	 * */
	sequence_ctrl = (sequence_ctrl >> 4) << 4;

	struct mgmt_header_t *header = (struct mgmt_header_t *) packet;
	header->fc = fc;
	header->duration = duration;
	header->seq_ctrl = sequence_ctrl; //not madwifi-ng
	memcpy(header->da, addr1, 6);
	memcpy(header->sa, addr2, 6);
	memcpy(header->bssid, addr3, 6);
	return length + sizeof(struct mgmt_header_t);
}


int ieee80211_mgmt_forge(u_char *packet, int length, int subtype, int channel, unsigned char *essid, u_int16_t capability, u_char *rates_supp, u_char *rates_supp_ext)
{
	DPRINT(DMAX2, "%s: ieee80211_mgmt_forge", __FILE__);

	int data_length = 0;
	unsigned short interval;
	struct timeval tv1;
	u_int64_t timestamp = 0;
	int i;

	/*  setting pointer to the beginning of the packet */
	unsigned char *p = (unsigned char *) packet + length;

	switch (subtype) {
	
		case ST_ASSOC_REQUEST:
			/*  Capability: 2 bytes */
			memcpy(p + data_length, &capability, sizeof(capability));
			data_length += sizeof(capability);
			/*  interval */
			interval = BEACON_INTERVAL;
			p[data_length++] = interval & 0xFF;
			p[data_length++] = (interval >> 8) & 0xFF;
			break;
			
		case ST_PROBE_REQUEST:
			break;
		default:
			/*  fixed information first */
			memcpy(p + data_length, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 12); 
		
			gettimeofday(&tv1, NULL);
			timestamp = tv1.tv_usec; //tv1.tv_sec*1000000 + 
		
			for (i = 0; i < 8; i++) 
			{
				p[i + data_length] = (timestamp >> (i*8)) & 0xFF;
			}
		
			/*  interval */
			interval = BEACON_INTERVAL;
			p[data_length+8] = interval & 0xFF;
			p[data_length+9] = (interval >> 8) & 0xFF;
		
			/*  end fixed info */
			data_length += 10;
		
			/*  Capability: 2 bytes */
			memcpy(p + data_length, &capability, sizeof(capability));
			data_length += sizeof(capability);
		
			break;
	}
	
	/*  ESSID */
	int len = strlen((char*)essid);
	p[data_length++] = ELEMENT_ID_SSID; 
	p[data_length++] = len; /* essid tag  */
	if (len) 
	{
		memcpy(p + data_length, essid, len); /* actual essid */
		data_length += len;
	}

	/*  Supported rates */
	memset(p + data_length++, ELEMENT_ID_SUPPORTED_RATES, 1); 
	memset(p + data_length++, EID_SUPPORTED_RATES_LENGHT, 1); 
	memcpy(p + data_length, rates_supp, EID_SUPPORTED_RATES_LENGHT); 
	data_length += EID_SUPPORTED_RATES_LENGHT;
	
	/*  Channel Tag */
	if (channel)
	{
		memset(p + data_length++, ELEMENT_ID_CHANNEL, 1); /* channel tag  */
		memset(p + data_length++, sizeof(char), 1); 
		memset(p + data_length++, channel, 1); 
	}

	/*  Extended supported rates */
	memset(p + data_length++, ELEMENT_ID_EXT_SUPPORTED_RATES, 1); 
	memset(p + data_length++, EID_SUPPORTED_RATES_EXT_LENGHT, 1); 
	memcpy(p + data_length, rates_supp_ext, EID_SUPPORTED_RATES_EXT_LENGHT); 
	data_length += EID_SUPPORTED_RATES_EXT_LENGHT;

	return length + data_length;
}

int ieee80211_auth_forge(u_char *packet, int length, u_int16_t auth_algo, u_int16_t auth_seq, u_int16_t status)
{
	int data_length = 0;
	unsigned char *p = (unsigned char *) packet + length;
	DPRINT(DMAX2, "%s: ieee80211_auth_forge", __FILE__);

	/*  Authentification system */
	memcpy(p + data_length, (unsigned char *) &auth_algo, 2);
	data_length += 2;
	memcpy(p + data_length, (unsigned char *) &auth_seq, 2);
	data_length += 2;
	memcpy(p + data_length, (unsigned char *) &status, 2);
	data_length += 2;

	return length + data_length;
}

int ieee80211_assoc_forge(u_char *packet, int length, u_int16_t capability, u_int16_t status, u_int16_t aid, u_char *rates_supp, u_char *rates_supp_ext)
{
	int data_length = 0;
	unsigned char *p = (unsigned char *) packet + length;
	DPRINT(DMAX2, "%s: ieee80211_assoc_forge", __FILE__);

	/*  Capability: 2 bytes */
	memcpy(p + data_length, (unsigned char *) &capability, 2);
	data_length += 2;
	
	/*  Status code: 2 bytes */
	memcpy(p + data_length, (unsigned char *) &status, 2);
	data_length += 2;

	/* AID: 2 bytes */
	memcpy(p + data_length, (unsigned char *) &aid, 2);
	data_length += 2;

	/*  Supported rates */
	memset(p + data_length++, ELEMENT_ID_SUPPORTED_RATES, 1); 
	memset(p + data_length++, sizeof(rates_supp), 1); 
	memcpy(p + data_length, rates_supp, sizeof(rates_supp)); 
	data_length += sizeof(rates_supp);
	
	/*  Extended supported rates */
	memset(p + data_length++, ELEMENT_ID_EXT_SUPPORTED_RATES, 1); 
	memset(p + data_length++, sizeof(rates_supp_ext), 1); 
	memcpy(p + data_length, rates_supp_ext, sizeof(rates_supp_ext)); 
	data_length += sizeof(rates_supp_ext);

	return length + data_length;
}
	
void ieee80211_parse_mgmt_frame_body(u_char *packet, int length, struct mgmt_body_t *mgmt_body_fields)
{
	/*  tag parsing in management frame */
	u_char *p, *f;
	u_char *tag = NULL;
	int i;

	p = packet + sizeof(struct mgmt_header_t);
	memset(mgmt_body_fields, '\0', sizeof(struct mgmt_body_t));
	memset(&mgmt_body_fields->ssid, '\0', sizeof(struct ssid_t));

	memcpy(&mgmt_body_fields->timestamp, p, sizeof(u_int8_t)*8);
	p += sizeof(u_int8_t)*8;
	
	memcpy(&mgmt_body_fields->beacon_interval, p, sizeof(u_int16_t));
	p += sizeof(u_int16_t);

	memcpy(&mgmt_body_fields->capability_info, p, sizeof(u_int16_t));
	p += sizeof(u_int16_t);
	
	while (p < (packet + length))
	{
		u_char tag_number = p[0];
		u_char tag_length = p[1];
		tag = (u_char *) (p + 2);
		f = NULL;

		switch (tag_number) 
		{
			case ELEMENT_ID_SSID:
				f = mgmt_body_fields->ssid.ssid;
				mgmt_body_fields->ssid.element_id = ELEMENT_ID_SSID;
				mgmt_body_fields->ssid.length = tag_length;
				break;
			case ELEMENT_ID_SUPPORTED_RATES:
				break;
			case ELEMENT_ID_EXT_SUPPORTED_RATES:
				break;
			case ELEMENT_ID_CHANNEL:
				f = (u_char *) &mgmt_body_fields->channel;
				break;
			case ELEMENT_ID_CHANSWITCHANN:
				mgmt_body_fields->channel_switch.element_id = ELEMENT_ID_CHANSWITCHANN;
				mgmt_body_fields->channel_switch.length = tag_length;
				mgmt_body_fields->channel_switch.mode = *(tag);
				mgmt_body_fields->channel_switch.new_channel = *(tag + 1);
				mgmt_body_fields->channel_switch.count = *(tag + 2);
				break;
			case ELEMENT_ID_CHALLENGE_TEXT:
				break;
			case ELEMENT_PACMAP_RSSI:
				if (tag_length == sizeof(mgmt_body_fields->pacmap_rssi))
				{
					f = (u_char *) &mgmt_body_fields->pacmap_rssi;
				}
				break;
			case ELEMENT_PACMAP_SEQ:
				f = (u_char *) &mgmt_body_fields->pacmap_seq;
				break;
			default:
				f = NULL;
				break;
		}
		for (i = 0; f != NULL && i < tag_length; i++, f++) {
			*f = *(tag + i);
		}
		p += 2 + tag_length;
		f = NULL;
	}	
}	

int ieee80211_status_code_auth(u_char *packet)
{
	int code;
	u_char *p = packet + sizeof(struct mgmt_header_t);
	/* Auth Algo + Auth Seq */
	p += 4;
	code = *p + (*(p + 1) << 8);
	return code;
}

int ieee80211_status_code_assoc(u_char *packet)
{
	int code;
	u_char *p = packet + sizeof(struct mgmt_header_t);
	/* Capability information */
	p += 2;
	code = *p + (*(p + 1) << 8);
	return code;
}

/*** Common functions ***/
int is_same_mac(MACADDR_TYPE(a), MACADDR_TYPE(b)) 
{
	if (a == NULL || b == NULL)
		return 0;
	/* MAC addresses are 6 bytes long */
	return (memcmp(a, b, 6*sizeof(u_int8_t)) == 0);
}	

int is_same_ip(IPADDR_TYPE(a), IPADDR_TYPE(b))
{
	if (a == NULL || b == NULL)
		return 0;
	/* IP addresses are 4 bytes long */
	return (memcmp(a, b, 4*sizeof(u_int8_t)) == 0);
}

int is_same_ssid(u_char *a, u_char *b) 
{
	if (strlen((char *)a) != strlen((char *)b))
		return 0;
	return (memcmp(a, b, strlen((char *)a)*sizeof(u_char)) == 0);
}	

int is_same_ssid_prefix(u_char *a, u_char *b) 
{
	return (memcmp(a, b, strlen((char *)a)*sizeof(u_char)) == 0);
}	


char* packet_type(u_int16_t type)
{
	switch (type)
	{
		case 0x0800: return "IPv4";
		case 0x0806: return "ARP ";
		case 0x86DD: return "IPv6";
	}
	return "???";
}

int _sthexa_to_int(char c)
{
	if ('0' <= c && c <= '9')
		return c - '0' ;
	if ('A' <= c && c <= 'F')
		return c - 'A' + 10;
	if ('a' <= c && c <= 'f')
		return c - 'a' + 10;
	return 0;
}				

void ato_mac_address(MACADDR_TYPE(macaddr), char *s)
{
	/* s = "XX:XX:XX:XX:XX:XX" */
	int i;
	for (i = 0; i < 6; i++)
	{
		macaddr[i] = _sthexa_to_int(s[3*i])*0x10 + _sthexa_to_int(s[3*i + 1]);
	}
}

void ato_ip_address(IPADDR_TYPE(ipaddr), char *s)
{
	/* s = "192.168.1.13" */
	int i = 0, j = 0;
	int len = strlen(s);
	while (i < len)
	{
		ipaddr[j] = 0;
		while (s[i] != '.' && i < len)
		{
			ipaddr[j] = ipaddr[j]*10 + (s[i] - '0');
			i++;
		}
		i++; j++;
	}
}

char* get_l4proto(unsigned char packet[4096], int length, u_int16_t ether_type)
{
	if (ether_type != 0x0800) return " ";

	u_char l3proto = *(packet + 41);
	switch (l3proto)
	{
		case 1: return "ICMP";
		case 2: return "IGMP";
		case 6: return "TCP";
		case 17: return "UDP";
		case 89: return "OSPF";
		case 132: return "SCTP";
	}
	return "?";
}

void print_icmp_seq(unsigned char packet[4096], int length, u_int16_t ether_type)
{
	if ((ether_type != 0x0800) || (ether_type == 0x0800 && *(packet + 41) != 1)) return;

	char buffer[1000];
	memset(buffer, '\0', 1000);
	u_char *p = packet + 44;
	sprintf(buffer, "%d.%d.%d.%d -> %d.%d.%d.%d ", *(p), *(p+1), *(p+2), *(p+3), *(p+4), *(p+5), *(p+6), *(p+7));

	p = packet + 52;
	switch (*p)
	{
		case 8:
			sprintf(buffer + strlen(buffer), "Echo Request ");	
			break;
		case 0:
			sprintf(buffer + strlen(buffer), "Echo Reply ");	
			break;
	}

	p = packet + 58;
	sprintf(buffer + strlen(buffer), "icmp_seq=%d", *p*0x100 + *(p+1));	
	DPRINT(DDEBUG, "%s", buffer);
}
