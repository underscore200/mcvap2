#ifndef __PACMAP_PROTO80211_H__
#define __PACMAP_PROTO80211_H__

#include <pacmap/pacmap.h>
#include <pacmap/wifi/ieee802_11.h>

struct wifi_fn_t *wfn_alloc(int sz);
void wfn_free(struct wifi_fn_t *wfn);

/* PacMap API functions */
int proto80211_packet_send(void* buf, size_t count, int rate);
int proto80211_packet_recv(struct wifi_fn_t *wfn, u_char *packet, int length, struct wif *wi, int rssi);		
int proto80211_packet_ti_send(void* buf, size_t count);
int proto80211_packet_ti_recv(struct wifi_fn_t *wfn, u_char *packet, int length, struct wif *wi);
void proto80211_add_timer(int timer_usec, void *arg, int sz_arg);
int proto80211_ap_msgs_msg_send(void* buf, size_t count, IPADDR_TYPE(ip_address));
int proto80211_ap_msgs_msg_recv(struct wifi_fn_t *wfn, IPADDR_TYPE(ip_address), u_char *packet, int length);

/* Frame functions */
int ieee80211_framectrl_forge(u_char * packet, int length, u_char *addr1, u_char *addr2, u_char *addr3, u_int16_t sequence_ctrl, u_int16_t fc, u_int16_t duration);
int ieee80211_mgmt_forge(u_char *packet, int length, int subtype, int channel, unsigned char *essid, u_int16_t capability, u_char *rates_supp, u_char *rates_supp_ext);
int ieee80211_auth_forge(u_char *packet, int length, u_int16_t auth_algo, u_int16_t auth_seq, u_int16_t status);
int ieee80211_assoc_forge(u_char *packet, int length, u_int16_t capability, u_int16_t status, u_int16_t aid, u_char *rates_supp, u_char *rates_supp_ext);
void ieee80211_parse_mgmt_frame_body(u_char *packet, int length, struct mgmt_body_t *mgmt_body_fields);
int ieee80211_status_code_auth(u_char *packet);
int ieee80211_status_code_assoc(u_char *packet);

// TEST
int ieee80211_ack_forge(u_char *packet, int length, u_char *addr1, u_int16_t fc, u_int16_t duration);

/* Common functions */
int is_same_ssid(u_char *a, u_char *b);
int is_same_ssid_prefix(u_char *a, u_char *b);
char* packet_type(u_int16_t type);
char* get_l4proto(unsigned char packet[4096], int length, u_int16_t ether_type);
void print_icmp_seq(unsigned char packet[4096], int length, u_int16_t ether_type);

/*** AP MSGS messages ***/
enum {
	AP_MSGS_SCAN_REQUEST		= 0x01,
	AP_MSGS_SCAN_RESPONSE		= 0x02,
	AP_MSGS_CLIENT_MOVE			= 0x04,
};

#endif /* __PACMAP_PROTO80211_H__ */

