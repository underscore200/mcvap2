/*
 * Example of a monitor card.
 * This device just listens to packets, but does not process them.
 *
 * 2010  Maria Eugenia Berezin <berezin @ imag.fr>
 *
 */
 
#include <pacmap/pacmap.h>
#include "wifi/monitor.h"

int main(int argc, char** argv)
{       
        pacmap_init(argc, argv, monitor_init);
        return 0;
}

